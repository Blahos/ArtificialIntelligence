﻿using System;

namespace GeneticAlgorithmLibrary
{
	public static class CrossingFunctions
	{
		private static Random mRandom = new Random();
		public static Tuple<TDna, TDna> Cross<TDna>(TDna aDna1, TDna aDna2, double aCrossChance = 1.0)
			where TDna : BinaryDna, new()
		{
			if (aDna1 == null) throw new ArgumentNullException("aDna1");
			if (aDna2 == null) throw new ArgumentNullException("aDna2");
			if (aDna1.Code == null) throw new NullReferenceException("aDna1.Code");
			if (aDna2.Code == null) throw new NullReferenceException("aDna2.Code");

			var lResult = Cross(aDna1.Code, aDna2.Code, aCrossChance);

			return new Tuple<TDna, TDna>(new TDna { Code = lResult.Item1 }, new TDna { Code = lResult.Item2 });
		}

		public static Tuple<byte[], byte[]> Cross(byte[] aDna1, byte[] aDna2, double aCrossChance = 1.0)
		{
			if (aDna1 == null) throw new ArgumentNullException("aDna1");
			if (aDna2 == null) throw new ArgumentNullException("aDna2");
			if (aDna1.Length != aDna2.Length) throw new Exception("Binary codes must have the same length !");

			var lCrossPosition = mRandom.Next(0, ObviousConstants.cBitsInByte * aDna1.Length + 1); // first flipped position
			if (mRandom.NextDouble() < 1 - aCrossChance) lCrossPosition = 0;
			//Console.WriteLine("Crossing position: {0}", lCrossPosition);

			var lResult1 = new byte[aDna1.Length];
			var lResult2 =  new byte[aDna1.Length];

			// Partialy flipped byte index
			var lPByteIndex = lCrossPosition / ObviousConstants.cBitsInByte;

			// Keep the same bytes
			if (lPByteIndex > 0)
			{
				Buffer.BlockCopy(aDna1, 0, lResult1, 0, lPByteIndex);
				Buffer.BlockCopy(aDna2, 0, lResult2, 0, lPByteIndex);
			}

			// Flip bytes
			if (aDna1.Length - (lPByteIndex + 1) > 0)
			{
				Buffer.BlockCopy(aDna2, lPByteIndex + 1, lResult1, lPByteIndex + 1, aDna1.Length - (lPByteIndex + 1));
				Buffer.BlockCopy(aDna1, lPByteIndex + 1, lResult2, lPByteIndex + 1, aDna1.Length - (lPByteIndex + 1));
			}

			// Partialy flipped byte
			var lMask = (byte)0;
			var lUnflippedCount = lCrossPosition - lPByteIndex * ObviousConstants.cBitsInByte;

			for (byte i = 0; i < lUnflippedCount; i++)
			{
				lMask |= (byte)((byte)1 << (byte)(ObviousConstants.cBitsInByte - i - (byte)1));
			}

			if (lPByteIndex < aDna1.Length)
			{
				lResult1[lPByteIndex] = (byte)(aDna1[lPByteIndex] & lMask);
				lResult2[lPByteIndex] = (byte)(aDna2[lPByteIndex] & lMask);

				lResult1[lPByteIndex] |= (byte)(aDna2[lPByteIndex] & (byte)(~lMask));
				lResult2[lPByteIndex] |= (byte)(aDna1[lPByteIndex] & (byte)(~lMask));
			}
			return new Tuple<byte[], byte[]>(lResult1, lResult2);
		}
	}
}
