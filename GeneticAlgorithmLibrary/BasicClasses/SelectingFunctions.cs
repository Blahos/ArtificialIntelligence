﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GeneticAlgorithmLibrary
{
	public static class SelectingFunctions
	{
		private static readonly Random cRandom = new Random();
		/// <summary>
		/// "Standardní" select pro fitness členy, pravděpodobnost výběru člena do další populace je úměrná
		/// jeho fitness hodnotě.
		/// </summary>
		/// <typeparam name="TMember"></typeparam>
		/// <typeparam name="TDna"></typeparam>
		/// <typeparam name="TFitnessArgument"></typeparam>
		/// <param name="aMembers"></param>
		/// <param name="aSelectionIndex"></param>
		/// <returns></returns>
		public static Tuple<TMember, TMember> Select<TMember, TDna, TFitnessArgument, TInit>(List<TMember> aMembers)
			where TMember : FitnessPopulationMember<TDna, TFitnessArgument, TInit>
			where TDna : GADna
		{
			var lTotalFitness = aMembers.Select(a => a.Fitness).Sum();

			var lRand1 = cRandom.NextDouble() * lTotalFitness;
			var lMember1Index = 0;
			while (lMember1Index < aMembers.Count && lRand1 > aMembers[lMember1Index].Fitness)
			{
				lRand1 -= aMembers[lMember1Index].Fitness;
				lMember1Index++;
			}

			var lRand2 = cRandom.NextDouble() * lTotalFitness;
			var lMember2Index = 0;
			while (lMember2Index < aMembers.Count && lRand2 > aMembers[lMember2Index].Fitness)
			{
				lRand2 -= aMembers[lMember2Index].Fitness;
				lMember2Index++;
			}

			return new Tuple<TMember, TMember>(aMembers[lMember1Index], aMembers[lMember2Index]);
		}
	}
}
