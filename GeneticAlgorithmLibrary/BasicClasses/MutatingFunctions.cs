﻿using System;

namespace GeneticAlgorithmLibrary
{
	public static class MutatingFunctions
	{
		private static readonly Random cRandom = new Random();
		/// <summary>
		/// Provede mutaci na náhodné bitu v kódu dna. Mutace stoprocentně proběhne,
		/// funkce nepracuje se žádnou pravděpodobností.
		/// </summary>
		/// <typeparam name="TDna"></typeparam>
		/// <param name="aDna"></param>
		/// <returns></returns>
		public static TDna Mutate<TDna>(TDna aDna)
			where TDna : BinaryDna, new()
		{
			var lDnaLength = aDna.Code.Length;
			var lMutationPosition = cRandom.Next(0, ObviousConstants.cBitsInByte * lDnaLength);
			var lByteIndex = lMutationPosition / ObviousConstants.cBitsInByte;
			var lBitIndex = lMutationPosition % ObviousConstants.cBitsInByte;

			var lNewCode = new byte[lDnaLength];
			Buffer.BlockCopy(aDna.Code, 0, lNewCode, 0, lDnaLength);

			var lMutatedByte = lNewCode[lByteIndex];
			lMutatedByte = (byte)(lMutatedByte ^ (byte)(1 << (ObviousConstants.cBitsInByte - lBitIndex - 1)));

			lNewCode[lByteIndex] = lMutatedByte;

			var lNewDna = new TDna { Code = lNewCode };
			return lNewDna;
		}
		public static TDna MutateWithChance<TDna>(TDna aDna, double aChance)
			where TDna : BinaryDna, new()
		{
			if (cRandom.NextDouble() < aChance)
			{
				var lNewDna = Mutate(aDna);
				return lNewDna;
			}
			return new TDna { Code = aDna.Code };
		}
	}
}
