﻿
namespace GeneticAlgorithmLibrary
{
	public abstract class FitnessPopulationMember<TDna, TFitnessArgument, TInit> : GAPopulationMember<TDna, TInit>
		where TDna : GADna
	{
		private double mFitness;
		/// <summary>
		/// Fitness calculated by last call of CalculateFitness method. 0 by default
		/// </summary>
		public double Fitness { get { return mFitness; } }
		protected abstract double CalculateFitnessInner(TFitnessArgument aArgument);
		/// <summary>
		/// Objekt si vypočítá fitness podle zadaného argumentu, vypočtenou hodnotu jednak vrátí,
		/// a také si ji uloží do property Fitness
		/// </summary>
		/// <param name="aArgument"></param>
		/// <returns></returns>
		public double CalculateFitness(TFitnessArgument aArgument)
		{
			var lFitness = CalculateFitnessInner(aArgument);
			mFitness = lFitness;
			return lFitness;
		}
	}
}
