﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GeneticAlgorithmLibrary
{
    public class GASolver<TMember, TDna, TInit>
		where TMember : GAPopulationMember<TDna, TInit>, new()
		where TDna : GADna
    {
		private GAPopulation<TMember, TDna, TInit> mPopulation;
		private object mLock = new object();
		public Action<int, List<TMember>> PostIterationAction { get; set; }
		public Action<int, List<TMember>> PreIterationAction { get; set; }

		public void SetPopulation(List<TMember> aPopulation)
		{
			mPopulation = new GAPopulation<TMember, TDna, TInit>(aPopulation);
		}
		public GASolver(List<TMember> aInitialPopulation)
		{
			SetPopulation(aInitialPopulation);
		}
		public List<TMember> Solve(int aIterationCount,
			SelectCoupleDelegate<TMember, TDna, TInit> aCoupleSelectingFunction,
			CrossDelegate<TDna> aDnaCrossingFunction,
			MutateDelegate<TDna> aMutationFunction,
			TInit aInitArgument)
		{
			lock (mLock)
			{
				for (int i = 0; i < aIterationCount; i++)
				{
					if (PreIterationAction != null)
						PreIterationAction.Invoke(i, mPopulation.Members);
					mPopulation.Evolve(aCoupleSelectingFunction, aDnaCrossingFunction, aMutationFunction, aInitArgument);
					if (PostIterationAction != null)
						PostIterationAction.Invoke(i, mPopulation.Members);
				}

				return mPopulation.Members.ToList();
			}
		}

		public List<TMember> Solve(int aIterationCount,
			IEvolution<TMember, TDna, TInit> aEvolution,
			TInit aInitArgument)
		{
 			lock (mLock)
			{
				return Solve(aIterationCount, aEvolution.SelectCouple, aEvolution.Cross, aEvolution.Mutate, aInitArgument);
			}
		}
    }
}
