﻿using System;
using System.Collections.Generic;

namespace GeneticAlgorithmLibrary
{
	public interface IEvolution<TMember, TDna, TInit>
		where TMember : GAPopulationMember<TDna, TInit>
		where TDna : GADna
	{
		Tuple<TMember, TMember> SelectCouple(List<TMember> aPopulation, int aSelectionIndex);
		Tuple<TDna, TDna> Cross(TDna aDna1, TDna aDna2, int aSelectionIndex);
		TDna Mutate(TDna aSourceDna, int aSelectionIndex, int aMutationIndex);
	}
}
