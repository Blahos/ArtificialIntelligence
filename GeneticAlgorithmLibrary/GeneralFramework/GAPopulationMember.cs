﻿
namespace GeneticAlgorithmLibrary
{
	public abstract class GAPopulationMember<TDna, TInit>
		where TDna : GADna
	{
		public abstract TDna GetDna();
		public abstract void ConstructFromDna(TDna aDna);
		public virtual void Initialize(TInit aInitArgument)
		{

		}

		public GAPopulationMember(TDna aDna)
		{
			ConstructFromDna(aDna);
		}
		public GAPopulationMember()
		{

		}
	}
}
