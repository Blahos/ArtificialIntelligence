﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GeneticAlgorithmLibrary
{
	public delegate Tuple<TMember, TMember> SelectCoupleDelegate<TMember, TDna, TInit>(List<TMember> aPopulation, int aSelectionIndex) 
	    where TMember : GAPopulationMember<TDna, TInit> 
	    where TDna : GADna;

	public delegate Tuple<T, T> CrossDelegate<T>(T aDna1, T aDna2, int aSelectionIndex) 
	    where T : GADna;

	public delegate T MutateDelegate<T>(T aSourceDna, int aSelectionIndex, int aMutationIndex)
	    where T : GADna;

	public class GAPopulation<TMember, TDna, TInit>
		where TMember : GAPopulationMember<TDna, TInit>, new()
		where TDna : GADna
	{
		private object mLock = new object();
        public List<TMember> Members { get; private set; }

        public GAPopulation(IEnumerable<TMember> aInitialPopulation)
		{
			if (aInitialPopulation == null) throw new NullReferenceException("aInitialPopulation");
			var lPopulationList = aInitialPopulation.ToList();
			if (lPopulationList.Count % 2 == 1) throw new Exception("Population size must be even!");
			Members = lPopulationList;
		}

		public void Evolve(
			SelectCoupleDelegate<TMember, TDna, TInit> aCoupleSelectingFunction,
			CrossDelegate<TDna> aDnaCrossingFunction,
			MutateDelegate<TDna> aMutationFunction,
			TInit aInitArgument)
		{
			lock (mLock)
			{
				var lCurrentPopulation = Members;
				var lNewPopulation = new List<TMember>();
                
                // Create new couples from the old ones
				for (int i = 0; i < (lCurrentPopulation.Count + 1) / 2; i++)
				{
					var lCouple = aCoupleSelectingFunction.Invoke(lCurrentPopulation, i);
					var lCoupleDna = new Tuple<TDna, TDna>(lCouple.Item1.GetDna(), lCouple.Item2.GetDna());
					var lNewChildrenDna = aDnaCrossingFunction.Invoke(lCoupleDna.Item1, lCoupleDna.Item2, i);

					var lNewChild1DnaMutated = aMutationFunction.Invoke(lNewChildrenDna.Item1, i, 0);

					var lNewChild1 = new TMember();
					lNewChild1.Initialize(aInitArgument);
					lNewChild1.ConstructFromDna(lNewChild1DnaMutated);
					lNewPopulation.Add(lNewChild1);

                    // The second child is only created if there is still space in the new population
                    // This is for situations when the population size is odd - in those cases, the last
                    // couple will add only one child into the new population
                    if (lNewPopulation.Count < lCurrentPopulation.Count)
                    {
                        var lNewChild2DnaMutated = aMutationFunction.Invoke(lNewChildrenDna.Item2, i, 1);
                        var lNewChild2 = new TMember();
                        lNewChild2.Initialize(aInitArgument);
                        lNewChild2.ConstructFromDna(lNewChild2DnaMutated);
                        lNewPopulation.Add(lNewChild2);
                    }
				}

				Members = lNewPopulation;
			}
		}
	}
}
