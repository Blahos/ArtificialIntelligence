﻿using Library;
using LibraryWpf;
using Microsoft.Win32;
using NeuralNetworkLibrary;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace TestingAppWpf
{
    public class MainWindowViewModel : BaseViewModel
    {
        private readonly ActionQueue mQueue = new ActionQueue(20);
        private Timer mTimer;
        private readonly Random mRandom = new Random();
        private NeuralNetwork mNetwork;
        private LineSeries mLineSeries;
        
        private ExampleSettings mExampleSettings = null;
        public ExampleSettings ExampleSettings
        {
            get { return mExampleSettings; }
            private set
            {
                if (mExampleSettings == value) return;
                mExampleSettings = value;
                OnPropertyChanged();
            }
        }


        private bool mUpdateTable = true;
        public bool UpdateTable
        {
            get { return mUpdateTable; }
            set
            {
                if (value == mUpdateTable) return;
                mUpdateTable = value;
                if (mUpdateTable) UpdateTableData();
                OnPropertyChanged();
            }
        }

        private ObservableCollection<PointStatistics> mTrainCaseStatistics;
        public ObservableCollection<PointStatistics> TrainCaseStatistics
        {
            get { return mTrainCaseStatistics; }
            set
            {
                if (value == mTrainCaseStatistics) return;
                mTrainCaseStatistics = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<PointStatistics> mTestCaseStatistics;
        public ObservableCollection<PointStatistics> TestCaseStatistics
        {
            get { return mTestCaseStatistics; }
            set
            {
                if (value == mTestCaseStatistics) return;
                mTestCaseStatistics = value;
                OnPropertyChanged();
            }
        }

        private PlotModel mPlotViewModel;
        public PlotModel PlotViewModel
        {
            get { return mPlotViewModel; }
            set
            {
                if (value == mPlotViewModel) return;
                mPlotViewModel = value;
                OnPropertyChanged();
            }
        }

        private double mTotalError;
        public double TotalError
        {
            get { return mTotalError; }
            set
            {
                if (value == mTotalError) return;
                mTotalError = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<double> mWeights;
        public ObservableCollection<double> Weights
        {
            get { return mWeights; }
            set
            {
                if (value == mWeights) return;
                mWeights = value;
                OnPropertyChanged();
            }
        }

        private double mBias;
        public double Bias
        {
            get { return mBias; }
            set
            {
                if (value == mBias) return;
                mBias = value;
                OnPropertyChanged();
            }
        }

        private double mSteepness;
        public double Steepness
        {
            get { return mSteepness; }
            set
            {
                if (value == mSteepness) return;
                mSteepness = value;
                OnPropertyChanged();
            }
        }

        private bool mRunTimer = false;
        public bool RunTimer
        {
            get { return mRunTimer; }
            set
            {
                if (value == mRunTimer) return;
                mRunTimer = value;
                OnPropertyChanged();
            }
        }

        private int mNSteps = 1;
        public int NSteps
        {
            get { return mNSteps; }
            set
            {
                if (value == mNSteps) return;
                mNSteps = value;
                OnPropertyChanged();
            }
        }

        private RelayCommand mTrainNStepsCommand;
        public RelayCommand TrainNStepsCommand
        {
            get
            {
                if (mTrainNStepsCommand == null)
                {
                    mTrainNStepsCommand = new RelayCommand(a =>
                    {
                        TrainNSteps(NSteps);
                    });
                }
                return mTrainNStepsCommand;
            }
        }
        private RelayCommand mResetCommand;
        public RelayCommand ResetCommand
        {
            get
            {
                if (mResetCommand == null)
                {
                    mResetCommand = new RelayCommand(a => 
                    {
                        mQueue.Add(() =>
                        {
                            ResetNetwork();
                            ResetChart();
                            UpdateError();
                            UpdateTableData();
                        });
                    });
                }
                return mResetCommand;
            }
        }
        private RelayCommand mGenerateRandomCommand;
        public RelayCommand GenerateRandomCommand
        {
            get
            {
                if (mGenerateRandomCommand == null)
                {
                    mGenerateRandomCommand = new RelayCommand(a =>
                    {
                        mQueue.Add(() =>
                        {
                            var lNewExample = new ExampleSettings
                            {
                                TrainStep = 0.1,
                                Dimension = 2,
                                InputRange = new List<Range>
                            {
                                new Range(0, 1),
                                new Range(0, 1),
                            }
                            };
                            var lTrainCaseCount = 20;
                            var lTestCaseCount = 10;

                            lNewExample.TrainCases = new ObservableCollection<InputOutputPair>();
                            lNewExample.TestCases = new ObservableCollection<List<double>>();

                            for (int i = 0; i < lTrainCaseCount; i++)
                            {
                                var lX = mRandom.NextDouble();
                                var lY = mRandom.NextDouble();
                                lNewExample.TrainCases.Add(new InputOutputPair
                                {
                                    Input = new List<double> { lX, lY },
                                    Output = new List<double> { lX < lY ? 1 : 0 }
                                });
                            }
                            for (int i = 0; i < lTestCaseCount; i++)
                            {
                                lNewExample.TestCases.Add(new List<double>
                                {
                                    mRandom.NextDouble(),
                                    mRandom.NextDouble()
                                });
                            }

                            SetNewExampleSettings(lNewExample);
                        });
                    });
                }
                return mGenerateRandomCommand;
            }
        }
        private RelayCommand mLoadCaseCommand;
        public RelayCommand LoadCaseCommand
        {
            get
            {
                if (mLoadCaseCommand == null)
                {
                    mLoadCaseCommand = new RelayCommand(a =>
                    {
                        mQueue.Add(() =>
                        {
                            var lDialog = new OpenFileDialog();

                            if (lDialog.ShowDialog() ?? false)
                            {
                                var lNewExample = ExampleSettings.Load(lDialog.FileName);
                                SetNewExampleSettings(lNewExample);
                            }
                        });
                    });
                }
                return mLoadCaseCommand;
            }
        }

        public MainWindowViewModel()
        {
            ResetChart();

            mTimer = new Timer(TimerTick);
            mTimer.Change(20, Timeout.Infinite);
        }
        private void TimerTick(object aParam)
        {
            if (mRunTimer)
            {
                mQueue.Add(() =>
                {
                    TrainNSteps(NSteps);
                });
            }
            mTimer.Change(60, Timeout.Infinite);
        }
        private void TrainNSteps(int aCount)
        {
            if (ExampleSettings == null || ExampleSettings.TrainCasesNormalised == null) return;
            for (int i = 0; i < aCount; i++)
                foreach (var nTrainingCase in ExampleSettings.TrainCasesNormalised)
                {
                    mNetwork.Train(nTrainingCase.Input, nTrainingCase.Output, ExampleSettings.TrainStep);
                }

            if (ExampleSettings.Dimension == 2)
                UpdateChart();

            UpdateError();
            UpdateTableData();
        }
        private void ResetNetwork()
        {
            if (ExampleSettings == null) return;

            mNetwork = NeuralNetworkFactory.Construct2LayersCompleteNetwork(ExampleSettings.Dimension, 1);

            mNetwork.RandomiseBiases(aRandom: mRandom);
            mNetwork.RandomiseSteepnesses(aRandom: mRandom);
            mNetwork.RandomiseWeights(aRandom: mRandom);
        }
        private void ResetChart()
        {
            if (ExampleSettings == null) return;
            PlotViewModel = new PlotModel();
            PlotViewModel.InvalidatePlot(true);

            if (ExampleSettings.Dimension != 2) return;

            var lRange = ExampleSettings.InputRange;

            var lPointUpTrainSeries = new ScatterSeries { MarkerFill = OxyColors.Red };
            var lPointDownTrainSeries = new ScatterSeries { MarkerFill = OxyColors.Blue };
            var lPointUpTestSeries = new ScatterSeries { MarkerFill = OxyColors.Red, MarkerType = MarkerType.Circle };
            var lPointDownTestSeries = new ScatterSeries { MarkerFill = OxyColors.Blue, MarkerType = MarkerType.Circle };
            
            foreach (var nTrainCase in ExampleSettings.TrainCases)
            {
                var lValue = nTrainCase.Output[0];

                if (lValue > 0.5)
                    lPointUpTrainSeries.Points.Add(new ScatterPoint(nTrainCase.Input[0], nTrainCase.Input[1]));
                else
                    lPointDownTrainSeries.Points.Add(new ScatterPoint(nTrainCase.Input[0], nTrainCase.Input[1]));
            }
            foreach (var nTestCase in ExampleSettings.TestCases)
            {
                if (nTestCase[0] < nTestCase[1])
                {
                    lPointUpTestSeries.Points.Add(new ScatterPoint(nTestCase[0], nTestCase[1]));
                }
                else
                {
                    lPointDownTestSeries.Points.Add(new ScatterPoint(nTestCase[0], nTestCase[1]));
                }
            }

            var lXDiff = lRange[0].Max - lRange[0].Min;
            var lYDiff = lRange[1].Max - lRange[1].Min;
            var lExtraPercent = 0.05;

            var lXAxis = new LinearAxis
            {
                Position = AxisPosition.Bottom,
                Minimum = lRange[0].Min - lExtraPercent * lXDiff,
                Maximum = lRange[0].Max + lExtraPercent * lXDiff
            };
            var lYAxis = new LinearAxis
            {
                Position = AxisPosition.Left,
                Minimum = lRange[1].Min - lExtraPercent * lYDiff,
                Maximum = lRange[1].Max + lExtraPercent * lYDiff
            };
            //var lXAxis = new LinearAxis
            //{
            //    Position = AxisPosition.Bottom,
            //    Minimum = -10,
            //    Maximum = 20
            //};
            //var lYAxis = new LinearAxis
            //{
            //    Position = AxisPosition.Left,
            //    Minimum = -10,
            //    Maximum = 20
            //};

            PlotViewModel.Axes.Add(lXAxis);
            PlotViewModel.Axes.Add(lYAxis);

            PlotViewModel.Series.Add(lPointUpTrainSeries);
            PlotViewModel.Series.Add(lPointDownTrainSeries);
            PlotViewModel.Series.Add(lPointUpTestSeries);
            PlotViewModel.Series.Add(lPointDownTestSeries);

            mLineSeries = new LineSeries { Color = OxyColors.Green };
            PlotViewModel.Series.Add(mLineSeries);

            UpdateChart();
        }
        private void SetNewExampleSettings(ExampleSettings aSettings)
        {
            ExampleSettings = aSettings;

            ExampleSettings.TrainCasesNormalised = new ObservableCollection<InputOutputPair>();
            ExampleSettings.TestCasesNormalised = new ObservableCollection<List<double>>();

            foreach (var nCase in ExampleSettings.TrainCases)
            {
                var lInputNormalised = new List<double>();

                for (int i = 0; i < ExampleSettings.Dimension; i++)
                {
                    var lMin = ExampleSettings.InputRange[i].Min;
                    var lMax = ExampleSettings.InputRange[i].Max;
                    lInputNormalised.Add((nCase.Input[i] - lMin) / (lMax - lMin));
                }
                ExampleSettings.TrainCasesNormalised.Add(new InputOutputPair
                {
                    Input = lInputNormalised,
                    Output = nCase.Output
                });
            }

            foreach (var nCase in ExampleSettings.TestCases)
            {
                var lInputNormalised = new List<double>();

                for (int i = 0; i < ExampleSettings.Dimension; i++)
                {
                    var lMin = ExampleSettings.InputRange[i].Min;
                    var lMax = ExampleSettings.InputRange[i].Max;
                    lInputNormalised.Add((nCase[i] - lMin) / (lMax - lMin));
                }
                ExampleSettings.TestCasesNormalised.Add(lInputNormalised);
            }

            ResetNetwork();
            ResetChart();
            UpdateError();
            UpdateTableData();
        }

        private void UpdateChart()
        {
            Debug.Assert(mNetwork != null);
            mLineSeries.Points.Clear();

            var lNewPoints = GetPointsFromNetwork();
            foreach (var nPoint in lNewPoints) mLineSeries.Points.Add(nPoint);

            PlotViewModel.InvalidatePlot(true);
        }
        private void UpdateError()
        {
            if (ExampleSettings == null || ExampleSettings.TrainCasesNormalised == null) return;
            var lTotalError = 0.0;
            foreach (var nTrainingCase in ExampleSettings.TrainCasesNormalised)
            {
                lTotalError += mNetwork.GetError(nTrainingCase.Input, nTrainingCase.Output);
            }
            TotalError = lTotalError;
        }
        private void UpdateTableData()
        {
            if (!mUpdateTable) return;
            if (ExampleSettings == null) return;

            Weights = new ObservableCollection<double>(mNetwork.Connections.Select(a => a.Weight));
            Bias = mNetwork.InnerNodes[0].Bias;
            Steepness = mNetwork.InnerNodes[0].Steepness;

            var lTrainCaseStatistics = new List<PointStatistics>();
            for (int i = 0; i < ExampleSettings.TrainCases.Count; i++)
            {
                var lCase = ExampleSettings.TrainCases[i];
                var lCaseNormalised = ExampleSettings.TrainCasesNormalised[i];

                lTrainCaseStatistics.Add(new PointStatistics
                {
                    Input = lCase.Input,
                    DesiredOutput = lCase.Output,
                    NetworkOutput = mNetwork.GetValues(lCaseNormalised.Input),
                    Error = mNetwork.GetError(lCaseNormalised.Input, lCaseNormalised.Output)
                });
            }

            var lTestCaseStatistics = new List<PointStatistics>();
            for (int i = 0; i < ExampleSettings.TestCases.Count; i++)
            {
                var lCase = ExampleSettings.TestCases[i];
                var lCaseNormalised = ExampleSettings.TestCasesNormalised[i];

                lTestCaseStatistics.Add(new PointStatistics
                {
                    Input = lCase,
                    NetworkOutput = mNetwork.GetValues(lCaseNormalised)
                });
            }

            TrainCaseStatistics = new ObservableCollection<PointStatistics>(lTrainCaseStatistics);
        }
        private List<DataPoint> GetPointsFromNetwork()
        {
            var lPoints = new List<DataPoint>();
            // Ax + By + C = 0
            // => 
            // y = (-C - Ax) / B
            // or
            // x = (-C - By) / A
            var lA = mNetwork.Connections[0].Weight;
            var lB = mNetwork.Connections[1].Weight;
            var lC = mNetwork.InnerNodes[0].Bias;

            var lRange = ExampleSettings.InputRange;

            var lXDiff = lRange[0].Max - lRange[0].Min;
            var lYDiff = lRange[1].Max - lRange[1].Min;

            if (lB != 0)
            {
                lPoints.Add(new DataPoint(lRange[0].Min, -lC / lB * lYDiff + lRange[1].Min));
                lPoints.Add(new DataPoint(lRange[0].Max, (-lC - lA) / lB * lYDiff + lRange[1].Min));
            }
            else
            {
                lPoints.Add(new DataPoint(-lC / lA * lXDiff + lRange[0].Min, lRange[1].Min));
                lPoints.Add(new DataPoint((-lC - lB) / lA * lXDiff + lRange[0].Min, lRange[1].Max));
            }

            return lPoints;
        }
    }
}
