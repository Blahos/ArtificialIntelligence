﻿using LibraryWpf;
using Microsoft.Win32;
using NeuralNetworkLibrary;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingAppWpf
{
    public class MultilayerNetworkViewModel : BaseViewModel
    {

        private ObservableCollection<object> mTestCases;
        public ObservableCollection<object> TestCases
        {
            get { return mTestCases; }
            set
            {
                if (value == mTestCases) return;
                mTestCases = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<object> mTrainCases;
        public ObservableCollection<object> TrainCases
        {
            get { return mTrainCases; }
            set
            {
                if (value == mTrainCases) return;
                mTrainCases = value;
                OnPropertyChanged();
            }
        }

        private double mProgress = 0;
        public double Progress
        {
            get { return mProgress; }
            set
            {
                if (value == mProgress) return;
                mProgress = value;
                OnPropertyChanged();
            }
        }


        private double mProgress2 = 0;
        public double Progress2
        {
            get { return mProgress2; }
            set
            {
                if (value == mProgress2) return;
                mProgress2 = value;
                OnPropertyChanged();
            }
        }
        private bool mExecuteEnabled = true;
        public bool ExecuteEnabled
        {
            get { return mExecuteEnabled; }
            set
            {
                if (value == mExecuteEnabled) return;
                mExecuteEnabled = value;
                OnPropertyChanged();
            }
        }

        private bool mLoadEnabled = true;
        public bool LoadEnabled
        {
            get { return mLoadEnabled; }
            set
            {
                if (value == mLoadEnabled) return;
                mLoadEnabled = value;
                OnPropertyChanged();
            }
        }
        private PlotModel mPlotViewModel1;
        public PlotModel PlotViewModel1
        {
            get { return mPlotViewModel1; }
            set
            {
                if (value == mPlotViewModel1) return;
                mPlotViewModel1 = value;
                OnPropertyChanged();
            }
        }
        private PlotModel mPlotViewModel2;
        public PlotModel PlotViewModel2
        {
            get { return mPlotViewModel2; }
            set
            {
                if (value == mPlotViewModel2) return;
                mPlotViewModel2 = value;
                OnPropertyChanged();
            }
        }

        private RelayCommand mExecuteCommand;
        public RelayCommand ExecuteCommand
        {
            get
            {
                if (mExecuteCommand == null)
                {
                    mExecuteCommand = new RelayCommand(a =>
                    {
                        new Task(() => Execute()).Start();
                    });
                }
                return mExecuteCommand;
            }
        }
        private RelayCommand mLoadCommand;
        public RelayCommand LoadCommand
        {
            get
            {
                if (mLoadCommand == null)
                {
                    mLoadCommand = new RelayCommand(a =>
                    {
                        new Task(() => LoadAndTrain()).Start();
                    });
                }
                return mLoadCommand;
            }
        }
        
        public MultilayerNetworkViewModel()
        {

        }

        private void Execute()
        {
            ExecuteEnabled = false;

            var lNetworkData = GetNetwork();

            PlotViewModel1 = new PlotModel();
            PlotViewModel1.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, Minimum = 0, Maximum = 1 });
            PlotViewModel1.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Minimum = 0, Maximum = 1 });

            PlotViewModel2 = new PlotModel();
            PlotViewModel2.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, Minimum = 0, Maximum = 1 });
            PlotViewModel2.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Minimum = 0, Maximum = 1 });

            PlotViewModel2.Axes.Add(new LinearColorAxis());

            var lSize = 100;
            var lData = new double[lSize, lSize];
            
            for (int i = 0; i < lSize; i++)
            {
                for (int j = 0; j < lSize; j++)
                {
                    var lX = (double)i / (lSize - 1);
                    var lY = (double)j / (lSize - 1);

                    var lOutput = lNetworkData.Network.GetValue(lX, lY);

                    lData[i, j] = lOutput;
                }
            }

            var lValue0Points = new ScatterSeries { MarkerFill = OxyColors.Blue, MarkerType = MarkerType.Circle };
            var lValue1Points = new ScatterSeries { MarkerFill = OxyColors.Red };
            
            foreach (var nTrainCase in lNetworkData.TrainingCases)
            {
                if (nTrainCase.Item2 < 0.5)
                {
                    lValue0Points.Points.Add(new ScatterPoint(nTrainCase.Item1[0], nTrainCase.Item1[1]));
                }
                else
                {
                    lValue1Points.Points.Add(new ScatterPoint(nTrainCase.Item1[0], nTrainCase.Item1[1]));
                }
            }

            PlotViewModel1.Series.Add(lValue0Points);
            PlotViewModel1.Series.Add(lValue1Points);

            PlotViewModel2.Series.Add(new HeatMapSeries
            {
                Data = lData,
                X0 = 0,
                X1 = 1,
                Y0 = 0,
                Y1 = 1,
                Interpolate = true,
                RenderMethod = HeatMapRenderMethod.Bitmap
            });

            PlotViewModel1.InvalidatePlot(true);
            PlotViewModel2.InvalidatePlot(true);

            ExecuteEnabled = true;
        }
        private void LoadAndTrain()
        {
            LoadEnabled = false;
            Progress2 = 0;

            var lDialog = new OpenFileDialog();
            lDialog.Filter = "XML | *.xml";

            Doctor lData;
            if (lDialog.ShowDialog() ?? false)
            {
                lData = Doctor.FromXML(lDialog.FileName);
                lData.NormaliseInputs();
            }
            else
            {
                LoadEnabled = true;
                return;
            }

            var lNetworkStructure = new int[] { lData.Data.inputsCount }
                .Concat(lData.Data.neuronInLayersCount.ToArray()).ToArray();

            var lNetwork = NeuralNetworkFactory.ConstructCompleteNetwork(lNetworkStructure);

            var lRandom = new Random();
            lNetwork.RandomiseBiases(aRandom: lRandom);
            lNetwork.RandomiseSteepnesses(aRandom: lRandom);
            lNetwork.RandomiseWeights(aRandom: lRandom);

            var lRounds = 1000;

            for (int iRound = 0; iRound < lRounds; iRound++)
            {
                foreach (var nTrainCase in lData.Data.trainSet)
                {
                    var lError = lNetwork.GetError(nTrainCase.inputsNormalised, nTrainCase.outputs);
                    if (lError > 1e-6)
                        lNetwork.Train(nTrainCase.inputsNormalised, nTrainCase.outputs, lData.Data.learningRate);
                }
                Progress2 = (double)(iRound + 1) / lRounds;
            }

            var lTrainCases = new ObservableCollection<object>();
            foreach (var nTrainCase in lData.Data.trainSet)
            {
                lTrainCases.Add(new
                {
                    Inputs = new ListDouble(nTrainCase.inputs).ToString(),
                    Outputs = new ListDouble(nTrainCase.outputs).ToString(),
                    NetworkOutputs = new ListDouble(lNetwork.GetValues(nTrainCase.inputsNormalised)).ToString(),
                    Error = lNetwork.GetError(nTrainCase.inputsNormalised, nTrainCase.outputs).ToString("F3")
                });
            }
            TrainCases = lTrainCases;

            var lTestCases = new ObservableCollection<object>();
            foreach (var nTestCase in lData.Data.testSet)
            {
                lTestCases.Add(new
                {
                    Inputs = new ListDouble(nTestCase.inputs).ToString(),
                    NetworkOutputs = new ListDouble(lNetwork.GetValues(nTestCase.inputsNormalised)).ToString()
                });
            }
            TestCases = lTestCases;

            Progress2 = 1;
            LoadEnabled = true;
        }

        private NetworkWithTrainCases GetNetwork()
        {
            var lNetwork = NeuralNetworkFactory.ConstructCompleteNetwork(2, 8, 1);
            var lRandom = new Random();
            lNetwork.RandomiseBiases(aRandom: lRandom);
            lNetwork.RandomiseSteepnesses(aRandom: lRandom);
            lNetwork.RandomiseWeights(aRandom: lRandom);

            //lNetwork.InnerNodes[0].Bias = -0.5;
            //lNetwork.InnerNodes[0].InputConnections[0].Weight = 1;
            //lNetwork.InnerNodes[0].InputConnections[1].Weight = 0;
            //lNetwork.InnerNodes[0].Steepness = 100;

            //lNetwork.InnerNodes[1].Bias = -0.5;
            //lNetwork.InnerNodes[1].InputConnections[0].Weight = 0;
            //lNetwork.InnerNodes[1].InputConnections[1].Weight = 1;
            //lNetwork.InnerNodes[1].Steepness = 10000;

            //lNetwork.InnerNodes[2].Bias = -0.5;
            //lNetwork.InnerNodes[2].InputConnections[0].Weight = 1;
            //lNetwork.InnerNodes[2].InputConnections[1].Weight = 1;
            //lNetwork.InnerNodes[2].Steepness = 10000;

            //lNetwork = NeuralNetwork.FromTextFile("a.txt");

            var lTrainCases = new List<Tuple<List<double>, double>>();

            var lN = 150;

            for (int i = 0; i < lN; i++)
            {
                var lX = lRandom.NextDouble();
                var lY = lRandom.NextDouble();

                //var lOutput = ((lX > 0.5) && (lY > 0.5)) ? 1.0 : 0.0;
                //var lOutput = ((lX > 0.5) && (lY > 0.5) || (lX < 0.5) && (lY < 0.5)) ? 1.0 : 0.0;
                //var lOutput = ((lX - 0.5) * (lX - 0.5) + (lY - 0.5) * (lY - 0.5) < 0.0625) ? 1.0 : 0.0;
                var lOutput = ((lX - 0.3) * (lX - 0.3) + (lY - 0.4) * (lY - 0.4) < 0.05 || lX + lY > 1.6) ? 1.0 : 0.0;
                //var lOutput = ((lX > 0.5) && (lY > 0.5)) || ((lX - 0.2) * (lX - 0.2) + (lY - 0.2) * (lY - 0.2) < 0.02) ? 1.0 : 0.0;

                lTrainCases.Add(new Tuple<List<double>, double>(new List<double> { lX, lY }, lOutput));
            }

            //lTrainCases.Add(new Tuple<List<double>, double>(new List<double> { 0.45, 0.45 }, 0));
            //lTrainCases.Add(new Tuple<List<double>, double>(new List<double> { 0.49, 0.51 }, 1));
            //lTrainCases.Add(new Tuple<List<double>, double>(new List<double> { 0.51, 0.49 }, 1));
            //lTrainCases.Add(new Tuple<List<double>, double>(new List<double> { 0.51, 0.51 }, 1));

            var lRounds = 3000;
            var lTrainStep = 0.1;
            for (int iRound = 0; iRound < lRounds; iRound++)
            {
                for (int i = 0; i < lTrainCases.Count; i++)
                {
                    var lError1 = lNetwork.GetError(lTrainCases[i].Item1, lTrainCases[i].Item2);
                    if (lError1 > 1e-5)
                    {
                        //Console.WriteLine("Training on {0} {1}", lTrainCases[i].Item1[0], lTrainCases[i].Item1[1]);
                        lNetwork.Train(lTrainCases[i].Item1, lTrainCases[i].Item2, lTrainStep);
                    }
                    var lError2 = lNetwork.GetError(lTrainCases[i].Item1, lTrainCases[i].Item2);
                    if (lError2 > lError1)
                    {
                        Console.WriteLine("Error increse on {0} {1}", lTrainCases[i].Item1[0], lTrainCases[i].Item1[1]);
                    }
                }
                Progress = (double)(iRound + 1) / lRounds;
            }
            return new NetworkWithTrainCases
            {
                Network = lNetwork, 
                TrainingCases = lTrainCases
            };
        }

        private class NetworkWithTrainCases
        {
            public NeuralNetwork Network { get; set; }
            public List<Tuple<List<double>, double>> TrainingCases { get; set; }
        }
    }
    public class ListDouble : List<double>
    {
        public override string ToString()
        {
            var lString = string.Join(", ", this.Select(a => a.ToString("F3")));
            return lString;
        }
        public ListDouble(IEnumerable<double> aCollection) : base(aCollection) { }
    }
}
