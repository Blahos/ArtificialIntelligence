﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestingAppWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Test()
        {
            var lTest = new ExampleSettings
            {
                Dimension = 2,
                TrainStep = 0.3,
                InputRange = new List<Range>
                {
                    new Range(0, 10),
                    new Range(0, 10),
                }
            };

            lTest.TrainCases = new ObservableCollection<InputOutputPair>
            {
                new InputOutputPair { Input = new List<double> { 0.5, 0.5, 0.5 }, Output = new List<double> { 0 } },
                new InputOutputPair { Input = new List<double> { 0.2, 0.4, 0.6 }, Output = new List<double> { 0 } },
                new InputOutputPair { Input = new List<double> { 0.1, 0.8, 0.9 }, Output = new List<double> { 0 } },
                new InputOutputPair { Input = new List<double> { 0.98, 0.2, 0.1 }, Output = new List<double> { 1 } },
                new InputOutputPair { Input = new List<double> { 0.5, 0.9, 0.05 }, Output = new List<double> { 1 } },
                new InputOutputPair { Input = new List<double> { 0.7, 0.1, 0.1 }, Output = new List<double> { 0 } },
                new InputOutputPair { Input = new List<double> { 0.25, 0.8, 0.65 }, Output = new List<double> { 0 } },
                new InputOutputPair { Input = new List<double> { 0.8, 0.69, 0.2 }, Output = new List<double> { 1 } },
                new InputOutputPair { Input = new List<double> { 0.7, 0.6, 0.1 }, Output = new List<double> { 1 } },
                new InputOutputPair { Input = new List<double> { 0.7, 0.9, 0.05 }, Output = new List<double> { 1 } },
            };
            lTest.TestCases = new ObservableCollection<List<double>>
            {
                new List<double> { 0.5, 0.9, 0.1 },
                new List<double> { 0.3, 0.3, 0.3 },
                new List<double> { 0.6, 0.9, 0.2 },
            };

            lTest.Save("test.xml");
        }
    }
}
