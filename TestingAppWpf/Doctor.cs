﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TestingAppWpf
{
    /// <summary>
    /// Třída pro načtení XML se vstupy ze školy
    /// </summary>
    public class Doctor
    {
        public backpropagationNeuronNet Data { get; set; }

        public void NormaliseInputs()
        {
            foreach (var nTrainCase in Data.trainSet)
            {
                var lNormalisedInputs = new List<double>();
                for (int i = 0; i < Data.inputDescriptions.Count; i++)
                {
                    lNormalisedInputs.Add((nTrainCase.inputs[i] - Data.inputDescriptions[i].minimum) / (Data.inputDescriptions[i].maximum - Data.inputDescriptions[i].minimum));
                }
                nTrainCase.inputsNormalised = lNormalisedInputs;
            }
            foreach (var nTestCase in Data.testSet)
            {
                var lNormalisedInputs = new List<double>();
                for (int i = 0; i < Data.inputDescriptions.Count; i++)
                {
                    lNormalisedInputs.Add((nTestCase.inputs[i] - Data.inputDescriptions[i].minimum) / (Data.inputDescriptions[i].maximum - Data.inputDescriptions[i].minimum));
                }
                nTestCase.inputsNormalised = lNormalisedInputs;
            }
        }

        #region Serialization
        public void ToXML(string aFilePath)
        {
            using (var lSw = new StreamWriter(File.Create(aFilePath)))
            {
                var lSerializer = new XmlSerializer(typeof(backpropagationNeuronNet));
                lSerializer.Serialize(lSw, Data);
            }
        }
        public static Doctor FromXML(string aFilePath)
        {
            var lDoctor = new Doctor();
            using (var lSr = new StreamReader(File.OpenRead(aFilePath)))
            {
                var lSerializer = new XmlSerializer(typeof(backpropagationNeuronNet));
                var lData = lSerializer.Deserialize(lSr) as backpropagationNeuronNet;
                lDoctor.Data = lData;
            }
            return lDoctor;
        }
        #endregion

        #region Classes
        public class backpropagationNeuronNet
        {
            public List<inputDescription> inputDescriptions { get; set; }

            [XmlArrayItem("neuronInLayerCount")]
            public neuronInLayersCount neuronInLayersCount { get; set; }
            [XmlArrayItem("outputDescription")]
            public outputDescriptions outputDescriptions { get; set; }
            public trainSet trainSet { get; set; }
            public testSet testSet { get; set; }

            public int inputsCount { get; set; }
            public double lastStepInfluenceLearningRate { get; set; }
            public int layersCount { get; set; }
            public double learningRate { get; set; }
        }
        public class trainSet : List<trainSetElement>
        {

        }
        public class testSet : List<testSetElement>
        {

        }
        public class neuronInLayersCount : List<int>
        {

        }
        public class outputDescriptions : List<string>
        {

        }
        public class testSetElement
        {
            [XmlArrayItem("value")]
            public inputs inputs { get; set; }

            [XmlIgnore]
            public List<double> inputsNormalised { get; set; }
        }
        public class trainSetElement
        {
            [XmlArrayItem("value")]
            public inputs inputs { get; set; }

            [XmlArrayItem("value")]
            public outputs outputs { get; set; }

            [XmlIgnore]
            public List<double> inputsNormalised { get; set; }
        }
        public class inputs : List<double>
        {

        }
        public class outputs : List<double>
        {

        }
        public class inputDescription
        {
            public double minimum { get; set; }
            public double maximum { get; set; }
            public string name { get; set; }
        }
        #endregion
    }
}
