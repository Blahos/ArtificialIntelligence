﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingAppWpf
{
    public class InputOutputPair
    {
        public List<double> Input { get; set; }
        public List<double> Output { get; set; }
    }
}
