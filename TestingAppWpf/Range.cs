﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingAppWpf
{
    public struct Range
    {
        public double Min { get; set; }
        public double Max { get; set; }
        public Range(double aMin, double aMax)
        {
            Min = aMin;
            Max = aMax;
        }
        public static bool operator==(Range aFirst, Range aSecond)
        {
            return
                aFirst.Min == aSecond.Min &&
                aFirst.Max == aSecond.Max;
        }
        public static bool operator !=(Range aFirst, Range aSecond)
        {
            return !(aFirst == aSecond);
        }
    }
}
