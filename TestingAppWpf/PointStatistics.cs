﻿using LibraryWpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingAppWpf
{
    public class PointStatistics : BaseViewModel
    {

        private List<double> mInput;
        public List<double> Input
        {
            get { return mInput; }
            set
            {
                if (value == mInput) return;
                mInput = value;
                OnPropertyChanged();
            }
        }
        
        private List<double> mNetworkOutput;
        public List<double> NetworkOutput
        {
            get { return mNetworkOutput; }
            set
            {
                if (value == mNetworkOutput) return;
                mNetworkOutput = value;
                OnPropertyChanged();
            }
        }

        private List<double> mDesiredOutput;
        public List<double> DesiredOutput
        {
            get { return mDesiredOutput; }
            set
            {
                if (value == mDesiredOutput) return;
                mDesiredOutput = value;
                OnPropertyChanged();
            }
        }


        private double mError;
        public double Error
        {
            get { return mError; }
            set
            {
                if (value == mError) return;
                mError = value;
                OnPropertyChanged();
            }
        }
    }
}
