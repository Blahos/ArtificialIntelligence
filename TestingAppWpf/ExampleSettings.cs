﻿using LibraryWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TestingAppWpf
{
    public class ExampleSettings : BaseViewModel
    {
        private int mDimension;
        public int Dimension
        {
            get { return mDimension; }
            set
            {
                if (value == mDimension) return;
                mDimension = value;
                OnPropertyChanged();
            }
        }
        private List<Range> mInputRange;
        public List<Range> InputRange
        {
            get { return mInputRange; }
            set
            {
                if (value == mInputRange) return;
                mInputRange = value;
                OnPropertyChanged();
            }
        }
        private double mTrainStep;
        public double TrainStep
        {
            get { return mTrainStep; }
            set
            {
                if (value == mTrainStep) return;
                mTrainStep = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<InputOutputPair> mTrainCases;
        public ObservableCollection<InputOutputPair> TrainCases
        {
            get { return mTrainCases; }
            set
            {
                if (value == mTrainCases) return;
                mTrainCases = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<InputOutputPair> mTrainCasesNormalised;
        [XmlIgnore]
        public ObservableCollection<InputOutputPair> TrainCasesNormalised
        {
            get { return mTrainCasesNormalised; }
            set
            {
                if (value == mTrainCasesNormalised) return;
                mTrainCasesNormalised = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<List<double>> mTestCases;
        public ObservableCollection<List<double>> TestCases
        {
            get { return mTestCases; }
            set
            {
                if (value == mTestCases) return;
                mTestCases = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<List<double>> mTestCasesNormalised;
        [XmlIgnore]
        public ObservableCollection<List<double>> TestCasesNormalised
        {
            get { return mTestCasesNormalised; }
            set
            {
                if (value == mTestCasesNormalised) return;
                mTestCasesNormalised = value;
                OnPropertyChanged();
            }
        }

        public static ExampleSettings Load(string aFilePath)
        {
            using (var lSr = new StreamReader(File.OpenRead(aFilePath)))
            {
                var lSerializer = new XmlSerializer(typeof(ExampleSettings));
                var lResult = lSerializer.Deserialize(lSr) as ExampleSettings;
                
                return lResult;
            }
        }
        public void Save(string aFilePath)
        {
            using (var lSw = new StreamWriter(File.Create(aFilePath)))
            {
                var lSerializer = new XmlSerializer(typeof(ExampleSettings));
                lSerializer.Serialize(lSw, this);
            }
        }
    }
}
