﻿using GeneticAlgorithmLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingAppConsole
{
	public class Member : FitnessPopulationMember<Dna, FitnessArgument, object>
	{
		public int X { get; set; }
		public int Y { get; set; }

		public override Dna GetDna()
		{
			return new Dna { Value = new int[] { X, Y } };
		}

		public override void ConstructFromDna(Dna aDna)
		{
			X = aDna.Value[0];
			Y = aDna.Value[1];
		}
		public double GetTotalDistance(FitnessArgument aArgument)
		{
			var lX = aArgument.X;
			var lY = aArgument.Y;
			var lTotalDistance = 0.0;
			for (int i = 0; i < lX.Length; i++)
			{
				lTotalDistance += Math.Sqrt((double)(X - lX[i]) * (X - lX[i]) + (double)(Y - lY[i]) * (Y - lY[i]));
			}
			return lTotalDistance;
		}

		protected override double CalculateFitnessInner(FitnessArgument aArgument)
		{
			var lTotalDistance = GetTotalDistance(aArgument);
			var lFitness = 1000 / (lTotalDistance + 1);

			return lFitness;
		}
	}
}
