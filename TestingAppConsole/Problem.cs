﻿using GeneticAlgorithmLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingAppConsole
{
	public class Problem
	{
		public FitnessArgument FitnessArgument { get; set; }
		public double MutationChance { get; set; }
		private Member mLastSolution = null;

		private Random mRandom = new Random();

		public Member Solve(int aIterationCount, int aPopulationSize)
		{
			if (FitnessArgument.X == null) throw new ArgumentNullException("X");
			if (FitnessArgument.Y == null) throw new ArgumentNullException("Y");
			if (aPopulationSize < 2) throw new Exception("Population size must be at least 2 !");
			if (FitnessArgument.X.Length != FitnessArgument.Y.Length) throw new Exception("Number of X and Y coordinates must be equal !");

			var lXMax = FitnessArgument.X.Max();
			var lYMax = FitnessArgument.Y.Max();
			var lXMin = FitnessArgument.X.Min();
			var lYMin = FitnessArgument.Y.Min();

			var lInitialPopulation = new List<Member>();
			for (int i = 0; i < aPopulationSize; i++)
			{
				lInitialPopulation.Add(new Member { X = mRandom.Next(lXMin, lXMax + 1), Y = mRandom.Next(lYMin, lYMax + 1) });
			}

			var lSolver = new GASolver<Member, Dna, object>(lInitialPopulation)
			{
				PostIterationAction = (aI, aP) =>
					{
						var lTotalFitness = aP.Select(a => a.CalculateFitness(FitnessArgument)).Sum();
						var lBestFitness = aP.OrderByDescending(a => a.CalculateFitness(FitnessArgument)).First();
						Console.WriteLine("Best fitness: {0}, distance: {1}, pos.: [{2}, {3}]", lBestFitness.Fitness, lBestFitness.GetTotalDistance(FitnessArgument), lBestFitness.X, lBestFitness.Y);
						Console.WriteLine("Total fitness: {0}", lTotalFitness);
						//Console.WriteLine("Iteration {0}", aI);
						//foreach (var lMember in aP)
						//{
						//	Console.WriteLine("D: {0}, F: {1}, C: [{2}, {3}]", lMember.GetTotalDistance(X, Y), lMember.GetFitness(X, Y), lMember.X, lMember.Y);
						//}
						//Console.WriteLine("__________________________________________");
					}
			};
			var lResultPopulation = lSolver.Solve(aIterationCount, Select, Cross, Mutate, null);

			var lBest = lResultPopulation.OrderBy(a => a.GetTotalDistance(FitnessArgument)).First();
			mLastSolution = lBest;

			return lBest;
		}
		public void Export(string aPath)
		{
			if (mLastSolution == null) throw new Exception("Problem was not solved yet! Solve the problem first.");

			using (var lSw = new StreamWriter(File.Create(aPath)))
			{
				var lSeparator = ",";
				lSw.WriteLine(mLastSolution.X + lSeparator + mLastSolution.Y);

				for (int i = 0; i < FitnessArgument.X.Length; i++)
				{
					lSw.WriteLine(FitnessArgument.X[i] + lSeparator + FitnessArgument.Y[i]);
				}
			}
		}

		private Tuple<Member, Member> Select(List<Member> aMembers, int aSelectionIndex)
		{
			if (aSelectionIndex == 0)
			{
				var lTwoFittest = aMembers.OrderByDescending(a => a.CalculateFitness(FitnessArgument)).Take(2).ToList();
				return new Tuple<Member, Member>(lTwoFittest[0], lTwoFittest[1]);
			}

			var lTotalFitness = aMembers.Select(a => a.CalculateFitness(FitnessArgument)).Sum();

			var lRand1 = mRandom.NextDouble() * lTotalFitness;
			var lMember1Index = 0;
			while (lMember1Index < aMembers.Count && lRand1 > aMembers[lMember1Index].Fitness)
			{
				lRand1 -= aMembers[lMember1Index].Fitness;
				lMember1Index++;
			}


			var lRand2 = mRandom.NextDouble() * lTotalFitness;
			var lMember2Index = 0;
			while (lMember2Index < aMembers.Count && lRand2 > aMembers[lMember2Index].Fitness)
			{
				lRand2 -= aMembers[lMember2Index].Fitness;
				lMember2Index++;
			}

			return new Tuple<Member, Member>(aMembers[lMember1Index], aMembers[lMember2Index]);
		}
		private Tuple<Dna, Dna> Cross(Dna aDna1, Dna aDna2, int aSelectionIndex)
		{
			var lResultDna1 = new Dna { Value = new int[2] };
			var lResultDna2 = new Dna { Value = new int[2] };

			if (aSelectionIndex == 0)
			{
				lResultDna1.Value = aDna1.Value;
				lResultDna2.Value = aDna2.Value;
				return new Tuple<Dna, Dna>(lResultDna1, lResultDna2);
			}

			var lBytes1 = new byte[aDna1.Value.Length * sizeof(int)];
			var lBytes2 = new byte[aDna2.Value.Length * sizeof(int)];

			Buffer.BlockCopy(aDna1.Value, 0, lBytes1, 0, aDna1.Value.Length);
			Buffer.BlockCopy(aDna2.Value, 0, lBytes2, 0, aDna2.Value.Length);

			var lResultBytes = CrossingFunctions.Cross(lBytes1, lBytes2);

			Buffer.BlockCopy(lResultBytes.Item1, 0, lResultDna1.Value, 0, lResultBytes.Item1.Length);
			Buffer.BlockCopy(lResultBytes.Item2, 0, lResultDna2.Value, 0, lResultBytes.Item1.Length);

			return new Tuple<Dna, Dna>(lResultDna1, lResultDna2);
		}
		public Dna Mutate(Dna aDna, int aSelectionIndex, int aMutationIndex)
		{
			if (aSelectionIndex == 0) return new Dna { Value = aDna.Value };

			if (mRandom.NextDouble() > MutationChance) new Dna { Value = aDna.Value };

			var lPosition = mRandom.Next(64);
			var lResultDna = new Dna { Value = new int[2] };
			if (lPosition < 32)
			{
				var lMask = 1 << (32 - 1 - lPosition);
				lResultDna.Value[0] = aDna.Value[0] ^ lMask;
				lResultDna.Value[1] = aDna.Value[1];
			}
			else
			{
				var lMask = 1 << (64 - 1 - lPosition);
				lResultDna.Value[0] = aDna.Value[0];
				lResultDna.Value[1] = aDna.Value[1] ^ lMask;
			}
			return lResultDna;
		}
	}
}
