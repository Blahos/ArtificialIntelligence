﻿using GeneticAlgorithmLibrary;
using MathNet.Numerics.LinearAlgebra.Double;
using NeuralNetworkLibrary;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingAppConsole
{
	class Program
	{
		static void Main(string[] args)
		{
            var lNetwork = NeuralNetworkFactory.Construct3LayersCompleteNetwork(2, 2, 1);
            var lRandom = new Random();
            lNetwork.RandomiseBiases(aRandom: lRandom);
            lNetwork.RandomiseSteepnesses(aRandom: lRandom);
            lNetwork.RandomiseWeights(aRandom: lRandom);

            //lNetwork.InnerNodes[0].Bias = -0.5;
            //lNetwork.InnerNodes[0].InputConnections[0].Weight = 1;
            //lNetwork.InnerNodes[0].InputConnections[1].Weight = 0;
            //lNetwork.InnerNodes[0].Steepness = 100;

            //lNetwork.InnerNodes[1].Bias = -0.5;
            //lNetwork.InnerNodes[1].InputConnections[0].Weight = 0;
            //lNetwork.InnerNodes[1].InputConnections[1].Weight = 1;
            //lNetwork.InnerNodes[1].Steepness = 10000;

            //lNetwork.InnerNodes[2].Bias = -0.5;
            //lNetwork.InnerNodes[2].InputConnections[0].Weight = 1;
            //lNetwork.InnerNodes[2].InputConnections[1].Weight = 1;
            //lNetwork.InnerNodes[2].Steepness = 10000;

            //lNetwork = NeuralNetwork.FromTextFile("a.txt");

            var lTrainCases = new List<Tuple<List<double>, double>>();

            var lN = 100;

            for (int i = 0; i < lN; i++)
            {
                var lX = lRandom.NextDouble();
                var lY = lRandom.NextDouble();

                var lOutput = ((lX > 0.5) || (lY > 0.5)) ? 1.0 : 0.0;
                lTrainCases.Add(new Tuple<List<double>, double>(new List<double> { lX, lY }, lOutput));
            }
            //lTrainCases.Add(new Tuple<List<double>, double>(new List<double> { 0.45, 0.45 }, 0));
            //lTrainCases.Add(new Tuple<List<double>, double>(new List<double> { 0.49, 0.51 }, 1));
            //lTrainCases.Add(new Tuple<List<double>, double>(new List<double> { 0.51, 0.49 }, 1));
            //lTrainCases.Add(new Tuple<List<double>, double>(new List<double> { 0.51, 0.51 }, 1));
            var lRounds = 100000;
            var lTrainStep = 0.02;
            for (int iRound = 0; iRound < lRounds; iRound++)
            {
                for (int i = 0; i < lTrainCases.Count; i++)
                {
                    var lError1 = lNetwork.GetError(lTrainCases[i].Item1, lTrainCases[i].Item2);
                    if (lError1 > 1e-5)
                    {
                        //Console.WriteLine("Training on {0} {1}", lTrainCases[i].Item1[0], lTrainCases[i].Item1[1]);
                        lNetwork.Train(lTrainCases[i].Item1, lTrainCases[i].Item2, lTrainStep);
                    }
                    var lError2 = lNetwork.GetError(lTrainCases[i].Item1, lTrainCases[i].Item2);
                    if (lError2 > lError1)
                    {
                        Console.WriteLine("Error increse on {0} {1}", lTrainCases[i].Item1[0], lTrainCases[i].Item1[1]);
                    }
                }
            }

            //lNetwork.SaveToTextFile("a.txt");
        }
        private static void TestSomething()
        {

            //var lRandom = new Random();
            //var lTrainingSet = new List<Tuple<List<double>, double>>();
            //var lTrainingStepSize = 1;

            //var lTrainingCases = 100;
            //for (int i = 0; i < lTrainingCases; i++)
            //{
            //    var lX = lRandom.NextDouble();
            //    var lY = lRandom.NextDouble();
            //    var lValue = (double)(lY > lX ? 1 : 0);

            //    lTrainingSet.Add(new Tuple<List<double>, double>(new List<double> { lX, lY }, lValue));
            //}

            //var lNetwork = Factory.Construct2LayersCompleteNetwork(2, 1);
            //lNetwork.RandomiseBiases();
            //lNetwork.RandomiseWeights();

            //var lTrainingCycles = 50000;

            //Console.WriteLine("Errors before training: ");
            //double lTotalErrorBefore = 0;
            //for (int i = 0; i < lTrainingCases; i++)
            //{
            //    var lError = lNetwork.GetError(
            //            lTrainingSet[i].Item1,
            //            lTrainingSet[i].Item2);
            //    lTotalErrorBefore += lError;
            //    Console.WriteLine($"Case {i + 1}: {lError}");
            //}
            //Console.WriteLine($"Total: {lTotalErrorBefore}");
            //Console.WriteLine($"Weights: {lNetwork.Connections[0].Weight}, {lNetwork.Connections[1].Weight}");
            //Console.WriteLine($"Bias: {lNetwork.InnerNodes[0].Bias}");

            //for (var iCycle = 0; iCycle < lTrainingCycles; iCycle++)
            //{
            //    for (var iCase = 0; iCase < lTrainingCases; iCase++)
            //    {
            //        lNetwork.Train(
            //            lTrainingSet[iCase].Item1, 
            //            lTrainingSet[iCase].Item2, 
            //            lTrainingStepSize);
            //    }
            //}
            //Console.WriteLine("Errors after training: ");
            //var lTotalErrorAfter = 0.0;

            //for (int i = 0; i < lTrainingCases; i++)
            //{
            //    var lError = lNetwork.GetError(
            //            lTrainingSet[i].Item1,
            //            lTrainingSet[i].Item2);
            //    lTotalErrorAfter += lError;
            //    Console.WriteLine($"Case {i + 1}: {lError}");
            //}
            //Console.WriteLine($"Total: {lTotalErrorAfter}");
            //Console.WriteLine($"Error ratio: {lTotalErrorAfter / lTotalErrorBefore}");
            //Console.WriteLine($"Weights: {lNetwork.Connections[0].Weight}, {lNetwork.Connections[1].Weight}");
            //Console.WriteLine($"Bias: {lNetwork.InnerNodes[0].Bias}");
        }
        private static void TestNeuralNetworkConstruction()
		{
			var lNetwork = NeuralNetworkFactory.Construct3LayersCompleteNetwork(9, 4, 2);
			lNetwork.SaveToTextFile("nn942.txt");
		}
		private static void TestNeuralNetworkOptimization()
		{
			var lNetwork = NeuralNetwork.FromTextFile("test_network.txt");
            //var lNetwork = CreateSimpleNetwork();

			var lCount = 100000;
			var lSw = new Stopwatch();
			lSw.Start();
			for (int i = 0; i < lCount; i++)
			{
				var lBytes = lNetwork.ToBinary();
				var lNetworkRecovered = NeuralNetwork.FromBinary(lBytes);
			}
			lSw.Stop();
			Console.WriteLine("No optimization time: {0}ms", lSw.ElapsedMilliseconds);
			lSw.Restart();
			var lStructurePart = lNetwork.GetStructurePart();
			for (int i = 0; i < lCount; i++)
			{
				var lBytes = lNetwork.ToBinary(lStructurePart);
				var lNetworkRecovered = NeuralNetwork.FromBinary(lBytes);
			}
			lSw.Stop();
			Console.WriteLine("Structure part optimization time: {0}ms", lSw.ElapsedMilliseconds);
		}

		private static void ExecuteTestGeneticAlgorithmProblem()
		{
			var lProblem = new Problem()
			{
				FitnessArgument = new FitnessArgument
				{
					X = new[] { 20, 10, 0, 50, 100, 10, 100 },
					Y = new[] { 0, 0, 10, 50, 10, 50, 40 }
				},
				MutationChance = 0.02
			};

			Stopwatch lSw = new Stopwatch();
			lSw.Start();
			var lResult = lProblem.Solve(150, 40);
			lSw.Stop();

			Console.WriteLine("Solution: [{0}, {1}], fitness: {2}", lResult.X, lResult.Y, lResult.CalculateFitness(lProblem.FitnessArgument));

			lProblem.Export("Problem_solution.csv");
			Console.WriteLine("Problem exported.");
			Console.WriteLine("Solving time: {0}ms", lSw.ElapsedMilliseconds);
		}
		private static void PrintBinary(byte aByte)
		{
			PrintBinary(new[] { aByte });
		}
		private static void PrintBinary(byte[] aArray)
		{
			for (int i = 0; i < aArray.Length; i++)
			{
				Console.Write(Convert.ToString(aArray[i], 2).PadLeft(8, '0') + " ");
			}
			Console.WriteLine();
		}
		private static NeuralNetwork CreateSimpleNetwork()
		{
			var lNetwork = new NeuralNetwork();

			for (int i = 0; i < 3; i++) lNetwork.InnerNodes.Add(new NodeInner());
			for (int i = 0; i < 2; i++) lNetwork.InputNodes.Add(new NodeInput());

			for (int i = 0; i < 6; i++) lNetwork.Connections.Add(new Connection());
			for (int i = 0; i < 1; i++) lNetwork.Outputs.Add(new ConnectionOutput());

			lNetwork.Outputs[0].InputNode = lNetwork.InnerNodes[2];
			lNetwork.InnerNodes[2].InputConnections.Add(lNetwork.Connections[5]);
			lNetwork.InnerNodes[2].InputConnections.Add(lNetwork.Connections[4]);
			lNetwork.Connections[5].InputNode = lNetwork.InnerNodes[1];
			lNetwork.Connections[4].InputNode = lNetwork.InnerNodes[0];
			lNetwork.InnerNodes[1].InputConnections.Add(lNetwork.Connections[3]);
			lNetwork.InnerNodes[1].InputConnections.Add(lNetwork.Connections[1]);
			lNetwork.InnerNodes[0].InputConnections.Add(lNetwork.Connections[2]);
			lNetwork.InnerNodes[0].InputConnections.Add(lNetwork.Connections[0]);
			lNetwork.Connections[3].InputNode = lNetwork.InputNodes[1];
			lNetwork.Connections[1].InputNode = lNetwork.InputNodes[0];
			lNetwork.Connections[2].InputNode = lNetwork.InputNodes[1];
			lNetwork.Connections[0].InputNode = lNetwork.InputNodes[0];

			lNetwork.InnerNodes[2].Bias = 0;

			return lNetwork;
		}
	}
}
