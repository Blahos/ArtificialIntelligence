﻿using System;
using System.Collections.Generic;

namespace Library
{
	public static class ExtensionMethods
	{
		public static TItem MaxBy<TItem, TComparedValue>(this IEnumerable<TItem> aItems, Func<TItem, TComparedValue> aSelector)
			where TComparedValue : IComparable
		{
			var lBestItem = default(TItem);
			var lIsAssigned = false;
			var lBestValue = default(TComparedValue);
			foreach (var lItem in aItems)
			{
				var lValue = aSelector(lItem);
				if (!lIsAssigned || lValue.CompareTo(lBestValue) > 0)
				{
					lBestItem = lItem;
					lBestValue = lValue;
					lIsAssigned = true;
				}
			}
			return lBestItem;
		}
	}
}
