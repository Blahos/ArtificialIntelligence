﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Library.Ingeteam.Algorithms
{
	public static class ListComparsion
	{
		public static void Compare<T1, T2, TKey>(
			IEnumerable<T1> aList1,
			IEnumerable<T2> aList2,
			Func<T1, TKey> aKeySelector1,
			Func<T2, TKey> aKeySelector2,
			Action<T1> aList1Only,
			Action<T2> aList2Only,
			Action<T1, T2> aBothLists)
			where TKey : IComparable<TKey>
		{
			var lList1 = aList1.ToList();
			var lList2 = aList2.ToList();
			
			lList1.Sort((aObj1, aObj2) => aKeySelector1.Invoke(aObj1).CompareTo(aKeySelector1.Invoke(aObj2)));
			lList2.Sort((aObj1, aObj2) => aKeySelector2.Invoke(aObj1).CompareTo(aKeySelector2.Invoke(aObj2)));

			var i1 = 0;
			int i2 = 0;

			while (i1 < lList1.Count || i2 < lList2.Count)
			{
				if (i1 < lList1.Count && i2 < lList2.Count)
				{
					// objekty listu 1 i 2

					var lObj1 = lList1[i1];
					var lObj2 = lList2[i2];

					var lComparsionResult = aKeySelector1.Invoke(lObj1).CompareTo(aKeySelector2.Invoke(lObj2));
					if (lComparsionResult == 0)
					{
						// stejne ID v obou listech
						aBothLists.Invoke(lObj1, lObj2);
						i1++;
						i2++;
					}
					else if (lComparsionResult < 0)
					{
						aList1Only.Invoke(lObj1);
						i1++;
					}
					else
					{
						aList2Only.Invoke(lObj2);
						i2++;
					}
				}
				else if (i1 < lList1.Count)
				{
					// pouze objekty listu 1
					aList1Only.Invoke(lList1[i1]);
					i1++;
				}
				else
				{
					// pouze objekty listu 2
					aList2Only.Invoke(lList2[i2]);
					i2++;
				}
			}
		}

		public static void CompareNotSort<T1, T2, TKey>(
			IEnumerable<T1> aList1,
			IEnumerable<T2> aList2,
			Func<T1, TKey> aKeySelector1,
			Func<T2, TKey> aKeySelector2,
			Action<T1> aList1Only,
			Action<T2> aList2Only,
			Action<T1, T2> aBothLists)
			where TKey : IEquatable<TKey>
		{
			var lList1 = aList1.ToList();
			var lList2 = aList2.ToList();

			var lList1Keys = lList1.Select(a => aKeySelector1(a)).ToList();
			var lList2Keys = lList2.Select(a => aKeySelector2(a)).ToList();

			for (int i = 0; i < lList1Keys.Count; i++)
			{
				var lList1Key = lList1Keys[i];
				bool lFound = false;
				for (int j = 0; j < lList2Keys.Count; j++)
				{
					var lList2Key = lList2Keys[j];
					if (lList1Key.Equals(lList2Key))
					{
						aBothLists.Invoke(lList1[i], lList2[j]);
						lFound = true;
						break;
					}
				}
				if (!lFound)
					aList1Only.Invoke(lList1[i]);
			}
			for (int i = 0; i < lList2Keys.Count; i++)
			{
				var lList2Key = lList2Keys[i];
				bool lFound = false;
				for (int j = 0; j < lList1Keys.Count; j++)
				{
					var lList1Key = lList1Keys[j];
					if (lList2Key.Equals(lList1Key))
					{
						lFound = true;
						break;
					}
				}
				if (!lFound)
					aList2Only(lList2[i]);
			}
		}
	}
}
