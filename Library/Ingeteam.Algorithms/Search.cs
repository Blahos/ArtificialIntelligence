﻿using System;
using System.Collections.Generic;

namespace Library.Ingeteam.Algorithms
{
	public static class Search
	{
		public static int BinarySearch<T, TKey>(this IList<T> aList, TKey aValue, Func<T, TKey> aSelector)
			where TKey : IComparable<TKey>
		{
			if (aList.Count == 0)
			{
				return -1;
			}

			var iMin = 0;
			var iMax = aList.Count - 1;

			if (aValue.CompareTo(aSelector(aList[0])) < 0)
			{
				return ~0;
			}

			if (aValue.CompareTo(aSelector(aList[iMax])) > 0)
			{
				return ~aList.Count;
			}

			while (iMax > iMin + 1)
			{
				var iMid = (iMax + iMin) / 2;

				var lResult = aValue.CompareTo(aSelector(aList[iMid]));
				if (lResult < 0)
				{
					iMax = iMid;
				}
				else if (lResult > 0)
				{
					iMin = iMid;
				}
				else
				{
					return iMid;
				}
			}

			if (aValue.CompareTo(aSelector(aList[iMin])) == 0)
			{
				return iMin;
			}

			if (aValue.CompareTo(aSelector(aList[iMax])) == 0)
			{
				return iMax;
			}
			return ~iMax;
		}

		public static int BinarySearch<T, TKey>(this IList<T> aList, TKey aValue, Func<TKey, T, int> aComparer)
		{
			if (aList.Count == 0)
			{
				return -1;
			}

			var iMin = 0;
			var iMax = aList.Count - 1;

			if (aComparer(aValue, aList[0]) < 0)
			{
				return ~0;
			}

			if (aComparer(aValue, aList[iMax]) > 0)
			{
				return ~aList.Count;
			}

			while (iMax > iMin + 1)
			{
				var iMid = (iMax + iMin) / 2;

				var lResult = aComparer(aValue, aList[iMid]);
				if (lResult < 0)
				{
					iMax = iMid;
				}
				else if (lResult > 0)
				{
					iMin = iMid;
				}
				else
				{
					return iMid;
				}
			}

			if (aComparer(aValue, aList[iMin]) == 0)
			{
				return iMin;
			}

			if (aComparer(aValue, aList[iMax]) == 0)
			{
				return iMax;
			}
			return ~iMax;
		}

		public static int BinarySearch<T, TKey1, TKey2>(this IList<T> aList, TKey1 aValue1, TKey2 aValue2, Func<TKey1, TKey2, T, int> aComparer)
		{
			if (aList.Count == 0)
			{
				return -1;
			}

			var iMin = 0;
			var iMax = aList.Count - 1;

			if (aComparer(aValue1, aValue2, aList[0]) < 0)
			{
				return ~0;
			}

			if (aComparer(aValue1, aValue2, aList[iMax]) > 0)
			{
				return ~aList.Count;
			}

			while (iMax > iMin + 1)
			{
				var iMid = (iMax + iMin) / 2;

				var lResult = aComparer(aValue1, aValue2, aList[iMid]);
				if (lResult < 0)
				{
					iMax = iMid;
				}
				else if (lResult > 0)
				{
					iMin = iMid;
				}
				else
				{
					return iMid;
				}
			}

			if (aComparer(aValue1, aValue2, aList[iMin]) == 0)
			{
				return iMin;
			}

			if (aComparer(aValue1, aValue2, aList[iMax]) == 0)
			{
				return iMax;
			}
			return ~iMax;
		}

		public static int BinarySearch<T, TKey1, TKey2, TKey3>(this IList<T> aList, TKey1 aValue1, TKey2 aValue2, TKey3 aValue3, Func<TKey1, TKey2, TKey3, T, int> aComparer)
		{
			if (aList.Count == 0)
			{
				return -1;
			}

			var iMin = 0;
			var iMax = aList.Count - 1;

			if (aComparer(aValue1, aValue2, aValue3, aList[0]) < 0)
			{
				return ~0;
			}

			if (aComparer(aValue1, aValue2, aValue3, aList[iMax]) > 0)
			{
				return ~aList.Count;
			}

			while (iMax > iMin + 1)
			{
				var iMid = (iMax + iMin) / 2;

				var lResult = aComparer(aValue1, aValue2, aValue3, aList[iMid]);
				if (lResult < 0)
				{
					iMax = iMid;
				}
				else if (lResult > 0)
				{
					iMin = iMid;
				}
				else
				{
					return iMid;
				}
			}

			if (aComparer(aValue1, aValue2, aValue3, aList[iMin]) == 0)
			{
				return iMin;
			}

			if (aComparer(aValue1, aValue2, aValue3, aList[iMax]) == 0)
			{
				return iMax;
			}
			return ~iMax;
		}

	}

}
