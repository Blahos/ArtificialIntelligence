﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Library
{
    public class ActionQueue
    {
        private Timer mTimer;
        private readonly object mLock = new object();
        private readonly int mCapacity;
        private readonly Queue<Action> mQueue;
        public ActionQueue(int aCapacity)
        {
            mQueue = new Queue<Action>(aCapacity);
            mCapacity = aCapacity;

            mTimer = new Timer(TimerCallback);
            mTimer.Change(0, Timeout.Infinite);
        }
        public void Add(Action aAction)
        {
            if (aAction == null) return;
            lock (mLock)
            {
                if (mQueue.Count > mCapacity) mQueue.Dequeue();
                mQueue.Enqueue(aAction);
            }
        }
        private void TimerCallback(object aParam)
        {
            int? lRemainingCount = null;
            try
            {
                Action lNextAction = null;
                lock (mLock)
                {
                    if (mQueue.Count > 0)
                        lNextAction = mQueue.Dequeue();

                    lRemainingCount = mQueue.Count;
                }

                if (lNextAction != null)
                {
                    try
                    {
                        lNextAction.Invoke();
                    }
                    catch (Exception lEx)
                    {
                        throw new Exception("Exception while invoking an action!", lEx);
                    }
                }
            }
            finally
            {
                if (lRemainingCount == null || lRemainingCount == 0)
                    mTimer.Change(50, Timeout.Infinite);
                else
                    mTimer.Change(0, Timeout.Infinite);
            }
        }
    }
}
