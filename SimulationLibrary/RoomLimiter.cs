﻿using LibraryWpf;
using SimulationLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SimulationLibrary
{
	public class RoomLimiter : BaseViewModel
	{
		public Thickness LowerLimits
		{
			get { return new Thickness(XMin, YMin, 0, 0); }
		}
		private double mXMin;
		public double XMin
		{
			get { return mXMin; }
			set
			{
				if (mXMin == value) return;
				mXMin = value;
				OnPropertyChanged();
			}
		}
		private double mXMax;
		public double XMax
		{
			get { return mXMax; }
			set
			{
				if (mXMax == value) return;
				mXMax = value;
				OnPropertyChanged();
			}
		}
		private double mYMin;
		public double YMin
		{
			get { return mYMin; }
			set
			{
				if (mYMin == value) return;
				mYMin = value;
				OnPropertyChanged();
			}
		}
		private double mYMax;
		public double YMax
		{
			get { return mYMax; }
			set
			{
				if (mYMax == value) return;
				mYMax = value;
				OnPropertyChanged();
			}
		}

        public RoomLimiter() : this(0, 0) { XMin = -32; YMin = -32; }
        public RoomLimiter(double aXMax, double aYMax)
        {
            XMax = aXMax;
            YMax = aYMax;
            XMin = 0;
            YMin = 0;
        }
	}
}
