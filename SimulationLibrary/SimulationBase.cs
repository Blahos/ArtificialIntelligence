﻿using LibraryWpf;
using SimulationLibrary.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimulationLibrary
{
    /// <summary>
    /// Základní třída pro simulace. 
    /// Implementuje simulační smyčku, možnost jí zastavit a nastavit její rychlost.
    /// </summary>
    public abstract class SimulationBase : BaseViewModel
    {
        private static readonly List<SimulationSpecification> mSimulationList = 
            new List<SimulationSpecification>();
        // Just a dummy parameter. Unused, but keep it here.
        // It is used as a dummy variable for manual activation (Activate() method) of this class 
        // (to force initialisation of its static variables)
        private static bool mIsActivated = false;
        private readonly Timer mStepTimer;
        private DateTime mLastExecutionEndTime = DateTime.Now;

        public static ReadOnlyCollection<SimulationSpecification> SimulationList
        {
            get { return new ReadOnlyCollection<SimulationSpecification>(mSimulationList); }
        }

        public event Action<bool> IsPausedChanged;

        private readonly List<DateTime> mFrameTimes = new List<DateTime>();
        private double mFrameRate;
        public double FrameRate
        {
            get { return mFrameRate; }
            private set
            {
                if (mFrameRate == value) return;
                mFrameRate = value;
                OnPropertyChanged();
            }
        }

        private bool mIsPaused = true;
        public bool IsPaused
        {
            get { return mIsPaused; }
            set
            {
                if (mIsPaused == value) return;
                mIsPaused = value;
                OnPropertyChanged();
                if (IsPausedChanged != null) IsPausedChanged.Invoke(mIsPaused);
            }
        }

        private long mTicks = 0;
        public long Ticks
        {
            get { return mTicks; }
            private set
            {
                if (value == mTicks) return;
                mTicks = value;
                OnPropertyChanged();
            }
        }
        private double mSpeedCoefficient = 1;
        public double SpeedCoefficient
        {
            get { return mSpeedCoefficient; }
            set
            {
                if (mSpeedCoefficient == value) return;
                mSpeedCoefficient = value;
                OnPropertyChanged();
            }
        }

        private bool mSpeedLimit = true;
        /// <summary>
        /// No limitations for tick timer speed,
        /// i.e. setting 0 for the due time.
        /// The class has some sort of a shortcut for this value 
        /// and probably fires up again immediately.
        /// </summary>
        public bool SpeedLimit
        {
            get { return mSpeedLimit; }
            set
            {
                if (value == mSpeedLimit) return;
                mSpeedLimit = value;
                OnPropertyChanged();
            }
        }

        public static SimulationBase CreateSimulation(string aSimulationID)
        {
            var lSimulation = mSimulationList.FirstOrDefault(a => a.SimulationAttribute.ID == aSimulationID);
            
            if (lSimulation == null)
                throw new ArgumentException(String.Format("Unknown simulation ID \"{0}\"! Have you assigned the SimulationAttribute to your simulation class?", aSimulationID));
            if (!(lSimulation.SimulationType.IsSubclassOf(typeof(SimulationBase))))
                throw new ArgumentException(String.Format("The type \"{0}\" is not a subclass of SimulationBase!", lSimulation.SimulationType));

            var lInstance = Activator.CreateInstance(lSimulation.SimulationType) as SimulationBase;
            return lInstance;
        }

        protected abstract byte[] SaveInternal();
        protected abstract void LoadInternal(byte[] aArray);

        public SimulationBase(bool aIsPaused = true)
        {
            mStepTimer = new Timer(a => StepTimerTick(a, null));
            // Když nebudu chtít aby se simulace hned spustila. 
            // Nastavení IsPaused v inicializátoru by bylo pozdě.
            IsPaused = aIsPaused;
            var lDelayMs = 100;
            mLastExecutionEndTime = DateTime.Now;
            mStepTimer.Change(lDelayMs, Timeout.Infinite);
        }
        static SimulationBase()
        {
            foreach (var nAssembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                var lSimulationTypes = nAssembly.GetTypes()
                    .Where(a => a.IsSubclassOf(typeof(SimulationBase)))
                    .Where(a => !a.IsAbstract)
                    .Where(a => Attribute.GetCustomAttribute(a, typeof(SimulationAttribute)) != null);

                foreach (var nType in lSimulationTypes)
                {
                    var lAttribute = (Attribute.GetCustomAttribute(nType, typeof(SimulationAttribute)) as SimulationAttribute);
                    try
                    {
                        var lExistingSimulation = mSimulationList.FirstOrDefault(a => a.SimulationAttribute.ID == lAttribute.ID);
                        if (lExistingSimulation != null)
                            throw new Exception(String.Format("Can not use simulation \"{0}\" because the simulation \"{1}\" has the same ID (\"{2}\")!", 
                                nType.FullName, lExistingSimulation.SimulationType.FullName, lAttribute.ID));

                        mSimulationList.Add(new SimulationSpecification(nType, lAttribute));
                    }
                    catch (Exception lEx)
                    {
                        Console.WriteLine("Exception rised when adding simulation of type {0}", nType.Name);
                        Console.WriteLine(lEx.Message);
                    }
                }
            }
            Console.WriteLine("Available simulations:");
            foreach (var nSimulation in mSimulationList)
            {
                Console.WriteLine(nSimulation.SimulationType.FullName);
            }
        }
        /// <summary>
        /// Does nothing, it's only used to manually invoke the static constructor
        /// to be able to access the simulation type list.
        /// </summary>
        public static void Activate()
        {
            mIsActivated = true;
        }
        private void StepTimerTick(object sender, EventArgs e)
        {
            var lTickStartTime = DateTime.Now;

            var lTimer = sender as Timer;
            lTimer.Change(Timeout.Infinite, Timeout.Infinite);

            if (IsPaused)
            {
                lTimer.Change(100, Timeout.Infinite);
                return;
            }

            //var lTimeSinceLastExecution = lNewLastExecutionTime - mLastExecutionEndTime;

            try
            {
                TimerTick();
            }
            catch (Exception lEx)
            {
                Console.WriteLine("Error executing tick function in simulation timer! {0}", lEx.Message);
            }

            UpdateFrameRate();
            Ticks++;

            var lSpeedLimit = mSpeedLimit;
            if (!lSpeedLimit)
            {
                lTimer.Change(0, Timeout.Infinite);
                return;
            }

            // Just an attempt to control the speed of the timer.
            // But since the Timer class is not really precise, it's just approximate
            var lFullSpeedFPS = 60;
            var lSpeedCoeff = mSpeedCoefficient;
            var lTargetFPS = lSpeedCoeff * lFullSpeedFPS;
            var lMsPerFrame = 1000.0 / lTargetFPS;

            var lTickEndTime = DateTime.Now;
            mLastExecutionEndTime = lTickEndTime;

            var lNewDueTime = Math.Max(lMsPerFrame - (lTickEndTime - lTickStartTime).TotalMilliseconds, 0);
            var lNewDueTimeInt = (int)Math.Round(lNewDueTime);

            lTimer.Change(lNewDueTimeInt, Timeout.Infinite);
        }
        protected abstract void TimerTick();

        private void UpdateFrameRate()
        {
            var lCurrentFrameTime = DateTime.Now;
            mFrameTimes.Add(lCurrentFrameTime);
            while (mFrameTimes.Count > 0 && mFrameTimes[0] < lCurrentFrameTime.AddSeconds(-3))
            {
                mFrameTimes.RemoveAt(0);
            }
            if (mFrameTimes.Count > 1)
            {
                FrameRate = mFrameTimes.Count / (mFrameTimes[mFrameTimes.Count - 1] - mFrameTimes[0]).TotalMilliseconds * 1000;
            }
            else FrameRate = 0;
        }

        public void Save(string aFilePath)
        {
            var lSimulationIDAttribute = Attribute.GetCustomAttribute(this.GetType(), typeof(SimulationAttribute)) as SimulationAttribute;
            if (lSimulationIDAttribute == null) throw new Exception("This simulation type has not set SimulationIDAttribute value !");

            var lID = lSimulationIDAttribute.ID;
            
            var lBytes = SaveInternal();

            using (var lBw = new BinaryWriter(File.Create(aFilePath), Encoding.UTF8))
            {
                lBw.Write(lID);
                lBw.Write(lBytes.Length);
                lBw.Write(lBytes);
            }
        }
        public static SimulationBase Load(string aFilePath)
        {
            string lId;
            byte[] lBytes;
            using (var lBr = new BinaryReader(File.OpenRead(aFilePath), Encoding.UTF8))
            {
                lId = lBr.ReadString();

                var lBytesCount = lBr.ReadInt32();
                lBytes = lBr.ReadBytes(lBytesCount);
            }

            var lInstance = CreateSimulation(lId);
            lInstance.Initialise();

            lInstance.LoadInternal(lBytes);
            return lInstance;
        }
        public virtual void Initialise()
        {
            InitialiseCore();
        }
        /// All the necessary initialisations.
        /// Everything that should be done after creating the simulation instance, be it 
        /// after creating it directly or loading it (this is called before the load command)
        /// </summary>
        private void InitialiseCore()
        {
            IsPaused = true;
            mLastExecutionEndTime = DateTime.Now;
        }
    }
    /// <summary>
    /// Základní třída pro simulace. 
    /// Implementuje simulační smyčku, možnost jí zastavit a nastavit její rychlost.
    /// </summary>
    public abstract class SimulationBase<TInitData, TData> : SimulationBase, ISimulation
        where TInitData : InitData, new()
        where TData : SimulationData, new()
    {
        public TData Data { get; }
        // this is to satisfy the interface definition (TData Data does not)
        SimulationData ISimulation.Data { get { return Data; } }
		/// <summary>
        /// Available to be called from outside
        /// </summary>
        /// <param name="aInitData"></param>
        public void Initialise(TInitData aInitData)
        {
            base.Initialise();

            var lInitData = aInitData;
            if (lInitData == null)
                lInitData = GetDefaultInitData();

            InitialiseInner(lInitData);
        }
        /// <summary>
        /// Available to be called from outside
        /// </summary>
        public sealed override void Initialise()
        {
            Initialise(null);
        }
        void ISimulation.Initialise(InitData aInitData)
        {
            if (aInitData == null)
                Initialise();

            if (!(aInitData is TInitData))
                throw new ArgumentException("Incorrect type of the initial data argument!");

            Initialise(aInitData as TInitData);    
        }
        protected abstract void InitialiseInner(TInitData aInitData);
        /// <summary>
        /// This should not be returning null
        /// </summary>
        /// <returns></returns>
        public virtual TInitData GetDefaultInitData()
        {
            return new TInitData();
        }
        InitData ISimulation.GetDefaultInitData()
        {
            return GetDefaultInitData();
        }

        public SimulationBase(TData aData, bool aIsPaused = true)
            : base(aIsPaused) => Data = aData;
    }
}
