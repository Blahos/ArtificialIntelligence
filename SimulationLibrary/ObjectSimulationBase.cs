﻿using LibraryWpf;
using SimulationLibrary.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulationLibrary
{
	// Bázová třída pro simulaci nad kolekcí objektů,
	// obsahuje kolekci objektů, ukládá replay (historii stavů).
	public abstract class ObjectSimulationBase<TInitData, TData, TSimInterface> : VisualSimulationBase<TInitData, TData>, IObjectSimulation
        where TInitData : InitData, new()
        where TData : SimulationData, new()
        where TSimInterface : SimulationInterfaceBase
	{
		protected readonly ObservableCollection<BaseObjectViewModel> mObjects = new ObservableCollection<BaseObjectViewModel>();
        private readonly ReadOnlyObservableCollection<BaseObjectViewModel> mObjectReadonly;
        public ReadOnlyObservableCollection<BaseObjectViewModel> Objects
		{
			get { return mObjectReadonly; }
        }
        private readonly TSimInterface mInterfaceToObjects;
        private readonly List<Tuple<double, double, double, double, double>> mTickDurations = new List<Tuple<double, double, double, double, double>>();
        private int mReplayBufferSize;
		public int ReplayBufferSize
		{
			get { return mReplayBufferSize; }
			set
			{
				if (mReplayBufferSize == value) return;
				mReplayBufferSize = value;
				OnPropertyChanged();
			}
		}
		private readonly ObservableCollection<ReadOnlyObservableCollection<BaseObjectViewModel>> mReplayBuffer = new ObservableCollection<ReadOnlyObservableCollection<BaseObjectViewModel>>();
		public ObservableCollection<ReadOnlyObservableCollection<BaseObjectViewModel>> ReplayBuffer
		{
			get { return mReplayBuffer; }
        }
        private RoomLimiter mRoomLimiter;
        public RoomLimiter RoomLimiter
        {
            get { return mRoomLimiter; }
            set
            {
                if (mRoomLimiter == value) return;
                mRoomLimiter = value;
                OnPropertyChanged();
            }
        }

        public ObjectSimulationBase(TData aData, bool aIsPaused = true)
			: base(aData, aIsPaused)
		{
            mObjectReadonly = new ReadOnlyObservableCollection<BaseObjectViewModel>(mObjects);
            mInterfaceToObjects = CreateInterface();
            mObjects.CollectionChanged += ObjectsCollectionChanged;
        }
        public ObjectSimulationBase(bool aIsPaused = true)
            : this(new TData(), aIsPaused) { }
        
		protected virtual void TickStartAction() { }
		protected virtual void TickAfterCollisionAction(List<BaseObjectViewModel> aObjectsToDestroy) { }
		protected virtual void TickEndAction(List<BaseObjectViewModel> aObjectsToDestroy) { }
		protected override void TimerTick()
		{
            Stopwatch lSw1 = new Stopwatch();
            lSw1.Start();
			if (ViewProvider == null)
			{
				Console.WriteLine("Missing view provider!");
				return;
			}
            var lViewService = ViewProvider.ViewService;

			// Tady bude něco specifického (automatická evoluce)
			TickStartAction();

			SaveReplayFrame();

            lSw1.Stop();
            double lEl1 = lSw1.Elapsed.TotalMilliseconds;

            Stopwatch lSw2 = new Stopwatch();
            lSw2.Start();

   //         if (mObjects.Count > 20)
			//	mObjects.AsParallel().ForAll(a => a.Tick());
			//else
			//{
				foreach (var lObject in mObjects)
				{
					lObject.Tick();
				}
            //}

            lSw2.Stop();
            double lEl2 = lSw2.Elapsed.TotalMilliseconds;

            Stopwatch lSw3 = new Stopwatch();
            lSw3.Start();

            foreach (var lObject in mObjects)
			{
				foreach (var lObject2 in mObjects)
				{
					if (lObject != lObject2)
					{
						lObject.SolveCollision(lObject2);
					}
				}
            }
            lSw3.Stop();
            double lEl3 = lSw3.Elapsed.TotalMilliseconds;

            Stopwatch lSw4 = new Stopwatch();
            lSw4.Start();

            var lObjectsToDestroy = new List<BaseObjectViewModel>();
			foreach (var lObject in mObjects)
			{
				if (lObject.IsDestroyed) lObjectsToDestroy.Add(lObject);
			}


            // Tady bude něco specifického (dodatečné smazání jablíček, pokud jich je moc)
            TickAfterCollisionAction(lObjectsToDestroy);

			lViewService.Execute(() =>
			{
				foreach (var lObjectToDestroy in lObjectsToDestroy)
				{
					mObjects.Remove(lObjectToDestroy);
				}
			});

            lSw4.Stop();
            double lEl4 = lSw4.Elapsed.TotalMilliseconds;

            Stopwatch lSw5 = new Stopwatch();
            lSw5.Start();

            // Tady bude něco specifického - zvšení počtu snězených jablíček, přebarvení nejlepšího miniona
            TickEndAction(lObjectsToDestroy);

            lSw5.Stop();
            double lEl5 = lSw5.Elapsed.TotalMilliseconds;
            mTickDurations.Add(new Tuple<double, double, double, double, double>(lEl1, lEl2, lEl3, lEl4, lEl5));

            if (mTickDurations.Count > 100000)
            {
                using (var lWriter = new StreamWriter(File.OpenWrite("ticks_dump.txt")))
                {
                    foreach (var lTick in mTickDurations)
                    {
                        lWriter.WriteLine(lTick.Item1 + ", " + lTick.Item2 + ", " + lTick.Item3 + ", " + lTick.Item4 + ", " + lTick.Item5); 
                    }
                }
                mTickDurations.Clear();
            }
        }
		private void SaveReplayFrame()
		{
			var lObjects = new ObservableCollection<BaseObjectViewModel>();

			foreach (var lObject in mObjects)
			{
				lObjects.Add(lObject.GetCopy());
			}

            mReplayBuffer.Add(new ReadOnlyObservableCollection<BaseObjectViewModel>(lObjects));
            while (ReplayBuffer.Count > ReplayBufferSize)
			{
				mReplayBuffer.RemoveAt(0);
			}
		}
        /// <summary>
        /// Don't forget to call this in overriding methods!
        /// i.e. base.InitiliseInner(aInitData);
        /// </summary>
        /// <param name="aInitData"></param>
		protected override void InitialiseInner(TInitData aInitData)
		{
			mObjects.Clear();
			mReplayBuffer.Clear();
			ReplayBufferSize = 120;

            RoomLimiter = new RoomLimiter(800, 600);
		}
        protected abstract TSimInterface CreateInterface();

        private void ObjectsCollectionChanged(object aSender, System.Collections.Specialized.NotifyCollectionChangedEventArgs aE)
        {
            if (aE.NewItems != null)
            {
                foreach (var nItem in aE.NewItems)
                {
                    if (nItem == null) continue;
                    (nItem as BaseObjectViewModel).SetSimulationInterface(mInterfaceToObjects);
                }
            }
            if (aE.OldItems != null)
            {
                foreach (var nItem in aE.OldItems)
                {
                    if (nItem == null) continue;
                    (nItem as BaseObjectViewModel).RemoveSimulationInterface(mInterfaceToObjects);
                }
            }
        }
    }

    // Zjednodušení když člověk nechce interface používat (bude tam defaultní, bázová třída)
    public abstract class ObjectSimulationBase<TInitData, TData> : ObjectSimulationBase<TInitData, TData, SimulationInterfaceBase>
        where TInitData : InitData, new()
        where TData : SimulationData, new()
    {
        public ObjectSimulationBase(TData aData, bool aIsPaused = true)
            : base(aData, aIsPaused) { }
        public ObjectSimulationBase(bool aIsPaused = true)
            : base(aIsPaused) { }

        protected override sealed SimulationInterfaceBase CreateInterface()
        {
            return new SimulationInterfaceBase();
        }
    }
}
