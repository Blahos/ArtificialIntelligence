﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SimulationLibrary
{
	[AttributeUsage(AttributeTargets.Class)]
	public class SimulationAttribute : Attribute
	{
		public string ID { private set; get; }
        public string Name { private set; get; }
        public Type InitViewType { private set; get; }
        public Type ControlPanelType { private set; get; }
        /// <summary>
        /// aSimulationID must be unique (among all used simulations)
        /// </summary>
        /// <param name="aSimulationID">A unique string (among all used simulations)</param>
        /// <param name="aName"></param>
        /// <param name="aInitViewType"></param>
		public SimulationAttribute(string aSimulationID, string aName, Type aInitViewType, Type aControlPanelType)
		{
            ID = aSimulationID;
            Name = aName;
            InitViewType = aInitViewType;
            ControlPanelType = aControlPanelType;

            if (ControlPanelType != null && !ControlPanelType.IsSubclassOf(typeof(UserControl)))
                throw new Exception("The control panel type must derive from UserControl!");
        }
	}

    public class SimulationSpecification
    {
        public SimulationAttribute SimulationAttribute { get; private set; }
        public Type SimulationType { get; private set; }

        public SimulationSpecification(Type aSimulationType, SimulationAttribute aSimulationAttribute)
        {
            SimulationType = aSimulationType;
            SimulationAttribute = aSimulationAttribute;
        }
    }
}
