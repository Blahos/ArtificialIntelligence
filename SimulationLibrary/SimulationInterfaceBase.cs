﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulationLibrary
{
    /// <summary>
    /// Not really an interface, just a base class for all "interfacing" objects (simulation -> objects in simulations)
    /// </summary>
    public class SimulationInterfaceBase
    {
    }
}
