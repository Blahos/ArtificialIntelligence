﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulationLibrary
{
	public class RandomController : IController
	{
		private Random mRandom;
		public double GetAngularSpeed(double aMaxAngularSpeed, Guid aRequestId)
		{
			return (2 * mRandom.NextDouble() - 1) * aMaxAngularSpeed;
		}

		public double GetAcceleration(double aMaxAcceleration, Guid aRequestId)
		{
			return (2 * mRandom.NextDouble() - 1) * aMaxAcceleration;
		}

		public RandomController(int aSeed)
		{
			mRandom = new Random(aSeed);
		}
	}
}
