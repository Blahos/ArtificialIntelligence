﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulationLibrary.Interfaces
{
    public interface ISimulation : INotifyPropertyChanged
    {
        bool IsPaused { get; set; }
        SimulationData Data { get; }

        void Initialise();
        void Initialise(InitData aInitData);
        InitData GetDefaultInitData();
        void Save(string aFileName);
    }
}
