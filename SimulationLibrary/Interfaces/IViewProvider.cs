﻿using LibraryWpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulationLibrary
{
	public interface IViewProvider
	{
		BaseViewService ViewService { get; }
	}
}
