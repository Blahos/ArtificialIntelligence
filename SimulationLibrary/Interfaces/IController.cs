﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulationLibrary
{
	public interface IController
	{
		double GetAngularSpeed(double aMaxAngularSpeed, Guid aRequestId);
		double GetAcceleration(double aMaxAcceleration, Guid aRequestId);
	}
}
