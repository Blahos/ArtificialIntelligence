﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulationLibrary.Interfaces
{
    public interface IVisualSimulation : ISimulation
    {
        IViewProvider ViewProvider { get; set; }
    }
}
