﻿
About simulation structure:

There are basically 3 data structures that define any simulation:

1) InitData
	This structure is provided to a simulation right after it's creation (be it by simple instantiation or
	by load) to set up the basic simulation properties. This happens once, after the creation. These data 
	generally need not to be stored, but it depends. In case of load, they might be immediately overriden
	by the saved data.

2) SimulationData (or simply Data)
	This is a structure that every simulation has one instance of as a property. It should keep it updated 
	and provides it on demand to the application. It should store everything that is important to 
	the user. It's basically a datacontext for the "data output" of the simulation, as opposed to the visual
	output that the user sees on the screen. 

3) Saved simulation (saved data)
	This structure (or byte array or anything really) holds all the information needed to save the simulation
	and recover it in it the same state as it was saved in. These data are not stored anywhere during the runtime,
	they are only created if the user requests the save operation, and they are immediately stored on disk.