﻿using LibraryWpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulationLibrary
{
	public class EatenApplesStatistic : BaseViewModel
	{
		private int mGoodApplesEaten;
		public int GoodApplesEaten
		{
			get { return mGoodApplesEaten; }
			set
			{
				if (mGoodApplesEaten == value) return;
				mGoodApplesEaten = value;
				OnPropertyChanged();
			}
		}
		private int mPoisonedApplesEaten;
		public int PoisonedApplesEaten
		{
			get { return mPoisonedApplesEaten; }
			set
			{
				if (mPoisonedApplesEaten == value) return;
				mPoisonedApplesEaten = value;
				OnPropertyChanged();
			}
		}
	}
}
