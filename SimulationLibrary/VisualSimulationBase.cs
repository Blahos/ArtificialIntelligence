﻿using LibraryWpf;
using SimulationLibrary.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulationLibrary
{
	public abstract class VisualSimulationBase<TInitData, TData> : SimulationBase<TInitData, TData>, IVisualSimulation
        where TInitData : InitData, new()
        where TData : SimulationData, new()
	{
		private IViewProvider mViewProvider;
		public IViewProvider ViewProvider
		{
			get { return mViewProvider; }
			set
			{
				if (mViewProvider == value) return;
				mViewProvider = value;
				OnPropertyChanged();
			}
		}

		public VisualSimulationBase(TData aData, bool aIsPaused = true)
			: base(aData, aIsPaused)
		{

		}

        public void ExecuteOnViewIfPossible(Action aAction)
        {
            var lViewProvider = mViewProvider;
            if (lViewProvider != null)
            {
                var lViewService = lViewProvider.ViewService;
                {
                    if (lViewService != null)
                    {
                        lViewService.Execute(aAction);
                        return;
                    }
                }
            }
            // if view service is not available, try without it
            try
            {
                aAction.Invoke();
            }
            catch (Exception lEx)
            {
                throw new Exception("View service not available and could not execute action without view provider!", lEx);
            }
        }
	}
}
