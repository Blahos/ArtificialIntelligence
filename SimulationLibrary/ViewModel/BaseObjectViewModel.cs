﻿using LibraryWpf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulationLibrary
{
	public abstract class BaseObjectViewModel : BaseViewModel, IPosition
	{
		private static long mNextID = 1;
		private static long GetID() { return mNextID++; }

		private long mID;
		public long ID
		{
			get { return mID; }
			private set
			{
				if (mID == value) return;
				mID = value;
				OnPropertyChanged();
			}
		}
		private bool mCopyIDOverridden = false;
		private long mCopyID;
		public long CopyID
		{
			get { return mCopyID; }
			set
			{
				if (mCopyID == value) return;
				if (!mCopyIDOverridden)
				{
					mCopyID = value;
					OnPropertyChanged();
					mCopyIDOverridden = true;
				}
			}
		}
		private bool mIsDestroyed;
		public bool IsDestroyed
		{
			get { return mIsDestroyed; }
			private set
			{
				if (mIsDestroyed == value) return;
				mIsDestroyed = value;
				OnPropertyChanged();
			}
		}
		private double mSize;
		public double Size
		{
			get { return mSize; }
			set
			{
				if (mSize == value) return;
				mSize = value;
				OnPropertyChanged();
			}
		}
		private double mX;
		public double X
		{
			get { return mX; }
			set
			{
				if (mX == value) return;
				mX = value;
				OnPropertyChanged();
			}
		}
		private double mY;
		public double Y
		{
			get { return mY; }
			set
			{
				if (mY == value) return;
				mY = value;
				OnPropertyChanged();
			}
		}
		private RoomLimiter mRoomLimiter;
		public RoomLimiter RoomLimiter
		{
			get { return mRoomLimiter; }
			set
			{
				if (mRoomLimiter == value) return;
				mRoomLimiter = value;
				OnPropertyChanged();
			}
		}
		protected void LimitPosition()
		{
			if (RoomLimiter != null)
			{
				if (X - Size / 2 < RoomLimiter.XMin) X = RoomLimiter.XMin + Size / 2;
				if (X + Size / 2 > RoomLimiter.XMax) X = RoomLimiter.XMax - Size / 2;
				if (Y - Size / 2 < RoomLimiter.YMin) Y = RoomLimiter.YMin + Size / 2;
				if (Y + Size / 2 > RoomLimiter.YMax) Y = RoomLimiter.YMax - Size / 2;
			}
		}
		public bool HasValidCollision(BaseObjectViewModel aOther)
		{
			if (this.IsDestroyed || aOther == null || aOther.IsDestroyed) return false;
			return (aOther.X - X) * (aOther.X - X) + (aOther.Y - Y) * (aOther.Y - Y) < (aOther.Size + Size) * (aOther.Size + Size) / 4;
		}
		public virtual void Destroy()
		{
			IsDestroyed = true;
		}
		public BaseObjectViewModel()
		{
			ID = GetID();
			mCopyID = ID;
		}
		public abstract void Tick();
		public abstract void SolveCollision(BaseObjectViewModel aOther);
		public abstract BaseObjectViewModel GetCopy();
		public virtual void SetValuesFromCopy(BaseObjectViewModel aCopy)
		{
			Debug.Assert(aCopy.GetType() == this.GetType());

			X = aCopy.X;
			Y = aCopy.Y;
			Size = aCopy.Size;
		}
        // just so we can call this on the non generic class
        public virtual void SetSimulationInterface(SimulationInterfaceBase aInterface) { }
        // you must set what interface you are removing so you can only remove the one that you set before
        public virtual void RemoveSimulationInterface(SimulationInterfaceBase aInterface) { }
    }
    public abstract class BaseObjectViewModel<TSimInterface> : BaseObjectViewModel
        where TSimInterface : SimulationInterfaceBase
    {
        public TSimInterface SimulationInterface { get; private set; }

        public override sealed void SetSimulationInterface(SimulationInterfaceBase aInterface)
        {
            var lType = aInterface.GetType();
            if (lType != typeof(TSimInterface) && lType.IsSubclassOf(typeof(TSimInterface)))
                throw new Exception("The type of the simulation interface is not compatible!");

            SimulationInterface = aInterface as TSimInterface;
        }
        public override sealed void RemoveSimulationInterface(SimulationInterfaceBase aInterface)
        {
            if (SimulationInterface == aInterface)
                SimulationInterface = null;
        }
    }
}
