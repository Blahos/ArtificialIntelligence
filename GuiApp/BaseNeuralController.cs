﻿using GeneticAlgorithmLibrary;
using NeuralNetworkLibrary;
using SimulationLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuiApp
{
    public class BaseNeuralController : FitnessPopulationMember<BinaryDna, double, byte[]>, IController
    {
        protected static readonly Random cRandom = new Random();

        private NeuralNetwork mNetwork = null;
        private Dictionary<Guid, double[]> mLastOutputRequest = new Dictionary<Guid, double[]> { { Guid.Empty, new double[0] } };

        public BaseNeuralController(byte[] aNetworkStructure, byte[] aNetworkParameters = null)
        {
            Initialize(aNetworkStructure);
            if (aNetworkParameters != null)
                mNetwork.SetParameters(aNetworkParameters);
        }
        public override void ConstructFromDna(BinaryDna aDna)
        {
            mNetwork.SetParameters(aDna.Code);
        }

        public double GetAcceleration(double aMaxAcceleration, Guid aRequestId)
        {
            throw new NotImplementedException();
        }

        public double GetAngularSpeed(double aMaxAngularSpeed, Guid aRequestId)
        {
            throw new NotImplementedException();
        }
        

        public override BinaryDna GetDna()
        {
            if (mNetwork == null) throw new Exception("Neural controller is not initialised!");
            return new BinaryDna { Code = mNetwork.GetParameterPart() };
        }

        protected override double CalculateFitnessInner(double aArgument)
        {
            throw new NotImplementedException();
        }
    }
}
