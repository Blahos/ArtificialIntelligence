﻿using Library;
using LibraryWpf;
using SimulationLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GuiApp
{
	public class KeyboardController : IController
	{
		public Key LeftKey { get; set; }
		public Key RightKey { get; set; }
		public Key UpKey { get; set; }
		public Key DownKey { get; set; }
		private HashSet<Key> mPressedKeys = new HashSet<Key>();
		public double AccelerationAmmount { get; set; }
		public double AngularSpeedAmmount { get; set; }
		public double GetAngularSpeed(double aMaxAngularSpeed, Guid aRequestId)
		{
			var lTotalValue = 0.0;
			var lLeftPressed = mPressedKeys.Contains(LeftKey);
			var lRightPressed = mPressedKeys.Contains(RightKey);

			if (lLeftPressed) lTotalValue += AngularSpeedAmmount;
			if (lRightPressed) lTotalValue -= AngularSpeedAmmount;

			return lTotalValue;
		}

		public double GetAcceleration(double aMaxAcceleration, Guid aRequestId)
		{
			var lTotalValue = 0.0;
			var lUpPressed = mPressedKeys.Contains(UpKey);
			var lDownPressed = mPressedKeys.Contains(DownKey);

			if (lUpPressed) lTotalValue += AccelerationAmmount;
			if (lDownPressed) lTotalValue -= AccelerationAmmount;

			return lTotalValue;
		}

		private ICommand mKeyDownCommand;
		public ICommand KeyDownCommand
		{
			get
			{
				if (mKeyDownCommand == null)
				{
					mKeyDownCommand = new RelayCommand((a) => 
					{
						var lKeyArgs = a as KeyEventArgs;
						if (!mPressedKeys.Contains(lKeyArgs.Key))
							mPressedKeys.Add(lKeyArgs.Key);
					});
				}
				return mKeyDownCommand;
			}
		}
		private ICommand mKeyUpCommand;
		public ICommand KeyUpCommand
		{
			get
			{
				if (mKeyUpCommand == null)
				{
					mKeyUpCommand = new RelayCommand((a) =>
					{
						var lKeyArgs = a as KeyEventArgs;
						if (mPressedKeys.Contains(lKeyArgs.Key))
							mPressedKeys.Remove(lKeyArgs.Key);
					});
				}
				return mKeyUpCommand;
			}
		}
	}
}
