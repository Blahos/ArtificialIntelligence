﻿using LibraryWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuiApp
{
	public class SummaryViewModel : BaseViewModel
	{
		private ObservableCollection<SummaryItem> mItems;
		public ObservableCollection<SummaryItem> Items
		{
			get { return mItems; }
			set
			{
				if (mItems == value) return;
				mItems = value;
				OnPropertyChanged();
			}
		}
		public SummaryViewModel()
		{
			Items = new ObservableCollection<SummaryItem>();
		}
	}
}
