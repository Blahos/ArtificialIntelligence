﻿using SimulationLibrary;
using Simulations.AppleEating.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace GuiApp
{
	public class ObjectTemplateSelector : DataTemplateSelector
	{
		public override System.Windows.DataTemplate SelectTemplate(object item, System.Windows.DependencyObject container)
		{
			if (item == null) return null;
			//var lContainer = container as FrameworkElement; // why is this here?

			if (item.GetType() == typeof(MinionViewModel)) return MinionTemplate;
			if (item.GetType() == typeof(FoodViewModel)) return FoodTemplate;
			return null;
		}

		public DataTemplate MinionTemplate { get; set; }
		public DataTemplate FoodTemplate { get; set; }


	}
}
