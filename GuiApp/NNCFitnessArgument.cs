﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuiApp
{
    public class NNCFitnessArgument
    {
        public int Score { get; set; }
        /// <summary>
        /// Minimum score in the population
        /// </summary>
        public int MinScore { get; set; }
    }
}
