﻿using GeneticAlgorithmLibrary;
using Library;
using LibraryWpf;
using Microsoft.Win32;
using NeuralNetworkLibrary;
using SimulationLibrary;
using SimulationLibrary.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace GuiApp
{
	public class MainWindowViewModel : BaseViewModel, IViewProvider
	{
		private IObjectSimulation mSimulation;
		public IObjectSimulation Simulation
		{
			get { return mSimulation; }
			set
			{
				if (mSimulation == value) return;
				mSimulation = value;
				OnPropertyChanged();
                OnPropertyChanged(nameof(PauseText));
                OnPropertyChanged(nameof(DisplayObjects));
			}
		}
		//private readonly Timer mStepTimer;
		//private readonly Random mRandom = new Random();
		//private readonly Brush cNeuralMinionStandardColor = Brushes.Blue;
		//private readonly List<DateTime> mFrameTimes = new List<DateTime>();
		//private byte[] mNeuralNetworkStructure;

		#region Commands
		private ICommand mPauseCommand;
		public ICommand PauseCommand
		{
			get
			{
				if (mPauseCommand == null)
				{
					mPauseCommand = new RelayCommand((a) =>
					{
                        if (mSimulation == null) return;
						mSimulation.IsPaused = !mSimulation.IsPaused;
                        if (!mSimulation.IsPaused) ReplayBufferIndex = 0;
					});
				}
				return mPauseCommand;
			}
		}
		private ICommand mEvolveCommand;
		public ICommand EvolveCommand
		{
			get
			{
				if (mEvolveCommand == null)
				{
					mEvolveCommand = new RelayCommand(a => 
					{
						try
						{
							//mSimulation.EvolvePopulation();
							Console.WriteLine("This feature is currently unavailable.");
						}
						catch (Exception lEx)
						{
							Console.WriteLine("Exception occured during EvolveCommand: {0}", lEx);
						}
					});
				}
				return mEvolveCommand;
			}
		}
		private ICommand mSummaryCommand = null;
		public ICommand SummaryCommand
		{
			get
			{
				if (mSummaryCommand == null)
				{
					mSummaryCommand = new RelayCommand((a) => 
					{
						var lViewService = ViewService;
						if (lViewService == null)
						{
							Console.WriteLine("Missing view service!");
							return;
						}
                        if (mSimulation == null)
                        {
                            Console.WriteLine("No simulation open!");
                            return;
                        }
                        lViewService.Show(mSimulation.Data.GetSummary(), "Summary");
					});
				}
				return mSummaryCommand;
			}
		}
		private ICommand mSaveCommand = null;
		public ICommand SaveCommand
		{
			get
			{
				if (mSaveCommand == null)
				{
					mSaveCommand = new RelayCommand((a) => 
					{
						if (!mSimulation.IsPaused)
						{
							Console.WriteLine("Simulation must be paused to save !");
							return;
						}
						var lDialog = new SaveFileDialog();
						lDialog.AddExtension = true;
						lDialog.DefaultExt = ".bin";
						lDialog.FileName = "simulation";

						if (lDialog.ShowDialog() == true)
						{
							var lFileName = lDialog.FileName;
							mSimulation.Save(lFileName);
						}
						Console.WriteLine("Simulation saved successfuly");
					});
				}
				return mSaveCommand;
			}
		}
        private ICommand mNewCommand = null;
        public ICommand NewCommand
        {
            get
            {
                if (mNewCommand == null)
                {
                    mNewCommand = new RelayCommand((a) =>
                    {
                        try
                        {
                            var lViewModel = new NewSimulationViewModel();

                            // Select simulation
                            var lResult = mViewService.ShowDialog(lViewModel, "Pick type ...");
                            if (lResult == true)
                            {
                                var lSelectedSimulation = lViewModel.SelectedSimulation;
                                var lNewSimulation = SimulationBase.CreateSimulation(lSelectedSimulation.SimulationAttribute.ID) as IObjectSimulation;

                                if (lNewSimulation == null) throw new Exception(String.Format("Failed to create new simulation of type {0}", lViewModel.SelectedSimulation));

                                // Initialise simulation
                                var lInitData = lNewSimulation.GetDefaultInitData();
                                var lInitViewType = lSelectedSimulation.SimulationAttribute.InitViewType;

                                // Result of initialisation
                                bool? lInitResult = true;

                                // If there is an init form, use it to modify the initialisation data,
                                // otherwise initialise by the default data
                                if (lInitViewType != null)
                                    lInitResult = mViewService.ShowDialog(lInitData, lInitViewType, "Initialise your simulation ...");

                                if (lInitResult == true)
                                {
                                    SetNewSimulation(lNewSimulation, lInitData);

                                    Console.WriteLine("New simulation of type {0} successfuly created.", lViewModel.SelectedSimulation);
                                }
                            }
                        }
                        catch (Exception lEx)
                        {
                            Console.WriteLine("Problem creating a new simulation!");
                            Console.WriteLine(lEx.Message);
                            Console.WriteLine("Inner exception: {0}", lEx.InnerException.Message);
                        }
                    });
                }
                return mNewCommand;
            }
        }

        private ICommand mLoadCommand = null;
		public ICommand LoadCommand
		{
			get
			{
				if (mLoadCommand == null)
				{
					mLoadCommand = new RelayCommand((a) =>
					{
						try
						{
							if (mSimulation != null && !mSimulation.IsPaused)
							{
								Console.WriteLine("The existing simulation must be paused to load!");
								return;
							}

							var lDialog = new OpenFileDialog();
							lDialog.DefaultExt = ".bin";

							if (lDialog.ShowDialog() == true)
							{
								var lFileName = lDialog.FileName;

								var lNewSimulation = SimulationBase.Load(lFileName) as IObjectSimulation;

                                if (lNewSimulation == null) throw new Exception(String.Format("Failed to load simulation from {0}", lFileName));

                                SetNewSimulation(lNewSimulation);
							}
							Console.WriteLine("Simulation loaded successfuly");
						}
						catch (Exception lEx)
						{
							Console.WriteLine("Could not load simulation.");
							Console.WriteLine("Error message: {0}", lEx.Message);
						}
					});
				}
				return mLoadCommand;
			}
		}
		private ICommand mExportBestCommand = null;
		public ICommand ExportBestCommand
		{
			get
			{
				if (mExportBestCommand == null)
				{
					mExportBestCommand = new RelayCommand(a => 
					{
						if (!mSimulation.IsPaused)
						{
							Console.WriteLine("Simulation must be paused to export!");
							return;
						}

						Console.WriteLine("Feature currently not available");
						//var lDialog = new SaveFileDialog();
						//lDialog.AddExtension = true;
						//lDialog.DefaultExt = ".txt";
						//lDialog.FileName = "bestmember";

						//if (lDialog.ShowDialog() == true)
						//{
						//	var lBestMinion = mSimulation.Objects
						//		.OfType<MinionViewModel>()
						//		.Cast<MinionViewModel>()
						//		.Where(aa => aa.Controller is NeuralNetworkController)
						//		.MaxBy(aa => aa.Score);

						//	var lBestNeuralNetworkSP = (lBestMinion.Controller as NeuralNetworkController).GetNetworkStructurePart();
						//	var lBestNeuralNetworkPP = (lBestMinion.Controller as NeuralNetworkController).GetNetworkParameterPart();

						//	var lBestNetwork = NeuralNetwork.FromBinaryStructureOnly(lBestNeuralNetworkSP);
						//	lBestNetwork.SetParameters(lBestNeuralNetworkPP);

						//	lBestNetwork.SaveToTextFile(lDialog.FileName);
						//}
					});
				}
				return mExportBestCommand;
			}
		}
        #endregion

        #region Properties
        
        private int mReplayBufferIndex;
        public int ReplayBufferIndex
        {
            get { return mReplayBufferIndex; }
            set
            {
                if (value == mReplayBufferIndex) return;
                mReplayBufferIndex = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(DisplayObjects));
            }
        }

        private bool mViewActive = true;
        public bool ViewActive
        {
            get { return mViewActive; }
            set
            {
                if (mViewActive == value) return;
                mViewActive = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(DisplayObjects));
            }
        }

        public ReadOnlyObservableCollection<BaseObjectViewModel> DisplayObjects
        {
            get
            {
                if (mSimulation == null) return null;
                if (!mViewActive) return null;
                if (mSimulation.IsPaused)
                {
                    if (mReplayBufferIndex < 0)
                    {
                        var lReplayCount = mSimulation.ReplayBuffer.Count;
                        return mSimulation.ReplayBuffer[mReplayBufferIndex + lReplayCount];
                    }
                    else return mSimulation.Objects;
                }
                else return mSimulation.Objects;
            }
        }

		private BaseViewService mViewService;
		public BaseViewService ViewService
		{
			get { return mViewService; }
			set
			{
				if (mViewService == value) return;
				mViewService = value;
				OnPropertyChanged();
			}
		}
		private KeyboardController mKeyboardController = new KeyboardController 
		{
 			LeftKey = Key.A,
			RightKey = Key.D,
			UpKey = Key.W,
			DownKey = Key.S,
			AccelerationAmmount = 1,
			AngularSpeedAmmount = 8
		};
		public KeyboardController KeyboardController
		{
			get { return mKeyboardController; }
			set
			{
				if (mKeyboardController == value) return;
				mKeyboardController = value;
				OnPropertyChanged();
			}
		}
        public string PauseText
		{
			get
			{
				if (mSimulation != null)
				{ 
					if (mSimulation.IsPaused) return "Run";
					else return "Pause";
				}
				else
				{
					return "No simulation";
				}
			}
		}
		#endregion

		public MainWindowViewModel()
		{
            //mSimulation = new AppleEatingSimulation(lFileName, RoomLimiter, 40);
            //mSimulation.PropertyChanged += mSimulation_PropertyChanged;
            //SimulationBase.Activate();
		}
		#region Methods

        private void SetNewSimulation(IObjectSimulation aSimulation, InitData aInitData = null)
        {
            if (aSimulation == null) return;

            if (Simulation != null) Simulation.PropertyChanged -= SimulationPropertyChanged;

            aSimulation.ViewProvider = this;

            if (aInitData != null) aSimulation.Initialise(aInitData);

            Simulation = aSimulation;
            Simulation.PropertyChanged += SimulationPropertyChanged;
            Simulation.ReplayBufferSize = 600;
        }
		private void SimulationPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == nameof(SimulationBase.IsPaused))
			{
				OnPropertyChanged(nameof(PauseText));
				OnPropertyChanged(nameof(DisplayObjects));
			}
		}
		#endregion

	}
}
