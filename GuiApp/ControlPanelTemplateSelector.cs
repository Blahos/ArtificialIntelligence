﻿using SimulationLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace GuiApp
{
    public class ControlPanelTemplateSelector : DataTemplateSelector
    {
        private Dictionary<Type, Type> mTemplateDictionary = new Dictionary<Type, Type>();

        public ControlPanelTemplateSelector()
        {
            var lSimulationList = SimulationBase.SimulationList.Where(a => a.SimulationAttribute.ControlPanelType != null).ToList();

            foreach (var nSimulation in lSimulationList)
            {
                mTemplateDictionary.Add(nSimulation.SimulationType, nSimulation.SimulationAttribute.ControlPanelType);
            }
        }
        public override System.Windows.DataTemplate SelectTemplate(object item, System.Windows.DependencyObject container)
        {
            if (item == null) return null;

            if (!mTemplateDictionary.ContainsKey(item.GetType())) return null;

            var lPanelType = mTemplateDictionary[item.GetType()];

            FrameworkElementFactory lFactory = new FrameworkElementFactory(lPanelType);
            DataTemplate lTemplate = new DataTemplate(item.GetType()) { VisualTree = lFactory };

            return lTemplate;
        }
    }
}
