﻿using SimulationLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuiApp.Simulations.Test
{
    public class TestInitData : InitData
    {
        private int mIntValue;
        public int IntValue
        {
            get { return mIntValue; }
            set { SetProperty(ref mIntValue, value); }
        }
    }
}
