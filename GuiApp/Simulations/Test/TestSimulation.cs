﻿using GuiApp.Simulations.Test;
using GuiApp.Simulations.Test.View;
using SimulationLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuiApp.Simulations
{
    [Simulation("TestSimulation", "Test simulation", typeof(TestInitView), null)]
    public class TestSimulation : ObjectSimulationBase<TestInitData, TestData>
    {
        protected override void LoadInternal(byte[] aArray)
        {
            IntProperty = BitConverter.ToInt32(aArray, 0);
        }

        protected override byte[] SaveInternal()
        {
            return BitConverter.GetBytes(mIntProperty);
        }
        protected override void InitialiseInner(TestInitData aInitData)
        {
            base.InitialiseInner(aInitData);

            IntProperty = aInitData.IntValue;
        }

        private int mIntProperty;
        public int IntProperty
        {
            get { return mIntProperty; }
            set { SetProperty(ref mIntProperty, value); }
        }

        public override TestInitData GetDefaultInitData()
        {
            return new TestInitData { IntValue = 13 };
        }
    }
}
