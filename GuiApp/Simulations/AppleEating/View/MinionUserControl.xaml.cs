﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Simulations.AppleEating.View
{
	/// <summary>
	/// Interaction logic for MinionUserControl.xaml
	/// </summary>
	public partial class MinionUserControl : UserControl
	{
		public MinionUserControl()
		{
			InitializeComponent();
		}
		public static readonly DependencyProperty DirectionProperty = DependencyProperty.Register(
			"Direction", typeof(double), typeof(MinionUserControl), new PropertyMetadata(default(double)));

		public double Direction
		{
			get { return (double)GetValue(DirectionProperty); }
			set { SetValue(DirectionProperty, value); }
		}

		public static readonly DependencyProperty SizeProperty = DependencyProperty.Register(
			"Size", typeof(double), typeof(MinionUserControl), new PropertyMetadata(default(double)));

		public double Size
		{
			get { return (double)GetValue(SizeProperty); }
			set { SetValue(SizeProperty, value); }
		}

		public static readonly DependencyProperty MouseXProperty = DependencyProperty.Register(
			"MouseX", typeof(double), typeof(MinionUserControl), new PropertyMetadata(default(double)));

		public double MouseX
		{
			get { return (double)GetValue(MouseXProperty); }
			private set { SetValue(MouseXProperty, value); }
		}

		public static readonly DependencyProperty MouseYProperty = DependencyProperty.Register(
			"MouseY", typeof(double), typeof(MinionUserControl), new PropertyMetadata(default(double)));

		public double MouseY
		{
			get { return (double)GetValue(MouseYProperty); }
			private set { SetValue(MouseYProperty, value); }
		}
		public Brush BodyColor
		{
			get { return (Brush)GetValue(BodyColorProperty); }
			set { SetValue(BodyColorProperty, value); }
		}

		// Using a DependencyProperty as the backing store for BodyColor.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty BodyColorProperty =
			DependencyProperty.Register("BodyColor", typeof(Brush), typeof(MinionUserControl), new PropertyMetadata(default(Brush)));

		public Brush VisorColor
		{
			get { return (Brush)GetValue(VisorColorProperty); }
			set { SetValue(VisorColorProperty, value); }
		}

		// Using a DependencyProperty as the backing store for VisorColor.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty VisorColorProperty =
			DependencyProperty.Register("VisorColor", typeof(Brush), typeof(MinionUserControl), new PropertyMetadata(default(Brush)));

		
		private void mUserControl_MouseMove(object sender, MouseEventArgs e)
		{
            var lSource = PresentationSource.FromVisual(this);
            
            if (lSource != null)
            {
                var lDpiX = 96.0 * lSource.CompositionTarget.TransformToDevice.M11;
                var lDpiY = 96.0 * lSource.CompositionTarget.TransformToDevice.M22;

                var lScreenPosition = PointToScreen(e.GetPosition(this));
			    MouseX = lScreenPosition.X * 96 / lDpiX;
			    MouseY = lScreenPosition.Y * 96 / lDpiY;

            }
        }

	}
}
