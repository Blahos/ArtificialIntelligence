﻿using LibraryWpf;
using SimulationLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuiApp.Simulations.AppleEating
{
	public class AppleEatingData : SimulationData
	{
		private long mTicks;
		public long Ticks
		{
			get { return mTicks; }
			set
			{
				if (mTicks == value) return;
				mTicks = value;
				OnPropertyChanged();
			}
		}
		private int mGoodApplesEaten;
		public int GoodApplesEaten
		{
			get { return mGoodApplesEaten; }
			set
			{
				if (mGoodApplesEaten == value) return;
				mGoodApplesEaten = value;
				OnPropertyChanged();
			}
		}
		private int mPoisonedApplesEaten;
		public int PoisonedApplesEaten
		{
			get { return mPoisonedApplesEaten; }
			set
			{
				if (mPoisonedApplesEaten == value) return;
				mPoisonedApplesEaten = value;
				OnPropertyChanged();
			}
		}
		private int mGoodApplesEatenIteration;
		public int GoodApplesEatenIteration
		{
			get { return mGoodApplesEatenIteration; }
			set
			{
				if (mGoodApplesEatenIteration == value) return;
				mGoodApplesEatenIteration = value;
				OnPropertyChanged();
			}
		}
		private int mPoisonedApplesEatenIteration;
		public int PoisonedApplesEatenIteration
		{
			get { return mPoisonedApplesEatenIteration; }
			set
			{
				if (mPoisonedApplesEatenIteration == value) return;
				mPoisonedApplesEatenIteration = value;
				OnPropertyChanged();
			}
		}
		private int mGenerationNumber;
		public int GenerationNumber
		{
			get { return mGenerationNumber; }
			set
			{
				if (mGenerationNumber == value) return;
				mGenerationNumber = value;
				OnPropertyChanged();
			}
		}
		private SummaryViewModel mGenerationSummary;
		public SummaryViewModel GenerationSummary
		{
			get { return mGenerationSummary; }
			set
			{
				if (mGenerationSummary == value) return;
				mGenerationSummary = value;
				OnPropertyChanged();
			}
		}

		public void Reset()
		{
			Ticks = 0;
			GenerationNumber = 0;
			GoodApplesEaten = 0;
			GoodApplesEatenIteration = 0;
			PoisonedApplesEaten = 0;
			PoisonedApplesEatenIteration = 0;
			GenerationSummary = new SummaryViewModel();
		}
		public AppleEatingData()
		{
			Reset();
		}

		public void RecordThisGeneration(int aTotalScore, int aMinionCount, int aBestScore)
		{
			var lTicks = Ticks;
			if (GenerationSummary.Items.Count > 0)
			{
				lTicks -= GenerationSummary.Items.Sum(aa => aa.Ticks);
			}
			// Záznam o ukončené generaci
			var lSummaryItem = new SummaryItem
			{
				GenerationNumber = GenerationNumber,
				TotalScore = aTotalScore,
				AverageScore = (double)aTotalScore / aMinionCount,
				BestScore = aBestScore,
				GoodApplesEaten = GoodApplesEatenIteration,
				PoisonedApplesEaten = PoisonedApplesEatenIteration,
				Ticks = lTicks
			};
			GenerationSummary.Items.Add(lSummaryItem);

			GoodApplesEatenIteration = 0;
			PoisonedApplesEatenIteration = 0;
			GenerationNumber++;
		}

		public override BaseViewModel GetSummary()
		{
			return GenerationSummary;
		}
	}
}
