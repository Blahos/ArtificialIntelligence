﻿using GuiApp.Simulations.AppleEating;
using LibraryWpf;
using SimulationLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Simulations.AppleEating.ViewModel
{
	public class MinionViewModel : BaseObjectViewModel<AppleEatingInterfaceToObjects>, IPositionMovement
	{
		private int mCurrentParalysisSteps = 0;
		private const int cParalysisSteps = 350;
		/// <summary>
		/// "Fyzikální" omezení miniona, rychleji prostě nepojede
		/// </summary>
		public const double cMaxSpeed = 4;
		/// <summary>
		/// "Fyzikální" omezení miniona, rychleji se prostě otáčet nebude
		/// </summary>
		public const double cMaxAngularSpeed = 8;
		/// <summary>
		/// "Fyzikální" omezení miniona, rychleji prostě zrychlovat nebude
		/// </summary>
		public const double cMaxAcceleration = 1;
		private IController mController;
		public IController Controller
		{
			get { return mController; }
			set
			{
				if (mController == value) return;
				mController = value;
				OnPropertyChanged();
			}
		}
		private bool mIsParalyzed;
		public bool IsParalyzed
		{
			get { return mIsParalyzed; }
			private set
			{
				if (mIsParalyzed == value) return;
				mIsParalyzed = value;
				OnPropertyChanged();
			}
		}
		private double mDirection;
		public double Direction
		{
			get { return mDirection; }
			set
			{
				if (mDirection == value) return;
				mDirection = value;
				if (mDirection >= 360) mDirection -= 360;
				if (mDirection < 0) mDirection += 360;
				OnPropertyChanged();
			}
		}
		private double mSpeed;
		public double Speed
		{
			get { return mSpeed; }
			set
			{
				if (mSpeed == value) return;
				mSpeed = value;
				OnPropertyChanged();
			}
		}
		private EatenApplesStatistic mEatenApples;
		public EatenApplesStatistic EatenApples
		{
			get { return mEatenApples; }
			set
			{
				if (mEatenApples == value) return;
				if (mEatenApples != null)
					mEatenApples.PropertyChanged -= EatenApplesPropertyChanged;
				mEatenApples = value;
				if (mEatenApples != null)
					mEatenApples.PropertyChanged += EatenApplesPropertyChanged;

				OnPropertyChanged();
				OnPropertyChanged("Score");
			}
		}
		void EatenApplesPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			OnPropertyChanged("Score");
		}
		public int Score
		{
			get
			{
				if (EatenApples == null) return 0;
				var lScore = EatenApples.GoodApplesEaten;
                lScore -= 4 * EatenApples.PoisonedApplesEaten;
                //lScore = Math.Max(lScore, 0);
				return lScore;
			}
		}
		private Brush mBodyColor;
		public Brush BodyColor
		{
			get { return mBodyColor; }
			set
			{
				if (mBodyColor == value) return;
				mBodyColor = value;
				OnPropertyChanged();
			}
		}
		private Brush mVisorColor;
		public Brush VisorColor
		{
			get { return mVisorColor; }
			set
			{
				if (mVisorColor == value) return;
				mVisorColor = value;
				OnPropertyChanged();
			}
		}
		private void Move()
		{
			var lRequestId = Guid.NewGuid();
			var lController = Controller;
			if (lController == null) return;
			var lAcceleration = lController.GetAcceleration(cMaxAcceleration, lRequestId);
			var lAngularSpeed = lController.GetAngularSpeed(cMaxAngularSpeed, lRequestId);

			if (lAcceleration > cMaxAcceleration) lAcceleration = cMaxAcceleration;
			if (lAcceleration < -cMaxAcceleration) lAcceleration = -cMaxAcceleration;

			if (lAngularSpeed > cMaxAngularSpeed) lAngularSpeed = cMaxAngularSpeed;
			if (lAngularSpeed < -cMaxAngularSpeed) lAngularSpeed = -cMaxAngularSpeed;

			Direction += lAngularSpeed;
			Speed += lAcceleration;
			if (Speed > cMaxSpeed) Speed = cMaxSpeed;
			if (Speed < 0) Speed = 0;

			X += Speed * Math.Cos(Direction / 180 * Math.PI);
			Y -= Speed * Math.Sin(Direction / 180 * Math.PI);
		}

		public override void Tick()
		{
			if (IsParalyzed)
			{
				mCurrentParalysisSteps++;
				if (mCurrentParalysisSteps >= cParalysisSteps) IsParalyzed = false;
			}
			else
			{
				Move();
			}
			LimitPosition();
		}

		public override void SolveCollision(BaseObjectViewModel aOther)
		{
			if (!HasValidCollision(aOther)) return;

			if (aOther is FoodViewModel)
            {
                if (!SimulationInterface.ApplesEatable) return;

                var lFood = aOther as FoodViewModel;
				lFood.Destroy(DestroyReason.Eaten);

				if (!lFood.IsPoisoned)
				{
					EatenApples.GoodApplesEaten++;
				}
				else
				{
					EatenApples.PoisonedApplesEaten++;
				}
			}
		}

		public override BaseObjectViewModel GetCopy()
		{
			return new MinionViewModel
			{
				EatenApples = new EatenApplesStatistic { GoodApplesEaten = EatenApples.GoodApplesEaten, PoisonedApplesEaten = EatenApples.PoisonedApplesEaten },
				Direction = Direction,
				Size = Size,
				X = X,
				Y = Y,
				CopyID = ID,
				BodyColor = BodyColor,
				VisorColor = VisorColor
			};
		}

		public override void SetValuesFromCopy(BaseObjectViewModel aCopy)
		{
			base.SetValuesFromCopy(aCopy);

			var lCopy = aCopy as MinionViewModel;
			if (lCopy.EatenApples != null)
			{
				EatenApples = new EatenApplesStatistic { GoodApplesEaten = lCopy.EatenApples.GoodApplesEaten, PoisonedApplesEaten = lCopy.EatenApples.PoisonedApplesEaten };
			}
			Direction = lCopy.Direction;
			BodyColor = lCopy.BodyColor;
			VisorColor = lCopy.VisorColor;
		}

		public MinionViewModel()
			: base()
		{
			EatenApples = new EatenApplesStatistic();
		}
	}
}
