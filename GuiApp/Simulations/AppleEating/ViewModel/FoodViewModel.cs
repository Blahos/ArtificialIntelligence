﻿using LibraryWpf;
using SimulationLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulations.AppleEating.ViewModel
{
	public class FoodViewModel : BaseObjectViewModel
	{
		public override void Tick()
		{
			LimitPosition();
		}

		public override void SolveCollision(BaseObjectViewModel aOther)
		{
			//if (!HasValidCollision(aOther)) return;
			// Tady (v jídle) nedělat nic, když se srazí s minionem, tak ho zničí on
		}

		public override BaseObjectViewModel GetCopy()
		{
			return new FoodViewModel
			{
				Size = Size,
				X = X,
				Y = Y,
				CopyID = ID,
				IsPoisoned = IsPoisoned
			};
		}
		private bool mIsPoisoned;
		public bool IsPoisoned
		{
			get { return mIsPoisoned; }
			set
			{
				if (mIsPoisoned == value) return;
				mIsPoisoned = value;
				OnPropertyChanged();
			}
		}
		private DestroyReason? mDestroyReason;
		public DestroyReason? DestroyReason
		{
			get { return mDestroyReason; }
			private set
			{
				if (mDestroyReason == value) return;
				mDestroyReason = value;
				OnPropertyChanged();
			}
		}

		public void Destroy(DestroyReason aReason)
		{
			base.Destroy();
			DestroyReason = aReason;
		}
		public override void Destroy()
		{
			Destroy(SimulationLibrary.DestroyReason.SystemDestroy);
		}
	}
}
