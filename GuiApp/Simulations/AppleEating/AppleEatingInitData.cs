﻿using LibraryWpf;
using SimulationLibrary;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuiApp.Simulations.AppleEating
{
    public class AppleEatingInitData : InitData, IDialog
    {
        public ObservableCollection<NeuronLayerSettings> InnerLayers { get; }
            = new ObservableCollection<NeuronLayerSettings>();

        private int mPopulationSize = 10;
        public int PopulationSize
        {
            get { return mPopulationSize; }
            set
            {
                if (mPopulationSize == value) return;
                mPopulationSize = value;
                OnPropertyChanged();
            }
        }
        private int mAppleCount = 20;
        public int AppleCount
        {
            get { return mAppleCount; }
            set
            {
                if (mAppleCount == value) return;
                mAppleCount = value;
                OnPropertyChanged();
            }
        }

        private int mAutomaticEvolutionPeriod = 300;
        public int AutomaticEvolutionPeriod
        {
            get { return mAutomaticEvolutionPeriod; }
            set
            {
                if (mAutomaticEvolutionPeriod == value) return;
                mAutomaticEvolutionPeriod = value;
                OnPropertyChanged();
            }
        }

        public DialogResult CheckResult()
        {
            if (InnerLayers.Count < 0) return "Not enough neuron layers set!";
            if (InnerLayers.Any(a => a.NeuronCount <= 0)) return "Neuron layers sizes must be all positive integers!";

            return true;
        }
        public class NeuronLayerSettings
        {
            public int NeuronCount { get; set; }
        }
    }
}
