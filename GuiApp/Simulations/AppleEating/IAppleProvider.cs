﻿using SimulationLibrary;
using Simulations.AppleEating.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuiApp.Simulations.AppleEating
{
	public interface IAppleProvider
	{
		List<FoodViewModel> GetApples();
	}
}
