﻿using GeneticAlgorithmLibrary;
using GuiApp.Simulations.AppleEating;
using GuiApp.Simulations.AppleEating.View;
using LibraryWpf;
using NeuralNetworkLibrary;
using SimulationLibrary;
using Simulations.AppleEating.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

// The simulation itself is just in the Simulations namespace, not in the Simulations.AppleEating namespace
// This is because simulation classes are used in xaml, so we don't have to define xmlns for each simulation
namespace GuiApp.Simulations
{
    [Simulation("AppleEatingSimulation", "Apple Eating", typeof(AppleEatingInitView), typeof(AppleEatingControlPanel))]
    public class AppleEatingSimulation : ObjectSimulationBase<AppleEatingInitData, AppleEatingData, AppleEatingInterfaceToObjects>, IAppleProvider
    {
        /// TODO Move all simulations to a separate library (could be called simulation library, but this name is already
        /// used for the general library).
        /// But there should be something similar for all concrete simulations so they don't polute the gui app
		private readonly Brush mNeuralMinionStandardColor = Brushes.Blue;
        private readonly Random mRandom = new Random();

        // for the minion controller neural network
        public static int InputLayerSize { get; } = 3;
        public static int OutputLayerSize { get; } = 2;
        
        private byte[] mNeuralNetworkStructure = null;

        private bool mUseAutomaticEvolution = true;
		public bool UseAutomaticEvolution
		{
			get { return mUseAutomaticEvolution; }
			set
			{
				if (mUseAutomaticEvolution == value) return;
				mUseAutomaticEvolution = value;
				OnPropertyChanged();
			}
		}
        // for the current generation, the changes to AutomaticEvolutionPeriod will only affect future generations 
        private long mCurrentAutomaticEvolutionPeriod = 100;
        public long CurrentAutomaticEvolutionPeriod
        {
            get { return mCurrentAutomaticEvolutionPeriod; }
            private set
            {
                if (mCurrentAutomaticEvolutionPeriod == value) return;
                mCurrentAutomaticEvolutionPeriod = value;
                OnPropertyChanged();
            }
        }

        private long mAutomaticEvolutionPeriod = 100;
		public long AutomaticEvolutionPeriod
		{
			get { return mAutomaticEvolutionPeriod; }
			set
			{
				if (mAutomaticEvolutionPeriod == value) return;
				mAutomaticEvolutionPeriod = value;
				OnPropertyChanged();
			}
		}
        private long mCurrentGenerationSteps = 0;
		public long CurrentGenerationSteps
		{
			get { return mCurrentGenerationSteps; }
			private set
			{
				if (mCurrentGenerationSteps == value) return;
                mCurrentGenerationSteps = value;
				OnPropertyChanged();
			}
		}
        //private bool mApplesEatableChanged = false;
        private bool mApplesEatable = true;
		public bool ApplesEatable
		{
			get { return mApplesEatable; }
			set
			{
				if (mApplesEatable == value) return;
				mApplesEatable = value;
				//mApplesEatableChanged = true;
				OnPropertyChanged();
			}
		}
        private int mApplesMaxCount;
		public int ApplesMaxCount
		{
			get { return mApplesMaxCount; }
			set
			{
				if (mApplesMaxCount == value) return;
				mApplesMaxCount = value;
				OnPropertyChanged();
			}
		}
        private bool mRespawnApples = true;
        public bool RespawnApples
        {
            get { return mRespawnApples; }
            set
            {
                if (mRespawnApples == value) return;
                mRespawnApples = value;
                OnPropertyChanged();
            }
        }
        private double mPoisonedChance = 0.0;
		public void GenerateNewPopulatioFromNetwork(string aNeuralNetworkFilePath, int aPopulationSize, bool lRemovePreviousPopulation = true)
		{
			Console.WriteLine("This feature is currently not available");
			//if (lRemovePreviousPopulation)
			//{
			//	var lOldMembers = GetNeuralMinions(Objects);
			//	foreach (var nMember in lOldMembers) Objects.Remove(nMember);
			//}

			//var lTestNeuralNetwork = NeuralNetworkLibrary.NeuralNetwork.FromTextFile(aNeuralNetworkFilePath);

			//for (int i = 0; i < aPopulationSize; i++)
			//{
			//	var lNeuralMinion = CreateNewNNMinion(mNeuralNetworkStructure);
			//	(lNeuralMinion.Controller as NeuralNetworkController).Randomize();
			//	mObjects.Add(lNeuralMinion);
			//}
		}
		public AppleEatingSimulation(
			bool aIsPaused = true)
			: base(aIsPaused)
		{
            //DependencyObject lO = new DependencyObject(); // pro určení design módu

            //AutomaticEvolutionRemainingSteps = AutomaticEvolutionPeriod;
            //ApplesEatable = true;
            //NeuralNetworkLibrary.NeuralNetwork lTestNeuralNetwork;
            //if (!DesignerProperties.GetIsInDesignMode(lO))
            //    lTestNeuralNetwork = NeuralNetworkLibrary.NeuralNetwork.FromTextFile(aNeuralNetworkFilePath);
            //else
            //    lTestNeuralNetwork = NeuralNetworkLibrary.NeuralNetwork.FromTextFile(@"C:\Users\Jirka\Documents\Visual Studio 2013\Projects\ArtificialIntelligence\GuiApp\" + aNeuralNetworkFilePath);

            //var lNetworkStructure = lTestNeuralNetwork.GetStructurePart();
            //mNeuralNetworkStructure = lNetworkStructure;

            //for (int i = 0; i < aPopulationSize; i++)
            //{
            //    var lNeuralMinion = CreateNewNNMinion(mNeuralNetworkStructure);
            //    (lNeuralMinion.Controller as NeuralNetworkController).Randomize();
            //    mObjects.Add(lNeuralMinion);
            //}
        }
        protected override void InitialiseInner(AppleEatingInitData aInitData)
        {
            base.InitialiseInner(aInitData);

            AutomaticEvolutionPeriod = aInitData.AutomaticEvolutionPeriod;
            CurrentAutomaticEvolutionPeriod = AutomaticEvolutionPeriod;
            CurrentGenerationSteps = 0;
            ApplesMaxCount = aInitData.AppleCount;
            GenerateApples(ApplesMaxCount);
            
            int[] lLayersSizes = new int[aInitData.InnerLayers.Count + 2];
            lLayersSizes[0] = InputLayerSize;
            lLayersSizes[lLayersSizes.Length - 1] = OutputLayerSize;
            for (int i = 0; i < aInitData.InnerLayers.Count; i++)
                lLayersSizes[i + 1] = aInitData.InnerLayers[i].NeuronCount;

            var lDummyNetwork = NeuralNetworkFactory.ConstructCompleteNetwork(lLayersSizes);
            mNeuralNetworkStructure = lDummyNetwork.GetStructurePart();

            // Create the initial population
            for (int i = 0; i < aInitData.PopulationSize; i++)
            {
                var lNewMinion = CreateNewNNMinion(mNeuralNetworkStructure);
                mObjects.Add(lNewMinion);
            }
        }

        public AppleEatingSimulation()
			: this(true)
		{

		}
		protected override void TickStartAction()
		{
			base.TickStartAction();

			Data.Ticks++;

			if (ViewProvider == null)
			{
				Console.WriteLine("Missing view provider !");
				return;
			}

            CurrentGenerationSteps++;

            var lViewService = ViewProvider.ViewService;
			if (UseAutomaticEvolution)
			{
				if (CurrentGenerationSteps >= CurrentAutomaticEvolutionPeriod)
				{
                    CurrentGenerationSteps = 0;
                    CurrentAutomaticEvolutionPeriod = AutomaticEvolutionPeriod;
                    lViewService.Execute(() =>
                    {
                        var lIsPaused = IsPaused;
                        IsPaused = true;
                        ClearApples();
                        GenerateApples(ApplesMaxCount);
                        var lSw = new Stopwatch();
                        lSw.Start();
                        EvolvePopulation(mObjects);
                        lSw.Stop();
                        //Console.WriteLine("Evolution time: {0} ms", lSw.Elapsed.TotalMilliseconds);
                        //EvolveCommand.Execute(null);
                        IsPaused = lIsPaused;
                    });
                }
			}
		}
		protected override void TickAfterCollisionAction(List<BaseObjectViewModel> aObjectsToDestroy)
		{
			base.TickAfterCollisionAction(aObjectsToDestroy);

			if (mObjects.OfType<FoodViewModel>().Count() > ApplesMaxCount)
			{
				var lAppleToDestroy = mObjects.OfType<FoodViewModel>().First();
				//mObjects.Add(lAppleToDestroy);
				lAppleToDestroy.Destroy();
			}

			foreach (var lObjectToDestroy in aObjectsToDestroy)
			{
				if (lObjectToDestroy is FoodViewModel && (lObjectToDestroy as FoodViewModel).DestroyReason == DestroyReason.Eaten)
				{
					if (!(lObjectToDestroy as FoodViewModel).IsPoisoned)
					{
						Data.GoodApplesEaten++;
						Data.GoodApplesEatenIteration++;
					}
					else
					{
						Data.PoisonedApplesEaten++;
						Data.PoisonedApplesEatenIteration++;
					}
				}
			}
		}
		protected override void TickEndAction(List<BaseObjectViewModel> aObjectsToDestroy)
		{
			base.TickEndAction(aObjectsToDestroy);

            if (mObjects.OfType<FoodViewModel>().Count() < ApplesMaxCount && mRespawnApples)
            {
                GenerateApple();
            }
		}
		private List<MinionViewModel> GetNeuralMinions(IList<BaseObjectViewModel> aObjects)
		{
			var lNeuralMinions = aObjects
				.OfType<MinionViewModel>()
				.Where(aa => aa.Controller is NeuralNetworkController)
				.ToList();
			return lNeuralMinions;
		}
        private List<BaseObjectViewModel> GetAllButNeuralMinions(IList<BaseObjectViewModel> aObjects)
        {
            var lResult = aObjects
                .Where(aa => (!(aa is MinionViewModel) || !((aa as MinionViewModel).Controller is NeuralNetworkController)))
                .ToList();
            return lResult;
        }
        private void GenerateApples(int aCount)
        {
            for (int i = 0; i < aCount; i++)
            {
                GenerateApple();
            }
        }
        private void ClearApples()
        {
            var lAppleList = mObjects.Where(a => a is FoodViewModel).ToList();
            //ViewProvider.ViewService.Execute(() =>
            //{
                foreach (var nApple in lAppleList)
                    mObjects.Remove(nApple);
            //});
            
        }
        private void GenerateApple()
        {
            var lNewFood = TryConstructFoodInstance(mObjects.OfType<MinionViewModel>());
            if (lNewFood != null)
            {
                //lNewFood.IsEatable = mApplesEatable;
                lNewFood.IsPoisoned = mRandom.NextDouble() < mPoisonedChance;

                //if (mObjects.OfType<FoodViewModel>().Where(a => a.IsPoisoned).Count() == 0) lNewFood.IsPoisoned = true;
                //if (mObjects.OfType<FoodViewModel>().Count() == 0) lNewFood.IsPoisoned = false;

                ExecuteOnViewIfPossible(() =>
                {
                    mObjects.Add(lNewFood);
                });
                //var lViewService = ViewProvider.ViewService;
                //lViewService.Execute(() => mObjects.Add(lNewFood));
            }
        }

		private void EvolvePopulation(IList<BaseObjectViewModel> aObjects)
        {
            //Console.WriteLine("This feature is currently not available");
            var lNeuralMinions = GetNeuralMinions(aObjects);
            var lEverythingOther = GetAllButNeuralMinions(aObjects);

            var lTotalScore = lNeuralMinions.Select(aa => aa.Score).Sum();
			//Statistics.RecordThisGeneration(lTotalScore, lNeuralMinions.Count, lNeuralMinions.Max(aa => aa.Score));

			var lMaxScore = lNeuralMinions.Select(aa => aa.Score).Max();
            var lMinScore = lNeuralMinions.Select(aa => aa.Score).Min();

			Data.RecordThisGeneration(lTotalScore, lNeuralMinions.Count, lMaxScore);

			foreach (var lMinion in lNeuralMinions)
            {
                (lMinion.Controller as NeuralNetworkController).CalculateFitness(new NNCFitnessArgument
                {
                    Score = lMinion.Score,
                    MinScore = lMinScore
                });
            }

            var lGASolver =
                new GASolver<NeuralNetworkController, BinaryDna, byte[]>
                    (lNeuralMinions.Select(aa => aa.Controller as NeuralNetworkController)
                    .ToList());
            var lEvolution = new NeuralNetworkControllerEvolution();
            var lNewPopulation = lGASolver.Solve(1, lEvolution, mNeuralNetworkStructure);

            foreach (var lMinion in lNeuralMinions)
            {
                aObjects.Remove(lMinion);
                (lMinion.Controller as NeuralNetworkController).AppleProvider = null;
                (lMinion.Controller as NeuralNetworkController).ControlledObject = null;
                lMinion.Controller = null;
            }
            foreach (var lNewController in lNewPopulation)
            {
                var lNewMinion = CreateNewNNMinion(lNewController);
                aObjects.Add(lNewMinion);
            }
        }
		private MinionViewModel CreateNewNNMinion(NeuralNetworkController aController)
		{
			var lNeuralMinion = CreateNewNNMinionBasic();
			aController.ControlledObject = lNeuralMinion;
			aController.AppleProvider = this;
			aController.RoomLimiter = RoomLimiter;
			aController.MaxSpeed = MinionViewModel.cMaxSpeed;
			lNeuralMinion.Controller = aController;

			return lNeuralMinion;
		}
		private MinionViewModel CreateNewNNMinion(byte[] aControllerNetworkStructure, byte[] aControllerNetworkParameters = null)
		{
			var lNeuralMinion = CreateNewNNMinionBasic();
			var lController = new NeuralNetworkController(aControllerNetworkStructure, aControllerNetworkParameters);

            if (aControllerNetworkParameters == null)
                lController.Randomize();

			lController.ControlledObject = lNeuralMinion;
			lController.AppleProvider = this;
			lController.MaxSpeed = MinionViewModel.cMaxSpeed;
			lController.RoomLimiter = RoomLimiter;
			lNeuralMinion.Controller = lController;
			return lNeuralMinion;
		}
		/// <summary>
		/// Pouze základní generování miniona
		/// Velikost, pozice, barva, natočení, limiter se nastaví na mRoomLimiter
		/// </summary>
		/// <returns></returns>
		private MinionViewModel CreateNewNNMinionBasic()
		{
            //var lX = RoomLimiter.XMin + mRandom.NextDouble() * (RoomLimiter.XMax - RoomLimiter.XMin);
            //var lY = RoomLimiter.YMin + mRandom.NextDouble() * (RoomLimiter.YMax - RoomLimiter.YMin);
            var lX = RoomLimiter.XMin + 0.5 * (RoomLimiter.XMax - RoomLimiter.XMin) + (mRandom.NextDouble() - 0.5) * 10;
            var lY = RoomLimiter.YMin + 0.5 * (RoomLimiter.YMax - RoomLimiter.YMin) + (mRandom.NextDouble() - 0.5) * 10;

            var lSize = 20;
            var lAngle = mRandom.NextDouble() * 360;

            var lNeuralMinion = new MinionViewModel
			{
				X = lX,
				Y = lY,
				Size = lSize,
				RoomLimiter = RoomLimiter,
				BodyColor = mNeuralMinionStandardColor,
				VisorColor = Brushes.SkyBlue,
                Direction = lAngle,
            };
			return lNeuralMinion;
		}
		public void EvolvePopulation()
		{
			EvolvePopulation(mObjects);
		}
		private FoodViewModel TryConstructFoodInstance(IEnumerable<BaseObjectViewModel> aAvoidCollisions = null, int aMaxIterations = 40)
		{
			var lNewObject = (FoodViewModel)null;

			var i = -1;
			var lIsCollision = false;

			do
			{
				lIsCollision = false;
				i++;
				var lX = RoomLimiter.XMin + mRandom.NextDouble() * (RoomLimiter.XMax - RoomLimiter.XMin);
				var lY = RoomLimiter.YMin + mRandom.NextDouble() * (RoomLimiter.YMax - RoomLimiter.YMin);
                lNewObject = new FoodViewModel
                {
                    Size = 16,
                    RoomLimiter = RoomLimiter,
                    X = lX,
                    Y = lY
                };

				if (aAvoidCollisions != null)
				{
					foreach (var lObject in aAvoidCollisions)
					{
						if (lNewObject.HasValidCollision(lObject))
						{
							lIsCollision = true;
							lNewObject.Destroy();
							break;
						}
					}
				}
			}
			while (i < aMaxIterations && lIsCollision);

			if (!lIsCollision) return lNewObject;
			else
			{
				lNewObject.Destroy();
				return null;
			}
		}

		protected override byte[] SaveInternal()
		{
			// Výpočet velikosti pole bajtů
			var lNeuralMinions = GetNeuralMinions(Objects);
			var lStructurePart = (lNeuralMinions[0].Controller as NeuralNetworkController).GetNetworkStructurePart();

			var lDnas = new List<byte[]>();
			var lTotalDnaSize = 0;
			foreach (var lMinion in lNeuralMinions)
			{
				var lDna = (lMinion.Controller as NeuralNetworkController).GetDna();
				lDnas.Add(lDna.Code);
				lTotalDnaSize += lDna.Code.Length;
			}
			//   room limiter
			// + automatic evolution period
			// + dna structure part size + dna structure part 
			// + minion count 
			// + dna sizes 
			// + dna
			var lBytesCount = 
				  4 * sizeof(double)
				+ sizeof(long)
 				+ sizeof(int) + lStructurePart.Length * sizeof(byte)
				+ sizeof(int)
				+ lNeuralMinions.Count * sizeof(int)
				+ lTotalDnaSize * sizeof(byte);
			// Viz dokumentace: https://msdn.microsoft.com/en-us/library/7y0t45kk.aspx
			// Poslední argument true nutný, aby se dalo k poli bajtů zase dostat
			var lBytes = new MemoryStream(new byte[lBytesCount], 0, lBytesCount, true, true);

			// Zápis bajtů
			using (var lBw = new BinaryWriter(lBytes))
			{
				lBw.Write(RoomLimiter.XMin);
				lBw.Write(RoomLimiter.XMax);
				lBw.Write(RoomLimiter.YMin);
				lBw.Write(RoomLimiter.YMax);

				lBw.Write(AutomaticEvolutionPeriod);

				lBw.Write(lStructurePart.Length);
				lBw.Write(lStructurePart);

				lBw.Write(lNeuralMinions.Count);

				foreach (var nDna in lDnas)
				{
					lBw.Write(nDna.Length);
					lBw.Write(nDna);
				}
			}

			// Vrací přímo "underlying array", tzn. to které jsem si vytvořil výše (předpokládám)
			// tzn. lepší než ToArray(), které dělá kopii.
			return lBytes.GetBuffer();
		}
		protected override void LoadInternal(byte[] aArray)
		{
			mObjects.Clear();
			using (var lBr = new BinaryReader(new MemoryStream(aArray, 0, aArray.Length, false)))
			{
				var lXMin = lBr.ReadDouble();
				var lXMax = lBr.ReadDouble();
				var lYMin = lBr.ReadDouble();
				var lYMax = lBr.ReadDouble();
				RoomLimiter = new RoomLimiter { XMin = lXMin, XMax = lXMax, YMin = lYMin, YMax = lYMax };

				AutomaticEvolutionPeriod = lBr.ReadInt64();

				var lDnaStructurePartSize = lBr.ReadInt32();
				var lDnaStructurePart = lBr.ReadBytes(lDnaStructurePartSize);

                mNeuralNetworkStructure = lDnaStructurePart;

				var lMinionCount = lBr.ReadInt32();

				for (int i = 0; i < lMinionCount; i++)
				{
					var lDnaSize = lBr.ReadInt32();
					var lDna = lBr.ReadBytes(lDnaSize);
					//var lNewController = new NeuralNetworkController(lDnaStructurePart, lDna);
					var lNewMinion = CreateNewNNMinion(lDnaStructurePart, lDna);

					mObjects.Add(lNewMinion);
				}
			}
		}

		List<FoodViewModel> IAppleProvider.GetApples()
		{
			return Objects.OfType<FoodViewModel>().ToList();
		}

        public override AppleEatingInitData GetDefaultInitData()
        {
            return new AppleEatingInitData();
        }

        protected override AppleEatingInterfaceToObjects CreateInterface()
        {
            return new AppleEatingInterfaceToObjects(this);
        }
    }
}
