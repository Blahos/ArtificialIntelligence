﻿using SimulationLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuiApp.Simulations.AppleEating
{
    public class AppleEatingInterfaceToObjects : SimulationInterfaceBase
    {
        private readonly AppleEatingSimulation mSimulation;

        public AppleEatingInterfaceToObjects(AppleEatingSimulation aSimulation)
        {
            if (aSimulation == null) throw new ArgumentNullException(nameof(aSimulation));
            mSimulation = aSimulation;
        }

        public bool ApplesEatable { get { return mSimulation.ApplesEatable; } }
    }
}
