﻿using LibraryWpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuiApp
{
	public class SummaryItem : BaseViewModel
	{
		private int mGenerationNumber;
		public int GenerationNumber
		{
			get { return mGenerationNumber; }
			set
			{
				if (mGenerationNumber == value) return;
				mGenerationNumber = value;
				OnPropertyChanged();
			}
		}
		private int mPoisonedApplesEaten;
		public int PoisonedApplesEaten
		{
			get { return mPoisonedApplesEaten; }
			set
			{
				if (mPoisonedApplesEaten == value) return;
				mPoisonedApplesEaten = value;
				OnPropertyChanged();
			}
		}
		private int mGoodApplesEaten;
		public int GoodApplesEaten
		{
			get { return mGoodApplesEaten; }
			set
			{
				if (mGoodApplesEaten == value) return;
				mGoodApplesEaten = value;
				OnPropertyChanged();
			}
		}
		private double mAverageScore;
		public double AverageScore
		{
			get { return mAverageScore; }
			set
			{
				if (mAverageScore == value) return;
				mAverageScore = value;
				OnPropertyChanged();
			}
		}
		private int mBestScore;
		public int BestScore
		{
			get { return mBestScore; }
			set
			{
				if (mBestScore == value) return;
				mBestScore = value;
				OnPropertyChanged();
			}
		}
		private int mTotalScore;
		public int TotalScore
		{
			get { return mTotalScore; }
			set
			{
				if (mTotalScore == value) return;
				mTotalScore = value;
				OnPropertyChanged();
			}
		}
		private long mTicks;
		public long Ticks
		{
			get { return mTicks; }
			set
			{
				if (mTicks == value) return;
				mTicks = value;
				OnPropertyChanged();
			}
		}
	}
}
