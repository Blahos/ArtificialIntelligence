﻿using LibraryWpf;
using SimulationLibrary;
using SimulationLibrary.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuiApp
{
    public class NewSimulationViewModel : BaseViewModel, IDialog
    {
        public ReadOnlyCollection<SimulationSpecification> Simulations
        {
            get;
            private set;
        }

        private SimulationSpecification mSelectedSimulation;
        public SimulationSpecification SelectedSimulation
        {
            get { return mSelectedSimulation; }
            set
            {
                if (value == mSelectedSimulation) return;
                mSelectedSimulation = value;
                OnPropertyChanged();
            }
        }
        public NewSimulationViewModel()
        {
            var lObjectSimulations = SimulationBase.SimulationList.Where(a => a.SimulationType.GetInterfaces().Contains(typeof(IObjectSimulation)) && !a.SimulationType.IsAbstract).ToList();
            Simulations = new ReadOnlyCollection<SimulationSpecification>(lObjectSimulations);
        }

        public DialogResult CheckResult()
        {
            if (SelectedSimulation != null)
                return true;
            return "Select a simulation type!";
        }
    }
}
