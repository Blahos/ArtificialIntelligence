﻿using LibraryWpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace GuiApp
{
	public class ViewService : BaseViewService
	{
		private readonly Dictionary<Type, Func<System.Windows.Controls.Control>> mControls = new Dictionary<Type, Func<System.Windows.Controls.Control>>
		{
			{ typeof(SummaryViewModel), () => new SummaryUserControl() },
            { typeof(NewSimulationViewModel), () => new NewSimulationControl() }
		};
		protected override Dictionary<Type, Func<System.Windows.Controls.Control>> Controls
		{
			get { return mControls; }
		}
		public ViewService(Dispatcher aDispatcher, Window aWindow)
			: base(aDispatcher, aWindow)
		{

		}
	}
}
