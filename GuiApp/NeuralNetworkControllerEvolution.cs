﻿using GeneticAlgorithmLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuiApp
{
	public class NeuralNetworkControllerEvolution : IEvolution<NeuralNetworkController, BinaryDna, byte[]>
	{
		public Tuple<NeuralNetworkController, NeuralNetworkController> SelectCouple(List<NeuralNetworkController> aPopulation, int aSelectionIndex)
		{
			if (aSelectionIndex == 0)
			{
				var lTwoBest = aPopulation.OrderByDescending(a => a.Fitness).Take(2).ToList();
				return new Tuple<NeuralNetworkController, NeuralNetworkController>(lTwoBest[0], lTwoBest[1]);
			}
			var lNewCouple = SelectingFunctions.Select<NeuralNetworkController, BinaryDna, NNCFitnessArgument, byte[]>(aPopulation);
			return lNewCouple;
		}

		public Tuple<BinaryDna, BinaryDna> Cross(BinaryDna aDna1, BinaryDna aDna2, int aSelectionIndex)
		{
			if (aSelectionIndex == 0)
			{
				var lDna1 = new BinaryDna { Code = new byte[aDna1.Code.Length] };
				Buffer.BlockCopy(aDna1.Code, 0, lDna1.Code, 0, lDna1.Code.Length);
				var lDna2 = new BinaryDna { Code = new byte[aDna2.Code.Length] };
				Buffer.BlockCopy(aDna2.Code, 0, lDna2.Code, 0, lDna2.Code.Length);

				return new Tuple<BinaryDna, BinaryDna>(lDna1, lDna2);
			}

			var lNewDna = CrossingFunctions.Cross(aDna1, aDna2, 0.8);
			return lNewDna;
		}

		public BinaryDna Mutate(BinaryDna aSourceDna, int aSelectionIndex, int aMutationIndex)
		{
			var lNewDna = MutatingFunctions.MutateWithChance(aSourceDna, 0.5);
			return lNewDna;
		}
	}
}
