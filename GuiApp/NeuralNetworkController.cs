﻿using GeneticAlgorithmLibrary;
using GuiApp.Simulations.AppleEating;
using Library;
using NeuralNetworkLibrary;
using SimulationLibrary;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuiApp
{
	public class NeuralNetworkController : FitnessPopulationMember<BinaryDna, NNCFitnessArgument, byte[]>, IController
	{
		private NeuralNetwork mNetwork = null;
		private static readonly Random cRandom = new Random();

		public double MaxSpeed { get; set; }
		public IPositionMovement ControlledObject { get; set; }
		public IAppleProvider AppleProvider { get; set; }
		public RoomLimiter RoomLimiter { get; set; }
		private Dictionary<Guid, double[]> mLastInputRequest = new Dictionary<Guid, double[]> { { Guid.Empty, new double[0] } };
		public NeuralNetworkController(byte[] aNetworkStructure, byte[] aNetworkParameters = null)
			: this()
		{
			Initialize(aNetworkStructure);
			if (aNetworkParameters != null)
				mNetwork.SetParameters(aNetworkParameters);
		}
		public NeuralNetworkController()
		{
			MaxSpeed = 1;
		}
		public byte[] GetNetworkStructurePart()
		{
			return mNetwork.GetStructurePart();
		}
		public byte[] GetNetworkParameterPart()
		{
			return mNetwork.GetParameterPart();
		}
		private List<Tuple<IPosition, double>> FindClosestObjects<T>(List<T> aObjects, int aN)
			where T : IPosition
		{
			var lReturnList = new List<Tuple<IPosition, double>>();

			foreach (var lObject in aObjects)
			{
				var lDistance = (ControlledObject.X - lObject.X) * (ControlledObject.X - lObject.X) + (ControlledObject.Y - lObject.Y) * (ControlledObject.Y - lObject.Y);
				lDistance = Math.Sqrt(lDistance);

				var lPositionInTheList = 0;
				while (lPositionInTheList < lReturnList.Count)
				{
					var lComparedObject = lReturnList[lPositionInTheList];
					if (lComparedObject.Item2 <= lDistance)
					{
						lPositionInTheList++;
						continue;
					}
					else break;
				}
				if (lPositionInTheList < aN)
				{
					lReturnList.Insert(lPositionInTheList, new Tuple<IPosition, double>(lObject, lDistance));
					if (lReturnList.Count > aN) lReturnList.RemoveAt(lReturnList.Count - 1);
				}
			}
			return lReturnList;
		}
		/// <summary>
		/// Ve stupních
		/// </summary>
		/// <param name="aObject"></param>
		/// <returns></returns>
		private double GetAngleTowardsObject(IPosition aObject)
		{
			var lXDiff = aObject.X - ControlledObject.X;
			var lYDiff = aObject.Y - ControlledObject.Y;
			var lDirection = Math.Atan2(-lYDiff, lXDiff) * 180 / Math.PI;
			var lAngleDiff = lDirection - ControlledObject.Direction;

			while (lAngleDiff < 0) lAngleDiff += 360;
			while (lAngleDiff >= 360) lAngleDiff -= 360;
			if (lAngleDiff > 180) lAngleDiff = 360 - lAngleDiff;

			return lAngleDiff;
		}
		private double[] GetInputs(Guid aRequestId)
		{
			if (mLastInputRequest.ContainsKey(aRequestId)) return mLastInputRequest[aRequestId];

			mLastInputRequest.Clear();

			var lBigDistance = Math.Pow(double.MaxValue, 1.0 / 20);

			var lClosestNormalAppleDistance = lBigDistance;
			var lClosestPoisonedAppleDistance = lBigDistance;
			var lClosestNormalAppleAngle = 0.0;
			var lCosestPoisonedAppleAngle = 0.0;

			var lSecondClosestNormalAppleDistance = lBigDistance;
			var lSecondClosestPoisonedAppleDistance = lBigDistance;
			var lSecondClosestNormalAppleAngle = 0.0;
			var lSecondCosestPoisonedAppleAngle = 0.0;

			if (AppleProvider != null)
			{
				var lNormalApples = AppleProvider.GetApples().Where(a => !a.IsPoisoned).ToList();
				var lPoisonedApples = AppleProvider.GetApples().Where(a => a.IsPoisoned).ToList();

				var lClosestNormalApples = FindClosestObjects(lNormalApples, 2);
				var lClosestPoisonedApples = FindClosestObjects(lPoisonedApples, 2);

				if (lClosestNormalApples.Count > 0)
				{
					lClosestNormalAppleDistance = lClosestNormalApples[0].Item2;
					lClosestNormalAppleAngle = GetAngleTowardsObject(lClosestNormalApples[0].Item1);
				}
				if (lClosestPoisonedApples.Count > 0)
				{
					lClosestPoisonedAppleDistance = lClosestPoisonedApples[0].Item2;
					lCosestPoisonedAppleAngle = GetAngleTowardsObject(lClosestPoisonedApples[0].Item1);
				}
				if (lClosestNormalApples.Count > 1)
				{
					lSecondClosestNormalAppleDistance = lClosestNormalApples[1].Item2;
					lSecondClosestNormalAppleAngle = GetAngleTowardsObject(lClosestNormalApples[1].Item1);
				}
				if (lClosestPoisonedApples.Count > 1)
				{
					lSecondClosestPoisonedAppleDistance = lClosestPoisonedApples[1].Item2;
					lSecondCosestPoisonedAppleAngle = GetAngleTowardsObject(lClosestPoisonedApples[1].Item1);
				}
			}

			var lHalfDiagonal = 1.0;
			Debug.Assert(RoomLimiter != null);
			if (RoomLimiter != null)
			{
				lHalfDiagonal = Math.Sqrt((RoomLimiter.XMax - RoomLimiter.XMin) * (RoomLimiter.XMax - RoomLimiter.XMin) + (RoomLimiter.YMax - RoomLimiter.YMin) * (RoomLimiter.YMax - RoomLimiter.YMin))
					/ 2;
			}

			// Všechno to dělení těch hodnot je kvůli normalizaci dat. Takhle budou všechny v rozsahu buď 0 až 2 nebo -1 až 1
			var lInputs = new[] 
			{
                lClosestNormalAppleDistance / lHalfDiagonal,
                //lClosestPoisonedAppleDistance / lHalfDiagonal,
                //lSecondClosestNormalAppleDistance / lHalfDiagonal,
				//lSecondClosestPoisonedAppleDistance / lHalfDiagonal, 
				lClosestNormalAppleAngle / 180,
                //lCosestPoisonedAppleAngle / 180,
                //lSecondClosestNormalAppleAngle / 180, 
				//lSecondCosestPoisonedAppleAngle / 180,
				ControlledObject.Speed / MaxSpeed * 2,
                //2 * ControlledObject.X / RoomLimiter.XMax - 1,
                //2 * ControlledObject.Y / RoomLimiter.YMax - 1
			};
			
			mLastInputRequest.Add(aRequestId, lInputs);

			return lInputs;
		}
		public double GetAngularSpeed(double aMaxAngularSpeed, Guid aRequestId)
		{
			var lInputs = GetInputs(aRequestId);

			var lOutputs = mNetwork.GetValues(lInputs.ToList());
			var lOutput = lOutputs[1];

			var lAngularSpeed = (2 * lOutput - 1) * aMaxAngularSpeed;

			return lAngularSpeed;
		}
		public double GetAcceleration(double aMaxAcceleration, Guid aRequestId)
		{
			var lInputs = GetInputs(aRequestId);
			var lOutputs = mNetwork.GetValues(lInputs.ToList());
			var lOutput = lOutputs[0];

			return (2 * lOutput - 1) * aMaxAcceleration;
		}

		protected override double CalculateFitnessInner(NNCFitnessArgument aArgument)
		{
			return aArgument.Score - aArgument.MinScore + 1;
		}

		public override void Initialize(byte[] aInitArgument)
		{
			if (mNetwork != null)
			{
				Console.WriteLine("Multiple initialization not allowed!");
				return;
			}
			mNetwork = NeuralNetwork.FromBinaryStructureOnly(aInitArgument);
		}

		public override BinaryDna GetDna()
		{
			return new BinaryDna { Code = mNetwork.GetParameterPart() };
		}

		public override void ConstructFromDna(BinaryDna aDna)
		{
			mNetwork.SetParameters(aDna.Code);
		}

		public void Randomize()
		{
			foreach (var lNode in mNetwork.InnerNodes)
			{
				lNode.Bias = cRandom.NextDouble() * 1000 - 500;
			}
			foreach (var lConnection in mNetwork.Connections)
			{
				lConnection.Weight = cRandom.NextDouble() * 1000 - 500;
			}
		}
	}
}
