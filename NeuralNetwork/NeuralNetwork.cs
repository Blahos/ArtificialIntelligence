﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetworkLibrary
{
	public class NeuralNetwork
	{
		public List<NodeInner> InnerNodes { get; set; }
		public List<NodeInput> InputNodes { get; set; }
		public List<Connection> Connections { get; set; }
		public List<ConnectionOutput> Outputs { get; set; }
        /// <summary>
        /// Využívaný pro převod do a z binární podoby
        /// </summary>
		public int Denominator { get; set; }
		public NeuralNetwork()
		{
			Denominator = (int)Math.Sqrt(Int32.MaxValue);
			InnerNodes = new List<NodeInner>();
			InputNodes = new List<NodeInput>();
			Connections = new List<Connection>();
			Outputs = new List<ConnectionOutput>();
		}

        public void RandomiseWeights(double aMinValue = -1, double aMaxValue = 1, Random aRandom = null)
        {
            Debug.Assert(aMinValue < aMaxValue);
            if (aRandom == null) aRandom = new Random();
            foreach (var nConnection in Connections)
            {
                nConnection.Weight = aRandom.NextDouble() * (aMaxValue - aMinValue) + aMinValue;
            }
        }
        public void RandomiseBiases(double aMinValue = -1, double aMaxValue = 1, Random aRandom = null)
        {
            Debug.Assert(aMinValue < aMaxValue);
            if (aRandom == null) aRandom = new Random();
            foreach (var nNode in InnerNodes)
            {
                nNode.Bias = aRandom.NextDouble() * (aMaxValue - aMinValue) + aMinValue;
            }
        }
        public void RandomiseSteepnesses(double aMinValue = -1, double aMaxValue = 1, Random aRandom = null)
        {
            Debug.Assert(aMinValue < aMaxValue);
            if (aRandom == null) aRandom = new Random();
            foreach (var nNode in InnerNodes)
            {
                nNode.Steepness = aRandom.NextDouble() * (aMaxValue - aMinValue) + aMinValue;
            }
        }
        public void Train(List<double> aCaseInputs, double aCaseOutput, double aStepSize)
        {
            Train(aCaseInputs, new List<double> { aCaseOutput }, aStepSize);
        }
        public void Train(List<double> aCaseInputs, List<double> aCaseOutputs, double aStepSize)
        {
            Debug.Assert(aCaseInputs != null);
            Debug.Assert(InputNodes != null);
            Debug.Assert(aCaseInputs.Count == InputNodes.Count);
            Debug.Assert(Outputs != null);
            Debug.Assert(aCaseOutputs != null);
            Debug.Assert(Outputs.Count == aCaseOutputs.Count);
            Debug.Assert(aStepSize > 0);

            for (int i = 0; i < aCaseInputs.Count; i++)
            {
                InputNodes[i].InputValue = aCaseInputs[i];
            }

            var lInnerNodesSorted = InnerNodes.OrderByDescending(a => a.LevelInNetwork).ToList();
            var lLastLevel = lInnerNodesSorted[0].LevelInNetwork;

            // Derivative values of the error function
            // Derivatives with respect to weights, biases and steepnesses
            // z is used as an intermediate variable
            var lDEbyDz = new Dictionary<NodeInner, double>();
            var lDEbyDw = new Dictionary<Connection, double>();
            var lDEbyDb = new Dictionary<NodeInner, double>();
            var lDEbyDA = new Dictionary<NodeInner, double>(); // alpha, i.e. the steepness parameter

            // All nodes from the layer previous to the one being processed now
            var lPreviousLayerNodes = new List<NodeInner>();
            var lCurrentLayerNodes = new List<NodeInner>();

            foreach (var nNode in lInnerNodesSorted)
            {
                var lCurrentLevel = nNode.LevelInNetwork;
                // Shift layers
                if (lCurrentLayerNodes.Count > 0 && lCurrentLayerNodes[0].LevelInNetwork > lCurrentLevel)
                {
                    lPreviousLayerNodes = lCurrentLayerNodes;
                    lCurrentLayerNodes = new List<NodeInner>();
                }
                lCurrentLayerNodes.Add(nNode);

                // sum w_iy_i + b, without steepness
                var lAuxSum = 0.0;
                foreach (var nInput in nNode.InputConnections)
                {
                    lAuxSum += nInput.GetValue();
                }
                lAuxSum += nNode.Bias;

                // DE by Dz
                var lDerivativeValue = (double)0;

                // Output value of this node
                var lOutputValue = nNode.GetValue();

                // z of the current node
                var lDybyDz = lOutputValue * (1 - lOutputValue);

                if (lCurrentLevel == lLastLevel)
                {
                    var lOutputIndex = Outputs.IndexOf(Outputs.First(a => a.InputNode == nNode));

                    // Předpokládá se, že ConnectionOutput pouze přeposílá výstupní hodnotu 
                    // svého vstupního uzlu. Kdyby se tohle změnilo tak se musí upravit i toto.
                    // Možná že by stálo za to celou třídu ConnectionOutput smazat a brát výstupy
                    // přímo z uzlů poslední vrstvy, když se ta hodnota stejně jen přeposílá.

                    // Pokud by ConnectionOutput něco dělal s výstupem,
                    // tak by se musel používat až výstup toho ConnectionOutput, 
                    // a ne výstup uzlu, jak se používá tady 
                    // (v lDybyDz se používá lOutputValue, která je už z uzlu, ne až z ConnectionOutput)

                    var lCaseOutputValue = aCaseOutputs[lOutputIndex];
                    lDerivativeValue = (lOutputValue - lCaseOutputValue) * lDybyDz;
                }
                else
                {
                    foreach (var nPrevNode in lPreviousLayerNodes)
                    {
                        // Connection between the currently processed node (nNode) and his predecessor nPrevNode
                        var lConnection = nPrevNode.InputConnections.First(a => a.InputNode == nNode);
                        // z of the previous node
                        var lDzbyDy = lConnection.Weight * nPrevNode.Steepness;
                        lDerivativeValue += lDEbyDz[nPrevNode] * lDzbyDy * lDybyDz;
                    }
                }

                // Note: A problem could occur of this aux sum is zero and also steepness is zero. Then the network would get stuck on zero gradient point,
                // which wouldnt be a minimum, but probably a saddle point
                // This is because the steepness parameter is actually redundant, so if both the sum and the steepness are zero, 
                // there are two (opposite and equal in quality) ways to improve the parameters (decrease the error).

                lDEbyDz.Add(nNode, lDerivativeValue);
                lDEbyDb.Add(nNode, lDerivativeValue * nNode.Steepness);
                lDEbyDA.Add(nNode, lDerivativeValue * lAuxSum);
            }

            foreach (var nConnection in Connections)
            {
                var lInputNode = nConnection.InputNode;
                var lOutputNode = InnerNodes.First(a => a.InputConnections.Contains(nConnection));

                lDEbyDw.Add(nConnection, lDEbyDz[lOutputNode] * lInputNode.GetValue() * lOutputNode.Steepness);
            }

            // Posun v proti směru gradientu -> optimalizace parametrů sítě
            foreach (var nConnection in Connections)
            {
                nConnection.Weight -= lDEbyDw[nConnection] * aStepSize;
            }
            foreach (var nNode in InnerNodes)
            {
                nNode.Bias -= lDEbyDb[nNode] * aStepSize;
            }
            foreach (var nNode in InnerNodes)
            {
                nNode.Steepness -= lDEbyDA[nNode] * aStepSize;
            }
        }
        /// <summary>
        /// Calculates error value for the given input output combination
        /// E = 1/2*(Y(x) - y(x))^2
        /// </summary>
        /// <param name="aCaseInputs"></param>
        /// <param name="aCaseOutputs"></param>
        public double GetError(List<double> aCaseInputs, double aCaseOutput)
        {
            return GetError(aCaseInputs, new List<double> { aCaseOutput });
        }
        /// <summary>
        /// Calculates error value for the given input output combination
        /// E = 1/2*(Y(x) - y(x))^2
        /// </summary>
        /// <param name="aCaseInputs"></param>
        /// <param name="aCaseOutputs"></param>
        public double GetError(List<double> aCaseInputs, List<double> aCaseOutputs)
        {
            Debug.Assert(aCaseInputs != null);
            Debug.Assert(InputNodes != null);
            Debug.Assert(aCaseInputs.Count == InputNodes.Count);
            Debug.Assert(Outputs != null);
            Debug.Assert(aCaseOutputs != null);
            Debug.Assert(Outputs.Count == aCaseOutputs.Count);

            var lNetworkOutputs = GetValues(aCaseInputs);

            var lTotalError = 0.0;

            for (int i = 0; i < lNetworkOutputs.Count; i++)
            {
                lTotalError += (lNetworkOutputs[i] - aCaseOutputs[i]) * (lNetworkOutputs[i] - aCaseOutputs[i]);
            }
            lTotalError *= 0.5;
            return lTotalError;
        }
        /// <summary>
        /// Vrátí první z výstupů sítě - hodí se u sítě s jedním výstupem
        /// </summary>
        /// <param name="aInputs"></param>
        /// <returns></returns>
        public double GetValue(params double[] aInputs)
        {
            return GetValues(aInputs)[0];
        }
        public List<double> GetValues(params double[] aInputs)
        {
            return GetValues(aInputs.ToList());
        }
		public List<double> GetValues(List<double> aInputs)
		{
			Debug.Assert(aInputs != null);
			Debug.Assert(InputNodes != null);
			Debug.Assert(aInputs.Count == InputNodes.Count);
			Debug.Assert(Outputs != null);

			for (int i = 0; i < aInputs.Count; i++)
			{
				InputNodes[i].InputValue = aInputs[i];
			}

			var lOutputs = Outputs.Select(a => a.GetValue()).ToList();
			return lOutputs;
		}
		public byte[] GetStructurePart()
		{
			var lInputsCount = InputNodes.Count;
			var lInnerNodesCount = InnerNodes.Count;
			var lNodeInputConnectionsCount = InnerNodes.SelectMany(a => a.InputConnections).Count();
			var lInnerConnectionsCount = Connections.Count;
			var lOutputConnectionsCount = Outputs.Count;

			// Každá hodnota bude int => 4 *
			//   Počet vstupních uzlů + počet vnitřních uzlů 
			// + počet výstupních spojů + počet vnitřních spojů
			// + počty vstupních spojů pro vnitřní uzly 
			// + indexy všech vstupních spojů pro vnitřní uzly 
			// + indexy vstupních uzlů pro všechny spoje (první všechny výstupní, pak vnitřní)
			var lArraySize = 4 * (
				1 + 1
				+ 1 + 1
				+ lInnerNodesCount
				+ lNodeInputConnectionsCount
				+ lOutputConnectionsCount + lInnerConnectionsCount
			);

			var lArray = new byte[lArraySize];

			using (var lStream = new BinaryWriter(new MemoryStream(lArray)))
			{
				lStream.Write(lInputsCount); // + počet vstupních uzlů
				lStream.Write(lInnerNodesCount); // + počet vnitřních uzlů 
				lStream.Write(lOutputConnectionsCount); // + počet výstupních spojů
				lStream.Write(lInnerConnectionsCount); // + počet vnitřních spojů

				foreach (var lInnerNode in InnerNodes) // + počty vstupních spojů pro vnitřní uzly 
				{
					lStream.Write(lInnerNode.InputConnections.Count);
				}

				foreach (var lInnerNode in InnerNodes) // + indexy všech vstupních spojů pro vnitřní uzly 
				{
					foreach (var lConnection in lInnerNode.InputConnections)
					{
						var lIndex = Connections.IndexOf(lConnection);
						lStream.Write(lIndex);
					}
				}

				foreach (var lOutput in Outputs) // + indexy vstupních uzlů pro všechny spoje (první všechny výstupní, ...
				{
					if (lOutput.InputNode is NodeInput)
					{
						lStream.Write(~InputNodes.IndexOf(lOutput.InputNode as NodeInput));
					}
					else
					{
						lStream.Write(InnerNodes.IndexOf(lOutput.InputNode as NodeInner));
					}
				}

				foreach (var lConnection in Connections) // ... pak vnitřní)
				{
					if (lConnection.InputNode is NodeInput)
					{
						lStream.Write(~InputNodes.IndexOf(lConnection.InputNode as NodeInput));
					}
					else
					{
						lStream.Write(InnerNodes.IndexOf(lConnection.InputNode as NodeInner));
					}
				}

				// Konec části popisující strukturu sítě
			}

			return lArray;
		}
		public byte[] GetParameterPart()
		{
			var lInputsCount = InputNodes.Count;
			var lInnerNodesCount = InnerNodes.Count;
			var lNodeInputConnectionsCount = InnerNodes.SelectMany(a => a.InputConnections).Count();
			var lInnerConnectionsCount = Connections.Count;

            // Každá hodnota bude int => sizeof(int) *
			//   Hodnota jmenovatele pro plovoucí řádové čárky
			// + biasy vnitřních uzlů 
            // + strmosti vnitřních uzlů
			// + váhy spojů (vnitřních)
			var lArraySize = sizeof(int) * (
				+ 1
                + lInnerNodesCount
                + lInnerNodesCount
				+ lInnerConnectionsCount
			);

			var lArray = new byte[lArraySize];

			using (var lStream = new BinaryWriter(new MemoryStream(lArray)))
			{
				// Začátek části popisující parametry sítě
				lStream.Write(Denominator); // Hodnota jmenovatele pro plovoucí řádové čárky

				foreach (var lInnerNode in InnerNodes) // + biasy vnitřních uzlů
				{
					lStream.Write((int)Math.Round(lInnerNode.Bias * Denominator));
                    lStream.Write((int)Math.Round(lInnerNode.Steepness * Denominator));
                }

				foreach (var lConnection in Connections) // + váhy vnitřních spojů
                {
					lStream.Write((int)Math.Round(lConnection.Weight * Denominator));
				}
			}

			return lArray;
		}
		/// <summary>
		/// Bacha, je třeba vložit bajty, které vznikly ze sítě se stejnou strukturou.
		/// </summary>
		/// <param name="aParameterBytes"></param>
		public void SetParameters(byte[] aParameterBytes, long aStartPosition = 0)
		{
			using (var lStream = new BinaryReader(new MemoryStream(aParameterBytes)))
			{
				lStream.BaseStream.Position = aStartPosition;

                // + hodnota jmenovatele pro plovoucí řádové čárky
                // + biasy vnitřních uzlů 
                // + strmosti vnitřních uzlů 
                // + váhy spojů (první všechny výstupní, pak vnitřní)

                // Začátek části popisující parametry sítě
                var lDenominator = lStream.ReadInt32();
				Denominator = lDenominator;

				for (int i = 0; i < InnerNodes.Count; i++)
				{
					var lBiasInt = lStream.ReadInt32();
					InnerNodes[i].Bias = (double)lBiasInt / Denominator;

                    var lSteepnessInt = lStream.ReadInt32();
                    InnerNodes[i].Steepness = (double)lSteepnessInt / Denominator;
                }

				for (int i = 0; i < Connections.Count; i++)
				{
					var lWeightInt = lStream.ReadInt32();
					Connections[i].Weight = (double)lWeightInt / Denominator;
				}

				// Konec části popisující parametry sítě
			}
		}
		public byte[] ToBinary(byte[] aStructurePart = null)
		{
			var lStructurePart = aStructurePart ?? GetStructurePart();
			var lParameterPart = GetParameterPart();

			var lResultArray = new byte[lStructurePart.Length + lParameterPart.Length];
			Buffer.BlockCopy(lStructurePart, 0, lResultArray, 0, lStructurePart.Length);
			Buffer.BlockCopy(lParameterPart, 0, lResultArray, lStructurePart.Length, lParameterPart.Length);
			return lResultArray;
		}
		/// <summary>
		/// Vytvoří pouze síť s danou strukturou, nenastavuje parametry
		/// => stačí strukturální část pole, ale může být i polé célé sítě 
		/// (i s parametrickou částí)
		/// </summary>
		/// <param name="aBytes"></param>
		/// <param name="aFinalPosition">Pozice, na které skončí stream v poli bajtů, když skončí čtení
		/// => první byte za koncem strukturální části</param>
		/// <returns></returns>
		public static NeuralNetwork FromBinaryStructureOnly(byte[] aBytes, out long aFinalPosition)
		{
			var lReturnNetwork = new NeuralNetwork();

			//   Počet vstupních uzlů + počet vnitřních uzlů 
			// + počet výstupních spojů + počet vnitřních spojů
			// + počty vstupních spojů pro vnitřní uzly 
			// + indexy všech vstupních spojů pro vnitřní uzly 
			// + indexy vstupních uzlů pro všechny spoje (první všechny výstupní, pak vnitřní)

			using (var lStream = new BinaryReader(new MemoryStream(aBytes)))
			{
				var lInputsCount = lStream.ReadInt32();
				for (int i = 0; i < lInputsCount; i++) lReturnNetwork.InputNodes.Add(new NodeInput());

				var lInnerNodesCount = lStream.ReadInt32();
				for (int i = 0; i < lInnerNodesCount; i++) lReturnNetwork.InnerNodes.Add(new NodeInner());

				var lOuputsCount = lStream.ReadInt32();
				for (int i = 0; i < lOuputsCount; i++) lReturnNetwork.Outputs.Add(new ConnectionOutput());

				var lInnerConnectionsCount = lStream.ReadInt32();
				for (int i = 0; i < lInnerConnectionsCount; i++) lReturnNetwork.Connections.Add(new Connection());

				var lInnerNodesConnectionsCount = new List<int>();
				for (int i = 0; i < lInnerNodesCount; i++)
				{
					var lCount = lStream.ReadInt32();
					lInnerNodesConnectionsCount.Add(lCount);
				}

				for (int iNode = 0; iNode < lInnerNodesCount; iNode++)
				{
					for (int iConnection = 0; iConnection < lInnerNodesConnectionsCount[iNode]; iConnection++)
					{
						var lConenctionIndex = lStream.ReadInt32();
						lReturnNetwork.InnerNodes[iNode].InputConnections.Add(lReturnNetwork.Connections[lConenctionIndex]);
					}
				}

				for (int i = 0; i < lOuputsCount; i++)
				{
					var lNodeIndex = lStream.ReadInt32();

					if (lNodeIndex < 0)
					{
						lNodeIndex = ~lNodeIndex;
						lReturnNetwork.Outputs[i].InputNode = lReturnNetwork.InputNodes[lNodeIndex];
					}
					else
					{
						lReturnNetwork.Outputs[i].InputNode = lReturnNetwork.InnerNodes[lNodeIndex];
					}
				}

				for (int i = 0; i < lInnerConnectionsCount; i++)
				{
					var lNodeIndex = lStream.ReadInt32();

					if (lNodeIndex < 0)
					{
						lNodeIndex = ~lNodeIndex;
						lReturnNetwork.Connections[i].InputNode = lReturnNetwork.InputNodes[lNodeIndex];
					}
					else
					{
						lReturnNetwork.Connections[i].InputNode = lReturnNetwork.InnerNodes[lNodeIndex];
					}
				}

				// Konec části popisující strukturu sítě
				aFinalPosition = lStream.BaseStream.Position;
				return lReturnNetwork;
			}
		}
		/// <summary>
		/// Vytvoří pouze síť s danou strukturou, nenastavuje parametry
		/// => stačí strukturální část pole, ale může být i polé célé sítě 
		/// (i s parametrickou částí)
		/// </summary>
		/// <param name="aBytes"></param>
		/// <returns></returns>
		public static NeuralNetwork FromBinaryStructureOnly(byte[] aBytes)
		{
			long lDumb;
			return FromBinaryStructureOnly(aBytes, out lDumb);
		}
		public static NeuralNetwork FromBinary(byte[] aBytes)
		{
            //   Počet vstupních uzlů + počet vnitřních uzlů 
            // + počet výstupních spojů + počet vnitřních spojů
            // + počty vstupních spojů pro vnitřní uzly 
            // + indexy všech vstupních spojů pro vnitřní uzly 
            // + indexy vstupních uzlů pro všechny spoje (první všechny výstupní, pak vnitřní)
            // + hodnota jmenovatele pro plovoucí řádové čárky
            // + biasy vnitřních uzlů 
            // + strmosti vnitřních uzlů 
            // + váhy spojů (první všechny výstupní, pak vnitřní)

            long lParameterPartStartIndex;
			var lReturnNetwork = FromBinaryStructureOnly(aBytes, out lParameterPartStartIndex);
			// Konec části popisující strukturu sítě
			// Začátek části popisující parametry sítě
			lReturnNetwork.SetParameters(aBytes, lParameterPartStartIndex);
			// Konec části popisující parametry sítě

			return lReturnNetwork;
		}
		public void SaveToTextFile(string aPath)
		{
			using (var lSw = new StreamWriter(File.OpenWrite(aPath)))
			{
				// Úvodní kecy
				lSw.WriteLine("Neural network file");
				lSw.WriteLine("Saved at: {0}", DateTime.Now);

				// Počty
				lSw.WriteLine("Input nodes count: ");
				lSw.WriteLine(InputNodes.Count);
				lSw.WriteLine("Inner nodes count: ");
				lSw.WriteLine(InnerNodes.Count);
				lSw.WriteLine("Output count: ");
				lSw.WriteLine(Outputs.Count);
				lSw.WriteLine("Connections count: ");
				lSw.WriteLine(Connections.Count);

				// Indexy vstupních spojů
				lSw.WriteLine("Input connections for inner nodes: ");
				foreach (var lNode in InnerNodes)
				{
					foreach (var lConnection in lNode.InputConnections)
					{
						var lIndex = Connections.IndexOf(lConnection);
						lSw.Write(lIndex + ";");
					}
					lSw.WriteLine();
				}

				// Indexy vstupních uzlů výstupních spojů - první vstupní uzly (inputnode), za nima vnitřní
				lSw.WriteLine("Input nodes for output connections: (input nodes first, inner indexed after them)");
				foreach (var lConnection in Outputs)
				{
					int lIndex;
					if (lConnection.InputNode is NodeInput) lIndex = InputNodes.IndexOf(lConnection.InputNode as NodeInput);
					else lIndex = InputNodes.Count + InnerNodes.IndexOf(lConnection.InputNode as NodeInner);
					lSw.WriteLine(lIndex);
				}

				// Indexy vstupních uzlů vnitřních spojů - první vstupní uzly (inputnode), za nima vnitřní
				lSw.WriteLine("Input nodes for inner connections: (input nodes first, inner indexed after them)");
				foreach (var lConnection in Connections)
				{
					int lIndex;
					if (lConnection.InputNode is NodeInput) lIndex = InputNodes.IndexOf(lConnection.InputNode as NodeInput);
					else lIndex = InputNodes.Count + InnerNodes.IndexOf(lConnection.InputNode as NodeInner);
					lSw.WriteLine(lIndex);
				}

                // Čísla vrstvy vnitřních uzlů
                lSw.WriteLine("Levels: ");
                foreach (var lNode in InnerNodes)
                {
                    lSw.WriteLine((lNode.LevelInNetwork ?? -1).ToString(CultureInfo.InvariantCulture));
                }

                // Biasy vnitřních uzlů
                lSw.WriteLine("Biases: ");
				foreach (var lNode in InnerNodes)
				{
					lSw.WriteLine(lNode.Bias.ToString(CultureInfo.InvariantCulture));
                }

                // Strmosti vnitřních uzlů
                lSw.WriteLine("Steepnesses: ");
                foreach (var lNode in InnerNodes)
                {
                    lSw.WriteLine(lNode.Steepness.ToString(CultureInfo.InvariantCulture));
                }

                // Váhy spojů
                lSw.WriteLine("Inner connections weights: ");
				foreach (var lConnection in Connections)
				{
					lSw.WriteLine(lConnection.Weight.ToString(CultureInfo.InvariantCulture));
				}
			}
		}
		public static NeuralNetwork FromTextFile(string aPath)
		{
			var lNetwork = new NeuralNetwork();
			using (var lSr = File.OpenText(aPath))
			{
				// Úvodní kecy
				lSr.ReadLine();
				lSr.ReadLine();

				// Počty
				lSr.ReadLine();

				var lInputCount = Int32.Parse(lSr.ReadLine());
				for (int i = 0; i < lInputCount; i++) lNetwork.InputNodes.Add(new NodeInput());
				lSr.ReadLine();

				var lInnerNodeCount = Int32.Parse(lSr.ReadLine());
				for (int i = 0; i < lInnerNodeCount; i++) lNetwork.InnerNodes.Add(new NodeInner());
				lSr.ReadLine();

				var lOutputCount = Int32.Parse(lSr.ReadLine());
				for (int i = 0; i < lOutputCount; i++) lNetwork.Outputs.Add(new ConnectionOutput());
				lSr.ReadLine();

				var lConnectionCount = Int32.Parse(lSr.ReadLine());
				for (int i = 0; i < lConnectionCount; i++) lNetwork.Connections.Add(new Connection());
				lSr.ReadLine();

				// Indexy vstupních spojů
				for (int i = 0; i < lInnerNodeCount; i++)
				{
					var lIndexes = lSr.ReadLine();
					var lIndexesSplit = lIndexes.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
					foreach (var lIndexString in lIndexesSplit)
					{
						var lIndex = Int32.Parse(lIndexString);
						lNetwork.InnerNodes[i].InputConnections.Add(lNetwork.Connections[lIndex]);
					}
				}
				lSr.ReadLine();

				// Indexy vstupních uzlů výstupních spojů - první vstupní uzly (inputnode), za nima vnitřní
				for (int i = 0; i < lOutputCount; i++)
				{
					var lIndex = Int32.Parse(lSr.ReadLine());
					if (lIndex < lInputCount) lNetwork.Outputs[i].InputNode = lNetwork.InputNodes[lIndex];
					else lNetwork.Outputs[i].InputNode = lNetwork.InnerNodes[lIndex - lInputCount];
				}
				lSr.ReadLine();

				// Indexy vstupních uzlů vnitřních spojů - první vstupní uzly (inputnode), za nima vnitřní
				for (int i = 0; i < lConnectionCount; i++)
				{
					var lIndex = Int32.Parse(lSr.ReadLine());
					if (lIndex < lInputCount) lNetwork.Connections[i].InputNode = lNetwork.InputNodes[lIndex];
					else lNetwork.Connections[i].InputNode = lNetwork.InnerNodes[lIndex - lInputCount];
				}
				lSr.ReadLine();

                // Čísla vrstvy vnitřních uzlů
                for (int i = 0; i < lInnerNodeCount; i++)
                {
                    var lLevel = (int?)int.Parse(lSr.ReadLine(), CultureInfo.InvariantCulture);
                    if (lLevel.Value < 0) lLevel = null;
                    lNetwork.InnerNodes[i].LevelInNetwork = lLevel;
                }
                lSr.ReadLine();

                // Biasy vnitřních uzlů
                for (int i = 0; i < lInnerNodeCount; i++)
				{
					var lBias = Double.Parse(lSr.ReadLine(), CultureInfo.InvariantCulture);
					lNetwork.InnerNodes[i].Bias = lBias;
				}
				lSr.ReadLine();

                // Strmosti vnitřních uzlů
                for (int i = 0; i < lInnerNodeCount; i++)
                {
                    var lSteepness = Double.Parse(lSr.ReadLine(), CultureInfo.InvariantCulture);
                    lNetwork.InnerNodes[i].Steepness = lSteepness;
                }
                lSr.ReadLine();

                // Váhy spojů
                for (int i = 0; i < lConnectionCount; i++)
				{
					var lWeight = Double.Parse(lSr.ReadLine(), CultureInfo.InvariantCulture);
					lNetwork.Connections[i].Weight = lWeight;
				}

			}
			return lNetwork;
		}

	}
}
