﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetworkLibrary
{
	public abstract class NodeBase
	{
		public abstract double GetValue();
	}
}
