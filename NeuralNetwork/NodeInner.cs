﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetworkLibrary
{
    public class NodeInner : NodeBase
	{
		private static double SigmoidValue(double aInput)
		{
			return 1 / (1 + Math.Exp(-aInput));
		}
		public double Bias { get; set; }
        public double Steepness { get; set; } = 1;
		public List<Connection> InputConnections { get; set; }

        private int? mLevelInNetwork;
        public int? LevelInNetwork
        {
            get { return mLevelInNetwork; }
            set
            {
                Debug.Assert(value == null || value.Value > 1);
                mLevelInNetwork = value;
            }
        }

        private double mLastOutput = double.NaN;
        private double mLastBias = double.NaN;
        private double mLastSteepness = double.NaN;
		private double[] mInputsToLastOutput = new double[0];
		public override double GetValue()
		{
			Debug.Assert(InputConnections != null);

			bool lInputsModified = false;
			if (mInputsToLastOutput.Length != InputConnections.Count) lInputsModified = true;
			var lValue = 0.0;
			var lNewInputs = new double[InputConnections.Count];
            var lNewSteepness = Steepness;
            var lNewBias = Bias;

			for (int i = 0; i < InputConnections.Count; i++)
			{
				var lInput = InputConnections[i];
				var lInputValue = lInput.GetValue();
				if (i < mInputsToLastOutput.Length && lInputValue != mInputsToLastOutput[i]) lInputsModified = true;
				lNewInputs[i] = lInputValue;
				lValue += lInputValue;
			}

			if (!lInputsModified && lNewBias == mLastBias && lNewSteepness == mLastSteepness) 
				return mLastOutput;

			lValue += lNewBias;
            lValue *= lNewSteepness;

			lValue = SigmoidValue(lValue);

			mInputsToLastOutput = lNewInputs;
            mLastBias = lNewBias;
            mLastSteepness = lNewSteepness;
			mLastOutput = lValue;
			return lValue;
		}

		public NodeInner()
		{
			InputConnections = new List<Connection>();
		}
    }
}
