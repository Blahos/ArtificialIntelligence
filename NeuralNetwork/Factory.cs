﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetworkLibrary
{
	public static class NeuralNetworkFactory
    {
        public static NeuralNetwork ConstructCompleteNetwork(params int[] aLayersNodeCount)
        {
            // Ideálně by to chtělo udělat univerzálně, ať je to schopné udělat síť pro libovolný počet vrstev, 
            // ale zatím je to jen jako převolání odpovídající funkce (pro odpovídající počet vrstev)

            if (aLayersNodeCount.Length == 2) return Construct2LayersCompleteNetwork(aLayersNodeCount[0], aLayersNodeCount[1]);
            if (aLayersNodeCount.Length == 3) return Construct3LayersCompleteNetwork(aLayersNodeCount[0], aLayersNodeCount[1], aLayersNodeCount[2]);
            if (aLayersNodeCount.Length == 4) return Construct4LayersCompleteNetwork(aLayersNodeCount[0], aLayersNodeCount[1], aLayersNodeCount[2], aLayersNodeCount[3]);
            if (aLayersNodeCount.Length == 5) return Construct5LayersCompleteNetwork(aLayersNodeCount[0], aLayersNodeCount[1], aLayersNodeCount[2], aLayersNodeCount[3], aLayersNodeCount[4]);

            throw new ArgumentException($"{aLayersNodeCount.Length} layers are currently not supported.");

        }
        public static NeuralNetwork Construct2LayersCompleteNetwork(int aInputLayerCount, int aOutputLayerCount)
        {
            var lNetwork = new NeuralNetwork();
            for (int i = 0; i < aInputLayerCount; i++)
            {
                lNetwork.InputNodes.Add(new NodeInput());
            }
            var lOutputLayer = new List<NodeInner>();
            for (int i = 0; i < aOutputLayerCount; i++)
            {
                var lOutputLayerNode = new NodeInner { LevelInNetwork = 2 };
                lNetwork.InnerNodes.Add(lOutputLayerNode);
                lOutputLayer.Add(lOutputLayerNode);
            }

            foreach (var lOutputLayerNode in lOutputLayer)
            {
                foreach (var lInputLayerNode in lNetwork.InputNodes)
                {
                    var lConnection = new Connection();
                    lConnection.InputNode = lInputLayerNode;
                    lOutputLayerNode.InputConnections.Add(lConnection);
                    lNetwork.Connections.Add(lConnection);
                }
                lNetwork.Outputs.Add(new ConnectionOutput { InputNode = lOutputLayerNode });
            }
            
            return lNetwork;
        }
        public static NeuralNetwork Construct3LayersCompleteNetwork(int aInputLayerCount, int aSecondLayerCount, int aOutputLayerCount)
		{
			var lNetwork = new NeuralNetwork();
			for (int i = 0; i < aInputLayerCount; i++)
			{
				lNetwork.InputNodes.Add(new NodeInput());
			}
			var lSecondLayer = new List<NodeInner>();
			for (int i = 0; i < aSecondLayerCount; i++)
			{
				var lSecondLayerNode = new NodeInner { LevelInNetwork = 2 };
				lNetwork.InnerNodes.Add(lSecondLayerNode);
				lSecondLayer.Add(lSecondLayerNode);
			}
			var lOutputLayer = new List<NodeInner>();
			for (int i = 0; i < aOutputLayerCount; i++)
			{
				var lOutputLayerNode = new NodeInner { LevelInNetwork = 3 };
				lNetwork.InnerNodes.Add(lOutputLayerNode);
				lOutputLayer.Add(lOutputLayerNode);
			}

			foreach (var lSecondLayerNode in lSecondLayer)
			{
				foreach (var lInputLayerNode in lNetwork.InputNodes)
				{
					var lConnection = new Connection();
					lConnection.InputNode = lInputLayerNode;
					lSecondLayerNode.InputConnections.Add(lConnection);
					lNetwork.Connections.Add(lConnection);
				}
			}

			foreach (var lOutputLayerNode in lOutputLayer)
			{
				foreach (var lSecondLayerNode in lSecondLayer)
				{
					var lConnection = new Connection();
					lConnection.InputNode = lSecondLayerNode;
					lOutputLayerNode.InputConnections.Add(lConnection);
					lNetwork.Connections.Add(lConnection);
				}
				lNetwork.Outputs.Add(new ConnectionOutput { InputNode = lOutputLayerNode });
			}

			return lNetwork;
		}
		public static NeuralNetwork Construct4LayersCompleteNetwork(int aInputLayerCount, int aSecondLayerCount, int aThirdLayerCount, int aOutputLayerCount)
		{
			var lNetwork = new NeuralNetwork();
			for (int i = 0; i < aInputLayerCount; i++)
			{
				lNetwork.InputNodes.Add(new NodeInput());
			}
			var lSecondLayer = new List<NodeInner>();
			for (int i = 0; i < aSecondLayerCount; i++)
			{
				var lSecondLayerNode = new NodeInner { LevelInNetwork = 2 };
				lNetwork.InnerNodes.Add(lSecondLayerNode);
				lSecondLayer.Add(lSecondLayerNode);
			}
			var lThirdLayer = new List<NodeInner>();
			for (int i = 0; i < aThirdLayerCount; i++)
			{
				var lThirdLayerNode = new NodeInner { LevelInNetwork = 3 };
				lNetwork.InnerNodes.Add(lThirdLayerNode);
				lThirdLayer.Add(lThirdLayerNode);
			}
			var lOutputLayer = new List<NodeInner>();
			for (int i = 0; i < aOutputLayerCount; i++)
			{
                var lOutputLayerNode = new NodeInner { LevelInNetwork = 4 };
				lNetwork.InnerNodes.Add(lOutputLayerNode);
				lOutputLayer.Add(lOutputLayerNode);
			}

			foreach (var lSecondLayerNode in lSecondLayer)
			{
				foreach (var lInputLayerNode in lNetwork.InputNodes)
				{
					var lConnection = new Connection();
					lConnection.InputNode = lInputLayerNode;
					lSecondLayerNode.InputConnections.Add(lConnection);
					lNetwork.Connections.Add(lConnection);
				}
			}

			foreach (var lThirdLayerNode in lThirdLayer)
			{
				foreach (var lSecondLayerNode in lSecondLayer)
				{
					var lConnection = new Connection();
					lConnection.InputNode = lSecondLayerNode;
					lThirdLayerNode.InputConnections.Add(lConnection);
					lNetwork.Connections.Add(lConnection);
				}
			}

			foreach (var lOutputLayerNode in lOutputLayer)
			{
				foreach (var lThirdLayerNode in lThirdLayer)
				{
					var lConnection = new Connection();
					lConnection.InputNode = lThirdLayerNode;
					lOutputLayerNode.InputConnections.Add(lConnection);
					lNetwork.Connections.Add(lConnection);
				}
				lNetwork.Outputs.Add(new ConnectionOutput { InputNode = lOutputLayerNode });
			}

			return lNetwork;
		}
		public static NeuralNetwork Construct5LayersCompleteNetwork(int aInputLayerCount, int aSecondLayerCount, int aThirdLayerCount, int aFourthLayerCount, int aOutputLayerCount)
		{
			var lNetwork = new NeuralNetwork();
			for (int i = 0; i < aInputLayerCount; i++)
			{
				lNetwork.InputNodes.Add(new NodeInput());
			}
			var lSecondLayer = new List<NodeInner>();
			for (int i = 0; i < aSecondLayerCount; i++)
			{
				var lSecondLayerNode = new NodeInner { LevelInNetwork = 2 };
				lNetwork.InnerNodes.Add(lSecondLayerNode);
				lSecondLayer.Add(lSecondLayerNode);
			}
			var lThirdLayer = new List<NodeInner>();
			for (int i = 0; i < aThirdLayerCount; i++)
			{
				var lThirdLayerNode = new NodeInner { LevelInNetwork = 3 };
				lNetwork.InnerNodes.Add(lThirdLayerNode);
				lThirdLayer.Add(lThirdLayerNode);
			}
			var lFourthLayer = new List<NodeInner>();
			for (int i = 0; i < aFourthLayerCount; i++)
			{
				var lFourthLayerNode = new NodeInner { LevelInNetwork = 4 };
				lNetwork.InnerNodes.Add(lFourthLayerNode);
				lFourthLayer.Add(lFourthLayerNode);
			}
			var lOutputLayer = new List<NodeInner>();
			for (int i = 0; i < aOutputLayerCount; i++)
			{
				var lOutputLayerNode = new NodeInner { LevelInNetwork = 5 };
				lNetwork.InnerNodes.Add(lOutputLayerNode);
				lOutputLayer.Add(lOutputLayerNode);
			}

			foreach (var lSecondLayerNode in lSecondLayer)
			{
				foreach (var lInputLayerNode in lNetwork.InputNodes)
				{
					var lConnection = new Connection();
					lConnection.InputNode = lInputLayerNode;
					lSecondLayerNode.InputConnections.Add(lConnection);
					lNetwork.Connections.Add(lConnection);
				}
			}

			foreach (var lThirdLayerNode in lThirdLayer)
			{
				foreach (var lSecondLayerNode in lSecondLayer)
				{
					var lConnection = new Connection();
					lConnection.InputNode = lSecondLayerNode;
					lThirdLayerNode.InputConnections.Add(lConnection);
					lNetwork.Connections.Add(lConnection);
				}
			}

			foreach (var lFourthLayerNode in lFourthLayer)
			{
				foreach (var lThirdLayerNode in lThirdLayer)
				{
					var lConnection = new Connection();
					lConnection.InputNode = lThirdLayerNode;
					lFourthLayerNode.InputConnections.Add(lConnection);
					lNetwork.Connections.Add(lConnection);
				}
			}

			foreach (var lOutputLayerNode in lOutputLayer)
			{
				foreach (var lFourthLayerNode in lFourthLayer)
				{
					var lConnection = new Connection();
					lConnection.InputNode = lFourthLayerNode;
					lOutputLayerNode.InputConnections.Add(lConnection);
					lNetwork.Connections.Add(lConnection);
				}
				lNetwork.Outputs.Add(new ConnectionOutput { InputNode = lOutputLayerNode });
			}

			return lNetwork;
		}
	}
}
