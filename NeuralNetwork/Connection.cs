﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetworkLibrary
{
	public class Connection
	{
		public NodeBase InputNode { get; set; }
		public double Weight { get; set; }
		public double GetValue()
		{
			Debug.Assert(InputNode != null);
			var lValue = InputNode.GetValue();
			return lValue * Weight;
		}
	}
}
