﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetworkLibrary
{
	public class NodeInput : NodeBase
	{
		/// <summary>
		/// Hodnoty 0 až 1
		/// </summary>
		public double InputValue { get; set; }

        public override double GetValue()
		{
			return InputValue;
		}
	}
}
